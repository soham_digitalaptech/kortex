'use strict';

(function($) {
    
    if (!$.Kortx) {
        $.Kortx = {};
    }
    
    $(function() {
    
        $.Kortx.ContentAnimate = function(backup) {
            
            var base = this;
            base.blockHeight = 0;
            base.documentHeight = 0;
            base.blockCount = parseInt($('section.main-block').length, 10);
            base.activeBlockIndex = $('section').index($(document).find('.active'));
            base.allwNext = 'false';
            base.backup = backup;
            
            base.nextPermission = function() {
                base.allwNext = 'true';
                return base.allwNext;
            };
            
            base.init = function() {
                base.setBlockHeight();
                
                var direction;
                
                // trigger up or donw arrow keypress event
                $(document).keydown(function(event) {
                    if(event.which === 38) {
                        direction = 'up';
                    } else if (event.which === 40) {
                        direction = 'down';
                    }
                    base.changeSlide(direction);
                });
                
                // trigger mouse wheel scroll event
                $(document).bind('mousewheel DOMMouseScroll', function (event) {           
                    if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
                        direction = 'up';
                    }
                    else {
                        direction = 'down';
                    }
                    base.changeSlide(direction);
               });
               
               $(document).on('click', '.arrow-down', function() {
                   direction = "down";
                   base.changeSlide(direction);
               });

               $(document).on('click', '.arrow-up, .logo a', function(e) {

					if (typeof(Storage) !== "undefined") {
						localStorage.setItem('forceRefresh','1');
					} else {
						
					}
					location.reload(false);
               });
            };
            
            base.defineBlockHeight = function() {
                base.blockHeight = $(window).height();
            };
            
            base.setBlockHeight = function() {
                base.defineBlockHeight();
                $('.main-block').height(base.blockHeight);
            };
            
            base.changeSlide = function(direction) {
                if(base.allwNext === 'true'){
                    base.activeBlockIndex = $('section').index($(document).find('.active'));
                    if(direction === 'up' && base.activeBlockIndex > 0) {
                        $('section').eq(base.activeBlockIndex).removeClass('active');
                        var comingBlockIndex = base.activeBlockIndex - 1;
                        $('section').eq(comingBlockIndex).addClass('active');
                        base.runSlide(comingBlockIndex, direction);
                        base.allwNext = 'false';
                    } else if(direction === 'down' && (base.activeBlockIndex + 1) < base.blockCount) {
                        console.log(base.activeBlockIndex + 1)
                        console.log(base.blockCount)
                        
                        $('section').eq(base.activeBlockIndex).removeClass('active');
                        var comingBlockIndex = base.activeBlockIndex + 1;
                        $('section').eq(comingBlockIndex).addClass('active');
                        base.runSlide(comingBlockIndex, direction);
                        base.allwNext = 'false';
                    }
                }
            };
            
            base.runSlide = function(cbi, direction) {
                base.toggleNav(cbi);
                base.toggleNavControl(cbi);
                
                switch(cbi) {
                    
                    case 0:
                        base.runSlide0(direction);
                        break;
                    
                    case 1:
                        base.runSlide1(direction);
                        break;
                    
                    case 2:
                        base.runSlide2(direction);
                        break;
                    
                    case 3:
                        base.runSlide3(direction);
                        break;
                    
                    case 4:
                        base.runSlide4(direction);
                        break;
                    
                    case 5:
                        base.runSlide5(direction);
                        break;
                    
                    case 6:
                        base.runSlide6(direction);
                        break;
                    
                    case 7:
                        base.runSlide7(direction);
                        break;
                }
            };
            
            base.toggleNav =  function(cbi) {
                if (cbi > 0) {
                    $('.side_nav').removeClass('is-hidden').addClass('is-visible');
                } else {
                    $('.side_nav').removeClass('is-visible').addClass('is-hidden');
                }
            };
            
            base.toggleNavControl =  function(cbi) {
                $('.nav-item').removeClass('active-nav').eq(cbi).addClass('active-nav');
                
            };
            
            base.runSlide0 = function(direction) {
                var dir = direction;
                $.when(
                    $('.first_block').fadeIn(400),
                    $('.black_logo').delay(400).fadeIn(400),
                    $('.arrow01').delay(800).animate({width : '12.6875em'},200),
                    $('.searh-icon').delay(1000).animate({width : '18.5625em'},100),
                    $('.arrow02').delay(1100).animate({width : '11.563em'},200),
                    $('.man-img').delay(1300).fadeIn(200),
                    $('.heading_top').delay(1500).fadeIn(300),
                    $('.header_right').delay(1800).fadeIn(200),
                    $('#section1 .down_arrow').delay(2000).animate({bottom : '6.94%'},600)
                ).done(function() {
                    base.nextPermission();
                });
                                
                if(dir === 'up') {
                    $.when(
                        $('.second_block').animate({opacity:'0'}, 1000).promise(),
                        $('.second_block .title').fadeOut(600).promise(),
                        base.collapseCircles(),
                        $('.first_block').animate({opacity:'1'}, 1000).promise(),
                        $('.toplink').show().promise(),
                        $('.btn_blue').hide().promise()
                    ).done(function() {
                        base.nextPermission();
                    });
                }
                
            };
            
            base.runSlide1 = function(direction) {
                var dir = direction;
                if(dir === 'down') {
                    $.when(
                        $('.toplink').hide().promise(),
                        $('.btn_blue').show(300).promise(),
                        $('.first_block').animate({opacity: '0'}, 400).promise(),
                        $('.second_block').show().animate({opacity: '1'}, 500).promise(),
                        $('.second_block .title').fadeIn(800).promise(),
                        base.expandCircles()
                    ).done(function() {
                        base.nextPermission();
                    });
                } else if(dir === 'up') {
                    
                    $.when(
                        $('.third_block, .third_block-bg').animate({top:'150%'}, 1500).promise(),
                        $('.activity_part').fadeIn(100).promise()
                    ).done(function() {
                        $('.third_block, .third_block-bg').addClass('is-hidden').promise(),
                        base.expandCircles(),
                        $('.second_block .title').fadeIn(600).promise().done(function() {
                            base.nextPermission();
                        });
                    });
                }
            };
            
            base.runSlide2 = function(direction) {
                var dir = direction;
                if(dir === 'down') {
                    
                    $.when(
                        $('.second_block .title').fadeOut(600).promise(),
                        base.collapseCircles()
                    ).done(function(){
                        $('.activity_part').delay(550).fadeOut(150).promise();
                        $('.third_block, .third_block-bg').removeClass('is-hidden').animate({top:'0%'}, 800).promise()
                            .done(function() {
                                $('.ph_avator01').fadeIn(600).promise()
                                .done(function(){
                                    base.nextPermission();
                                });
                            });
                    });                  
                   
                } else if(dir === 'up') {
                    
                    $.when(
                        $('.dark_bg').animate({opacity: '0'}, 800).promise(),
                        $('.white_logo').hide().promise(),
                        $('.black_logo').fadeIn(400).promise(),
                        $('.ticket').animate({top: '150%'}, 800).promise(),
                        $('.fourth_block h2').fadeOut(500).promise()
                    ).done(function() {
                        $('.dark_bg').css('display','none').promise().done(function() {
                            $('.third_block .title, .third_block .phone_img').fadeIn(600).promise().done(function() {
                                $('.activity_part').css('display', 'block').promise().done(function() {
                                    base.nextPermission();
                                });
                            });
                        });
                    });
                }
            };
            
            base.runSlide3 = function(direction) {
                var dir = direction;
                if (dir === 'down') {
                    $.when(
                        $('.activity_part').css('display', 'none').promise(),
                        $('.third_block .title, .third_block .phone_img').fadeOut(800).promise()
                    ).done(function() {
                        $('.fourth_block').removeClass('is-hidden').promise(),
                        $('.dark_bg').css('display','block').animate({opacity: '1'}, 400).promise(),
                        $('.black_logo').hide().promise(),
                        $('.white_logo').fadeIn(400).promise(),
                        $('.ticket').animate({top: '9em'}, 800).promise().done(function() {
                            $('.fourth_block h2').fadeIn(500).promise().done(function(){
                                 base.nextPermission();
                            });
                        });
                    });
                } else if(dir === 'up') {
                    $.when(
                        $('.fifth_block .overlay_bg').css('display', 'block').animate({opacity: '0'}, 300).promise(),
                        $('.black_logo').hide().promise(),
                        console.log('black_logo hide')
                    ).done(function(){
                        $('.white_logo').fadeIn(400).promise(),
                        $('.make_selection').animate({left: '153.98%'}, 500).promise(),
                        $('.make_selection1').animate({left: '180.36%'}, 500).promise(),
                        $('.make_selection2').animate({left: '210.1%'}, 500).promise(),
                        $('.make_selection3').animate({left: '240%'}, 500).promise()
                        .done(function() {
                            $('.fifth_block h2, .selection_box img').animate({opacity: '0'}, 300).promise()
                            .done(function() {
                                $('.fifth_block').addClass('is-hidden').promise()
                                .done(function() {
                                    $('.fourth_block').css('display', 'block').promise(),
                                    $('.fourth_block .dark_bg').show().animate({opacity: '1'}).promise(),
                                    $('.ticket').animate({top: '9em'}, 600).promise(),
                                    $('.fourth_block h2').animate({top: '0%'}, 600).promise()
                                    .done(function() {
                                        base.nextPermission();
                                    });
                                });
                            });
                        });
                    });
                }
            };
            
            base.runSlide4 = function(direction) {
                var dir = direction;
                if(dir === 'down') {
                    
                    $.when(
                        $('.ticket, .fourth_block h2').animate({top: '-150%'}, 600).promise()
                    ).done(function() {
                        $('.white_logo').hide().promise(),
                        $('.black_logo').fadeIn(400).promise(),
                        $('.dark_bg').animate({opacity: '0'}, 400).promise().done(function() {
                            $('.dark_bg').css('display','none').promise().done(function() {
                                $('.fifth_block').removeClass('is-hidden').promise(),
                                $('.fifth_block h2, .selection_box img').animate({opacity: '1'}, 300).promise()
                                .done(function() {
                                    $('.make_selection').animate({left: '3.98%'}, 800).promise(),
                                    $('.make_selection1').animate({left: '30.36%'}, 800).promise(),
                                    $('.make_selection2').animate({left: '60.1%'}, 800).promise(),
                                    $('.make_selection3').animate({left: '90%'}, 800).promise()
                                    .done(function() {
                                        $('.fifth_block .overlay_bg').animate({opacity: '1'}, 600).promise()
                                        .done(function() {
                                            base.nextPermission();
                                        });
                                    });
                                });
                            });
                        });
                    });
                } else if(dir === 'up') {
                    $.when($('.chat_03').fadeOut(200).promise()).done(function() {
                    $('.white_logo').hide().promise(),
                    $('.black_logo').fadeIn(400).promise(),
                        $('.chat_02').fadeOut(200).promise().done(function() {
                            $('.chat_01').fadeOut(200).promise(),
                            $('.chat_heading').fadeIn(300).promise().done(function() {
                                $('.incm_chat').animate({top:'-120%'}, 500).promise().done(function() {                                    
                                    $('.sixth_block .dark_bg').css('display','block').animate({opacity: '0'}, 300).promise(),
                                    $('.sixth_block').addClass('is-hidden').promise(),
                                    $('.fifth_block').removeClass('is-hidden').animate({opacity: '1'}, 500).promise().done(function() {
                                        base.nextPermission();
                                    });
                                });
                            });
                        });
                    });
                }
            };
            
            base.runSlide5 = function(direction) {
                var dir = direction;
                if(dir === 'down') {
                    
                    $.when($('.fifth_block').animate({opacity: '0'}, 500).promise())
                    .done(function() {
                        $('.fifth_block').addClass('is-hidden').css('opacity', '0').promise()
                        .done(function() {
                            $('.black_logo').hide().promise(),
                            $('.white_logo').fadeIn(400).promise(),
                            $('.sixth_block').removeClass('is-hidden').promise(),
                            $('.sixth_block .dark_bg').css('display','block').animate({opacity: '1'}, 300).promise()
                            .done(function(){
                                $('.incm_chat').animate({top:'6.2em'}, 500).promise()
                                .done(function() {
                                    $('.chat_heading').fadeIn(300).promise()
                                    .done(function() {
                                         $('.chat_01').fadeIn(300).promise()
                                         .done(function() {
                                             $('.chat_02').fadeIn(300).promise()
                                             .done(function() {
                                                 $('.chat_03').fadeIn(300).promise().done(function() {
                                                     base.nextPermission();
                                                 });
                                             });
                                         });
                                    });
                                });
                            });
                        });
                    });
                }
                
                if(dir === 'up') {
                    $.when($('.fed_button.three').fadeOut(200).promise(),
                    $('.black_logo').hide().promise(),
                    $('.white_logo').fadeIn(400).promise()
                    ).done(function() {
                        $('.fed_button.two').fadeIn(200).promise().done(function() {
                            $('.fed_button.one').fadeIn(200).promise().done(function() {
                                $('.ph_bg').animate({opacity: '0'}, 200).promise(),
                                $('.empty-phone').animate({top: '150%'}, 300).promise(),
                                $('.getdone').fadeOut(200).promise(),
                                $('.seventh_block').fadeOut(300).promise().done(function() {
                                     $('.sixth_block').animate({top: '0%'}, 800).promise().done(function() {
                                         base.nextPermission();
                                     });
                                });
                            });
                        });
                    });
                }
            };
            
            base.runSlide6 = function(direction) {
                var dir = direction;
                if(dir === 'down') {
                    
                    $.when(
                        $('.sixth_block').animate({top: '-120%'}, 800).addClass('z200').promise())
                        .done(function(){
                            $('.white_logo').hide().promise(),
                            $('.black_logo').fadeIn(400).promise(),
                            $('.seventh_block').removeClass('is-hidden').css('display', 'block').animate({top: '0%'}, 800).promise()
                            .done(function() {
                                $('.getdone').fadeIn(300).promise(),
                                $('.empty-phone').animate({top: '0%'}, 600).promise()
                                .done(function() {
                                    $('.ph_bg').animate({opacity: '1'}, 300).promise()
                                    .done(function() {
                                        $('.fed_button.one').fadeIn(300).promise()
                                        .done(function() {
                                            $('.fed_button.two').fadeIn(300).promise()
                                            .done(function(){
                                                $('.fed_button.three').fadeIn(300).promise()
                                                .done(function() {
                                                    base.nextPermission();
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                } else if(dir === 'up') {
                    $.when(
                        $('.btn_blue').show(300).promise(),
                        $('.arrow-down').fadeIn(300).promise(),
                        $('.arrow-up').fadeOut(300).promise(),
                        $('.white_logo').hide().promise(),
                        $('.black_logo').fadeIn(400).promise(),
                        $('.eightth_block').fadeOut(800).promise(),
                        $('.seventh_block').fadeIn(900).promise()
                    ).done(function() {
                         base.nextPermission(); 
                    });
                }
            };
            
            base.runSlide7 = function(direction) {
                var dir = direction;
                if(dir === 'down') {
                    $.when(
                        $('.btn_blue').hide(300).promise(),
                        $('.arrow-down').fadeOut(300).promise(),
                        $('.black_logo').hide().promise(),
                        $('.white_logo').fadeIn(400).promise(),
                        $('.seventh_block').fadeOut(900).promise(),
                        $('.eightth_block').fadeIn(800).promise()
                    ).done(function() {
                        $('.arrow-up').fadeIn(300).promise(),
                        base.nextPermission();
                    });
                } else if(dir === 'up') {
                    base.nextPermission();
                }
            };
            
            base.expandCircles = function() {
                $('.activity_part').show();
                $('#datascience').animate({
                    left:'36.0445%', 
                    top:'0'
                }, 800);
                $('#artificialintelligence').animate({
                    right:'0%', 
                    top:'19.007%'
                }, 800);
                $('#biometrics').animate({
                    right:'0%',
                    bottom:'19.007%'
                }, 800);
                $('#predictiveanalytics').animate({
                    right:'36.0445%',
                    bottom:'0'
                }, 800);
                $('#machinelearning').animate({
                    left:'0',
                    bottom:'19.007%'
                }, 800);
                $('#dataprocessing').animate({
                    left:'0%',
                    top:'19.007%'
                }, 800);

                $('.circle span').animate({opacity: '1'}, 1100);
            };

            base.collapseCircles = function() {
                $('.circle span').animate({opacity: '0'}, 150);
                    
                $('#datascience').animate({
                    left:'36.0445%', 
                    top:'38.4397%'
                }, 800);
                
                $('#artificialintelligence').animate({
                    right:'36.0445%', 
                    top:'38.4397%'
                }, 800);
                
                $('#biometrics').animate({
                    right:'36.0445%',
                    bottom:'38.4397%'
                }, 800);
                
                $('#predictiveanalytics').animate({
                    right:'36.0445%',
                    bottom:'38.4397%'
                }, 800);
                
                $('#machinelearning').animate({
                    left:'36.0445%',
                    bottom:'38.4397%'
                }, 800);
                
                $('#dataprocessing').animate({
                    left:'36.0445%',
                    top:'38.4397%'
                }, 800);
            };
        };
        
        var contentAnimate = new $.Kortx.ContentAnimate();

        if (typeof(Storage) !== "undefined") {

			if(!localStorage.getItem('forceRefresh')) {
				var loaderContent = '<div id="loading-outer"><div class="loader-wrapper"><div class="loader"><img src="images/kortx-loader.gif" alt=""></div></div></div>';
				$.when($('.loader-section').append(loaderContent).promise()).done(function() {
                    var pageloadTimer = setTimeout(function() {
                        $('#loading-outer').hide();
                        contentAnimate.init();
                        contentAnimate.runSlide0();
                    }, 7500);
                });
			} else {
				localStorage.clear();
                contentAnimate.init();
                contentAnimate.runSlide0();
			}
		} else {
			// Sorry! No Web Storage support..
		}
        
    });
    
})(jQuery);