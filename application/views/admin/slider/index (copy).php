	<?php echo $header;?>
        <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="js/fileinput.js" type="text/javascript"></script>
	<script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
        <script type="text/javascript" src="js/jssor.slider.mini.js"></script>

        <style>
            .hideMe{
                display: none;
            }
            #progress-bar {
                background-color: #12CC1A;
                height:20px;
                color: #FFFFFF;
                width:0%;
                -webkit-transition: width .3s;
                -moz-transition: width .3s;
                transition: width .3s;
            }
            #progress-div {
                border:#0FA015 1px solid;
                padding: 5px 0px;
                margin:30px 0px;
                border-radius:4px;
                text-align:center;
                width:85%;
            }
        </style>
        <script type="text/javascript">
            jQuery(function($){
                $("#file_1").fileinput({
                    overwriteInitial: true,
                    'showUpload':false, 
                    'previewFileType':'image'
                });
                
                $('#upload_slider_form').validate({
                        rules: {
                                'upload[]':{required:true,},
                       },
                       messages: {
                           'upload[]':"Please atleast one Image",
                       },
                });
                
                $('#file_1').change(function(){
                    var files = $('#file_1')[0].files;
                    if(files[0])
                    {
                        var ext = files[0].name.split('.').pop();
                        if(ext=='jpg' || ext=='jpeg' || ext=='png' || ext=='gif' || ext=='bmp'){

                        }
                        else
                        {
                            $('#image_validation').html('Only CSV/XLS/XLSX File can be Uploaded','ERROR');
                            $('.fileinput-remove-button').trigger('click');
                            return false;
                        }
                    }
                    $("#progress-div").hide();
                });
            });
        </script>
        
        <script>
            jQuery(document).ready(function ($) {
                var _SlideshowTransitions = [
                //Zoom- in
                {$Duration: 1200, $Zoom: 1, $Easing: { $Zoom: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseOutQuad }, $Opacity: 2 },
                //Zoom+ out
                {$Duration: 1000, $Zoom: 11, $SlideOut: true, $Easing: { $Zoom: $JssorEasing$.$EaseInExpo, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 },
                //Rotate Zoom- in
                {$Duration: 1200, $Zoom: 1, $Rotate: 1, $During: { $Zoom: [0.2, 0.8], $Rotate: [0.2, 0.8] }, $Easing: { $Zoom: $JssorEasing$.$EaseSwing, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseSwing }, $Opacity: 2, $Round: { $Rotate: 0.5} },
                //Rotate Zoom+ out
                {$Duration: 1000, $Zoom: 11, $Rotate: 1, $SlideOut: true, $Easing: { $Zoom: $JssorEasing$.$EaseInExpo, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInExpo }, $Opacity: 2, $Round: { $Rotate: 0.8} },

                //Zoom HDouble- in
                {$Duration: 1200, x: 0.5, $Cols: 2, $Zoom: 1, $Assembly: 2049, $ChessMode: { $Column: 15 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Zoom: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 },
                //Zoom HDouble+ out
                {$Duration: 1200, x: 4, $Cols: 2, $Zoom: 11, $SlideOut: true, $Assembly: 2049, $ChessMode: { $Column: 15 }, $Easing: { $Left: $JssorEasing$.$EaseInExpo, $Zoom: $JssorEasing$.$EaseInExpo, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 },

                //Rotate Zoom- in L
                {$Duration: 1200, x: 0.6, $Zoom: 1, $Rotate: 1, $During: { $Left: [0.2, 0.8], $Zoom: [0.2, 0.8], $Rotate: [0.2, 0.8] }, $Easing: { $Left: $JssorEasing$.$EaseSwing, $Zoom: $JssorEasing$.$EaseSwing, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseSwing }, $Opacity: 2, $Round: { $Rotate: 0.5} },
                //Rotate Zoom+ out R
                {$Duration: 1000, x: -4, $Zoom: 11, $Rotate: 1, $SlideOut: true, $Easing: { $Left: $JssorEasing$.$EaseInExpo, $Zoom: $JssorEasing$.$EaseInExpo, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInExpo }, $Opacity: 2, $Round: { $Rotate: 0.8} },
                //Rotate Zoom- in R
                {$Duration: 1200, x: -0.6, $Zoom: 1, $Rotate: 1, $During: { $Left: [0.2, 0.8], $Zoom: [0.2, 0.8], $Rotate: [0.2, 0.8] }, $Easing: { $Left: $JssorEasing$.$EaseSwing, $Zoom: $JssorEasing$.$EaseSwing, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseSwing }, $Opacity: 2, $Round: { $Rotate: 0.5} },
                //Rotate Zoom+ out L
                {$Duration: 1000, x: 4, $Zoom: 11, $Rotate: 1, $SlideOut: true, $Easing: { $Left: $JssorEasing$.$EaseInExpo, $Zoom: $JssorEasing$.$EaseInExpo, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInExpo }, $Opacity: 2, $Round: { $Rotate: 0.8} },

                //Rotate HDouble- in
                {$Duration: 1200, x: 0.5, y: 0.3, $Cols: 2, $Zoom: 1, $Rotate: 1, $Assembly: 2049, $ChessMode: { $Column: 15 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Top: $JssorEasing$.$EaseInCubic, $Zoom: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseOutQuad, $Rotate: $JssorEasing$.$EaseInCubic }, $Opacity: 2, $Round: { $Rotate: 0.7} },
                //Rotate HDouble- out
                {$Duration: 1000, x: 0.5, y: 0.3, $Cols: 2, $Zoom: 1, $Rotate: 1, $SlideOut: true, $Assembly: 2049, $ChessMode: { $Column: 15 }, $Easing: { $Left: $JssorEasing$.$EaseInExpo, $Top: $JssorEasing$.$EaseInExpo, $Zoom: $JssorEasing$.$EaseInExpo, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInExpo }, $Opacity: 2, $Round: { $Rotate: 0.7} },
                //Rotate VFork in
                {$Duration: 1200, x: -4, y: 2, $Rows: 2, $Zoom: 11, $Rotate: 1, $Assembly: 2049, $ChessMode: { $Row: 28 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Top: $JssorEasing$.$EaseInCubic, $Zoom: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseOutQuad, $Rotate: $JssorEasing$.$EaseInCubic }, $Opacity: 2, $Round: { $Rotate: 0.7} },
                //Rotate HFork in
                {$Duration: 1200, x: 1, y: 2, $Cols: 2, $Zoom: 11, $Rotate: 1, $Assembly: 2049, $ChessMode: { $Column: 19 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Top: $JssorEasing$.$EaseInCubic, $Zoom: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseOutQuad, $Rotate: $JssorEasing$.$EaseInCubic }, $Opacity: 2, $Round: { $Rotate: 0.8} }
                ];

                var options = {
                    $AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                    $AutoPlayInterval: 1500,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                    $PauseOnHover: 1,                                //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                    $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                    $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                    $SlideDuration: 600,                                //Specifies default duration (swipe) for slide in milliseconds
                    $FillMode:1,
                    $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                        $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                        $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                        $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                        $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                    },

                    $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                        $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                        $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        $AutoCenter: 2,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                        $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                    },

                    $ThumbnailNavigatorOptions: {                       //[Optional] Options to specify and enable thumbnail navigator or not
                        $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                        $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                        $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                        $Lanes: 2,                                      //[Optional] Specify lanes to arrange thumbnails, default value is 1
                        $SpacingX: 14,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                        $SpacingY: 12,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                        $DisplayPieces: 6,                             //[Optional] Number of pieces to display, default value is 1
                        $ParkingPosition: 156,                          //[Optional] The offset position to park thumbnail
                        $Orientation: 2                                //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
                    }
                };

                var jssor_slider1 = new $JssorSlider$("slider1_container", options);
                
                $("#slider1_container div").each(function(){ // the "div" wrapping the "img"
                        var number = $(this).index(); // get the slideIndex !!
                        if($(this).children("img[u=image]").attr('src'))
                        {
                            $(this).children("img[u=image]").attr("id","image_" + (number-1));
                        }
                });

                function SlideParkEventHandler(slideIndex, fromIndex) {
                    var src = $("#image_" + slideIndex).attr("alt");
                    $("#control_image").find('h4').html(src);
                    $("#control_image").find('a').attr('href','<?php echo base_url()."admin/slider/delete_image/'+$('#image_' + slideIndex).attr('image-id')+'/".$this->utility->getSecurity();?>');
                    $("#control_image").find('a').attr('title','Delete '+src);
                }

                jssor_slider1.$On($JssorSlider$.$EVT_PARK, SlideParkEventHandler);
                //responsive code begin
                //you can remove responsive code if you don't want the slider scales while window resizes
                function ScaleSlider() {
                    var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                    if (parentWidth)
                        jssor_slider1.$ScaleWidth(Math.max(Math.min(parentWidth, 1000), 300));
                    else
                        window.setTimeout(ScaleSlider, 30);
                }
                ScaleSlider();

                $(window).bind("load", ScaleSlider);
                $(window).bind("resize", ScaleSlider);
                $(window).bind("orientationchange", ScaleSlider);
                //responsive code end
            });
        </script>
        
	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>					
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url().'admin';?>">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url().'admin/slider';?>"><?php echo $title;?></a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
						  <!-- BEGIN VALIDATION STATES-->
						  <div class="portlet box green">
							 <div class="portlet-title">
								<h4>Upload Slider Image</h4>
							 </div>
							 <div class="portlet-body form">
								<form action="<?php echo base_url().'admin/slider/upload';?>" method="POST" id="upload_slider_form" class="form-horizontal" enctype="multipart/form-data">
								   <div class="alert alert-error hide">
									  <button class="close" data-dismiss="alert"></button>
									  You have some form errors. Please check below.
								   </div>
									<?php echo $this->utility->showMsg();?>
									<div class="control-group">
										<div class="controls">
                                            <input id="file_1" name="upload[]" class="input" type="file" multiple="true" data-preview-file-type="any">
                                                    
                                                    <header>
                                                        <h5>
                                                            <span style="color:blue;">Multiple Picture? (Press CRTL + Select Picture)</span><br/>
                                                            <span style="color:red;">Picture size must be 1900 x 520 </span> 
                                                        </h5>
                                                    </header>
                                                    
                                            <div id="image_validation" style="color:#FF0000;font-weight:bold;"></div>
										</div>
									</div>
                                                                    
                                                                        <div class="form-actions">
                                                                            <button type="submit" class="btn green"><i class="icon-upload-alt"></i> Upload</button>
                                                                        </div>
								   <input type="hidden" name="frmSecurity" value="<?php echo $this->utility->getSecurity();?>"/>
								</form>
								<!-- END FORM-->
							 </div>
						  </div>
                                                  <?php
                                                  if(!empty($images))
                                                  {
                                                  ?>
                                                  <div class="portlet box green">
                                                        <div class="portlet-title">
                                                               <h4><?php echo $title;?></h4>
                                                        </div>
                                                        <div class="portlet-body form">
                                                            <div class="portlet box purple span3">
                                                                <div class="portlet-title" id="control_image">
                                                                        <h4><?php echo $images[0]['image'];?></h4>
                                                                        <div class="tools">
                                                                            <a class="start" title="Delete <?php echo $images[0]['image'];?>" href="<?php echo base_url().'admin/slider/delete_image/'.base64_encode($images[0]['id']).'/'.$this->utility->getSecurity();?>">
                                                                                <i class="icon-trash icon-white"></i>
                                                                            </a>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 960px;
                                                                height: 480px; background: #ffffff; overflow: hidden;">

                                                                <!-- Loading Screen -->
                                                                <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                                                                    <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                                                                        background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
                                                                    </div>
                                                                    <div style="position: absolute; display: block; background: url(<?php echo base_url();?>img/loading.gif) no-repeat center center;
                                                                        top: 0px; left: 0px;width: 100%;height:100%;">
                                                                    </div>
                                                                </div>

                                                                <!-- Slides Container -->
                                                                <div u="slides" style="cursor: move; position: absolute; left: 240px; top: 0px; width: 720px; height: 480px; overflow: hidden;">
                                                                    <?php
                                                                    foreach($images as $value)
                                                                    {
                                                                    ?>
                                                                    <div>
                                                                        <img u="image" image-id="<?php echo base64_encode($value['id']);?>" alt="<?php echo $value['image'];?>" src="uploads/slider/<?php echo $value['image'];?>" />
                                                                        <img u="thumb" image-id="<?php echo base64_encode($value['id']);?>" alt="<?php echo $value['image'];?>" src="uploads/slider/thumb/<?php echo $value['image'];?>" />
                                                                        
                                                                    </div>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </div>

                                                                <!--#region Arrow Navigator Skin Begin -->
                                                                <style>
                                                                    /* jssor slider arrow navigator skin 05 css */
                                                                    /*
                                                                    .jssora05l                  (normal)
                                                                    .jssora05r                  (normal)
                                                                    .jssora05l:hover            (normal mouseover)
                                                                    .jssora05r:hover            (normal mouseover)
                                                                    .jssora05l.jssora05ldn      (mousedown)
                                                                    .jssora05r.jssora05rdn      (mousedown)
                                                                    */
                                                                    .jssora05l, .jssora05r {
                                                                        display: block;
                                                                        position: absolute;
                                                                        /* size of arrow element */
                                                                        width: 40px;
                                                                        height: 40px;
                                                                        cursor: pointer;
                                                                        background: url(<?php echo base_url();?>img/a17.png) no-repeat;
                                                                        overflow: hidden;
                                                                    }
                                                                    .jssora05l { background-position: -10px -40px; }
                                                                    .jssora05r { background-position: -70px -40px; }
                                                                    .jssora05l:hover { background-position: -130px -40px; }
                                                                    .jssora05r:hover { background-position: -190px -40px; }
                                                                    .jssora05l.jssora05ldn { background-position: -250px -40px; }
                                                                    .jssora05r.jssora05rdn { background-position: -310px -40px; }
                                                                </style>
                                                                <!-- Arrow Left -->
                                                                <span u="arrowleft" class="jssora05l" style="top: 158px; left: 248px;">
                                                                </span>
                                                                <!-- Arrow Right -->
                                                                <span u="arrowright" class="jssora05r" style="top: 158px; right: 8px">
                                                                </span>
                                                                <!--#endregion Arrow Navigator Skin End -->
                                                                <!--#region Thumbnail Navigator Skin Begin -->
                                                                <!-- Help: http://www.jssor.com/development/slider-with-thumbnail-navigator-jquery.html -->
                                                                <style>
                                                                    /* jssor slider thumbnail navigator skin 02 css */
                                                                    /*
                                                                    .jssort02 .p            (normal)
                                                                    .jssort02 .p:hover      (normal mouseover)
                                                                    .jssort02 .p.pav        (active)
                                                                    .jssort02 .p.pdn        (mousedown)
                                                                    */

                                                                    .jssort02 {
                                                                        position: absolute;
                                                                        /* size of thumbnail navigator container */
                                                                        width: 240px;
                                                                        height: 480px;
                                                                    }

                                                                        .jssort02 .p {
                                                                            position: absolute;
                                                                            top: 0;
                                                                            left: 0;
                                                                            width: 99px;
                                                                            height: 66px;
                                                                        }

                                                                        .jssort02 .t {
                                                                            position: absolute;
                                                                            top: 0;
                                                                            left: 0;
                                                                            width: 100%;
                                                                            height: 100%;
                                                                            border: none;
                                                                        }

                                                                        .jssort02 .w {
                                                                            position: absolute;
                                                                            top: 0px;
                                                                            left: 0px;
                                                                            width: 100%;
                                                                            height: 100%;
                                                                        }

                                                                        .jssort02 .c {
                                                                            position: absolute;
                                                                            top: 0px;
                                                                            left: 0px;
                                                                            width: 95px;
                                                                            height: 62px;
                                                                            border: #000 2px solid;
                                                                            box-sizing: content-box;
                                                                            background: url(<?php echo base_url();?>img/t01.png) -800px -800px no-repeat;
                                                                            _background: none;
                                                                        }

                                                                        .jssort02 .pav .c {
                                                                            top: 2px;
                                                                            _top: 0px;
                                                                            left: 2px;
                                                                            _left: 0px;
                                                                            width: 95px;
                                                                            height: 62px;
                                                                            border: #000 0px solid;
                                                                            _border: #fff 2px solid;
                                                                            background-position: 50% 50%;
                                                                        }

                                                                        .jssort02 .p:hover .c {
                                                                            top: 0px;
                                                                            left: 0px;
                                                                            width: 97px;
                                                                            height: 64px;
                                                                            border: #fff 1px solid;
                                                                            background-position: 50% 50%;
                                                                        }

                                                                        .jssort02 .p.pdn .c {
                                                                            background-position: 50% 50%;
                                                                            width: 95px;
                                                                            height: 62px;
                                                                            border: #000 2px solid;
                                                                        }

                                                                        * html .jssort02 .c, * html .jssort02 .pdn .c, * html .jssort02 .pav .c {
                                                                            /* ie quirks mode adjust */
                                                                            width /**/: 99px;
                                                                            height /**/: 66px;
                                                                        }
                                                                </style>

                                                                <!-- thumbnail navigator container -->
                                                                <div u="thumbnavigator" class="jssort02" style="left: 0px; bottom: 0px;">
                                                                    <!-- Thumbnail Item Skin Begin -->
                                                                    <div u="slides" style="cursor: default;">
                                                                        <div u="prototype" class="p">
                                                                            <div class=w><div u="thumbnailtemplate" class="t"></div></div>
                                                                            <div class=c></div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Thumbnail Item Skin End -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                  </div>
                                                  <?php
                                                  }
                                                  ?>
						  <!-- END VALIDATION STATES-->
					   </div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<script>
			jQuery(document).ready(function() {		
				App.setPage("slider");  // set current page
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>