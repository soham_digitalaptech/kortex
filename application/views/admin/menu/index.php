        <?php 
        function isHasSub($menu,$location)
        {
            $str="";
            if($menu)
            {
                $str.='<ol class="dd-list">';
            }
            foreach($menu as $value)
            {
                if($location==1)
                {
                    $str.='
                            <li class="dd-item dd3-item" data-id="'.$value['id'].'">
                                    <div class="dd-handle dd3-handle"></div>
                                    <div class="dd3-content">
                                        <span url="'.$value['url'].'">'.$value['caption'].'</span>
                                        <a href="#" title="Delete" class="pull-right control-menu control-delete"><i class="icon-trash"></i></a>
                                        <a href="#myModal1" data-toggle="modal" title="Edit" class="pull-right control-menu control-edit"><i class="icon-edit"></i></a>
                                    </div>
                            ';
                    if($value['sub_menu'])
                        $str.=isHasSub($value['sub_menu'],$location);
                    $str.="</li>";
                }    
            }
            if($menu)
            {
                $str.='</ol>';
            }
            return $str;
        }
        echo $header;
        ?>
	<link rel="stylesheet" type="text/css" href="assets/jquery-nestable/jquery.nestable.css" />
        <script src="assets/jquery-nestable/jquery.nestable.js"></script>
        <script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
        <script type="text/javascript">
            jQuery(function($){
                var form2 = $('#modalFrm');
                var error2 = $('.alert-error', form2);
                var success2 = $('.alert-success', form2);

                form2.validate({
                        errorElement: 'span', //default input error message container
                        errorClass: 'help-inline', // default input error message class
                        focusInvalid: false, // do not focus the last invalid input
                        ignore: "",
                        rules: {
                                caption: {
                                        required: true
                                },
                                url: {
                                        required: true,
                                        url:true
                                },
                        },
                        
                        invalidHandler: function (event, validator) { //display error alert on form submit   
                                success2.hide();
                                error2.show();
                        },

                        highlight: function (element) { // hightlight error inputs
                                $(element)
                                        .closest('.help-inline').removeClass('ok'); // display OK icon
                                $(element)
                                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                        },

                        unhighlight: function (element) { // revert the change dony by hightlight
                                $(element)
                                        .closest('.control-group').removeClass('error'); // set error class to the control group
                        },

                        success: function (label) {
                                if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
                                        label
                                                .closest('.control-group').removeClass('error').addClass('success');
                                        label.remove(); // remove error label here
                                } else { // display success icon for other inputs
                                        label
                                                .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                                }
                        },

                        submitHandler: function (form) {
                                form2.unbind('submit');
                                error2.hide();
                                form2.submit();
                        }

                });
                $('.dd').nestable({
                    group: 1
                });
                
                $('#main_menu').on('change',function(){
                    $('#orderFrm').find('input[name=order_set]').val(JSON.stringify($('#main_menu').nestable('serialize')));
                    $('#orderFrm').find('input[name=location]').val('1');
                    $('#orderFrm').submit();
                });
                
                $('#footer_menu').on('change',function(){
                    $('#orderFrm').find('input[name=order_set]').val(JSON.stringify($('#footer_menu').nestable('serialize')));
                    $('#orderFrm').find('input[name=location]').val('2');
                    $('#orderFrm').submit();
                });
                
                $('.addBtn').click(function(){
                    var div=$('#template_1').html();
                    //alert(div);
                    $(this).closest('.menu_container').find('.dd').find('ol:first').append(div);
                });
                
                $( "#myModal1" ).on('shown', function(){
                    
                });
                
                $('body').on('submit','#modalFrm',function(){
                    if(!$('#modalFrm').valid())
                        return false;
                    var dd_item=$('#dd_box').closest('.dd-item');
                    var id=dd_item.data('id');
                    $('#dd_box').html($( "#myModal1" ).find('input[name=caption]').val());
                    $('#dd_box').attr('url',$( "#myModal1" ).find('input[name=url]').val());
                    $( "#myModal1" ).modal('hide');
                    var targetURL="<?php echo base_url().'admin/menu/update'?>";
                    var data={
                        'id':id,
                        'caption':$( "#myModal1" ).find('input[name=caption]').val(),
                        'url':$( "#myModal1" ).find('input[name=url]').val(),
                        'location':$( "#myModal1" ).find('input[name=location]').val(),
                        'frmSecurity':'<?php echo $this->utility->getSecurity();?>'
                    };
                    showMsg('Saving..','SUCCESS');
                    var obj=ajaxFunctionNew(data,targetURL);
                    obj.success(function(res){
                        //alert(res);
                        if(res.status=="success")
                        {
                            $('#btn_save').removeAttr('disabled');
                            $('#btn_save').html('Save');
                            dd_item.attr('data-id',res.result.id);
                            showMsg(res.cause,'SUCCESS');
                            $('#dd_box').removeAttr('id');
                        }
                        else
                        {
                            showMsg(res.cause,'ERROR');
                            $('#dd_box').removeAttr('id');
                        }
                    });
                    return false;
                });
                
                $('body').on('click','#btn_save',function(){
                    $('#modalFrm').submit();
                });
                
                $('body').on('click','.control-edit',function(){
                    $( "#myModal1" ).find('input[name=caption]').val('');
                    $( "#myModal1" ).find('input[name=url]').val('');
                    
                    $( "#myModal1" ).find('input[name=caption]').val($(this).closest('.dd3-content').find('span').html());
                    $( "#myModal1" ).find('input[name=url]').val($(this).closest('.dd3-content').find('span').attr('url'));
                    
                    $( "#myModal1" ).find('input[name=location]').val($(this).closest('.dd').attr('location'));
                    $(this).closest('.dd3-content').find('span').attr('id','dd_box');
                });
                
                $('body').on('click','.control-delete',function(){
                    if(!confirm('Are you sure??\nAll Sub Menu will be also Deleted'))
                        return false;
                    var dd_item=$(this).closest('.dd-item');
                    var id=dd_item.data('id');
                    if(id==-99){
                        dd_item.remove();
                        return false;
                    }
                    var targetURL="<?php echo base_url().'admin/menu/delete'?>";
                    var data={
                        'id':id,
                        'frmSecurity':'<?php echo $this->utility->getSecurity();?>'
                    };
                    showMsg('Deleting..','SUCCESS');
                    var obj=ajaxFunctionNew(data,targetURL);
                    obj.success(function(res){
                        //alert(res);
                        if(res.status=="success")
                        {
                            dd_item.remove();
                            showMsg(res.cause,'SUCCESS');
                        }
                        else
                        {
                            showMsg(res.cause,'ERROR');
                        }
                    });
                    return false;
                });
            });
        </script>
        <style>
            .control-menu{
                margin:0 5px 0 5px;
                cursor: pointer;
            }
        </style>
	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>				
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url().'admin';?>">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url().'admin/menu';?>"><?php echo $title;?></a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="container-fluid">
                                    <div class="row-fluid">
                                            <div class="span12">
                                              <!-- BEGIN VALIDATION STATES-->
                                              <div class="portlet box green">
                                                     <div class="portlet-title">
                                                            <h4>Main Menu</h4>
                                                     </div>
                                                     <div class="portlet-body form menu_container">
                                                         <?php echo $this->utility->showMsg();?>
                                                         <div id="alertBox" class="hideMe"></div>
                                                        <!-- BEGIN FORM-->
                                                        <h3>Main Menu <button class="btn blue addBtn pull-right" type="button">Add New Main Menu</button></h3>
                                                        <div class="dd" id="main_menu" location="1">
                                                                <ol class="dd-list">
                                                                    <?php
                                                                    foreach($menu as $value)
                                                                    {
                                                                        if($value['location']==1)
                                                                        {
                                                                    ?>
                                                                    <li class="dd-item dd3-item" data-id="<?php echo $value['id']?>">
                                                                        <div class="dd-handle dd3-handle"></div>
                                                                        <div class="dd3-content">
                                                                            <span url="<?php echo $value['url']?>"><?php echo $value['caption'];?></span>
                                                                            <a href="#" title="Delete" class="pull-right control-menu control-delete"><i class="icon-trash"></i></a>
                                                                            <a href="#myModal1" data-toggle="modal" title="Edit" class="pull-right control-menu control-edit"><i class="icon-edit"></i></a>
                                                                        </div>
                                                                        <?php
                                                                            echo isHasSub($value['sub_menu'],1);
                                                                        ?>
                                                                    </li>
                                                                    <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </ol>
                                                        </div>
                                                     </div>
                                              </div>
                                              
                                              <div class="portlet box green">
                                                     <div class="portlet-title">
                                                            <h4>Footer Menu</h4>
                                                     </div>
                                                     <div class="portlet-body form menu_container">
                                                            <!-- BEGIN FORM-->
                                                            <h3>Footer Menu <button class="btn blue addBtn pull-right" type="button">Add New Footer Menu</button></h3>
                                                            <div class="dd" id="footer_menu" tag="footer_menu" location="2">
                                                                <ol class="dd-list">
                                                                    <?php
                                                                    foreach($menu as $value)
                                                                    {
                                                                        if($value['location']==2)
                                                                        {
                                                                    ?>
                                                                    <li class="dd-item dd3-item" data-id="<?php echo $value['id']?>">
                                                                        <div class="dd-handle dd3-handle"></div>
                                                                        <div class="dd3-content">
                                                                            <span url="<?php echo $value['url']?>"><?php echo $value['caption'];?></span>
                                                                            <a href="#" title="Delete" class="pull-right control-menu control-delete"><i class="icon-trash"></i></a>
                                                                            <a href="#myModal1" data-toggle="modal" title="Edit" class="pull-right control-menu control-edit"><i class="icon-edit"></i></a>
                                                                        </div>
                                                                        <?php
                                                                            echo isHasSub($value['sub_menu'],2);
                                                                        ?>
                                                                    </li>
                                                                    <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </ol>
                                                            </div>
                                                     </div>
                                              </div>
                                              <!-- END VALIDATION STATES-->
                                       </div>
                                    </div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
                <div id="template_1" style="display: none">
                    <li class="dd-item dd3-item" data-id="-99">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <span url="">New Menu</span>
                                <a href="#" title="Delete" class="pull-right control-menu control-delete"><i class="icon-trash"></i></a>
                                <a href="#myModal1" data-toggle="modal" title="Edit" class="pull-right control-menu control-edit"><i class="icon-edit"></i></a>
                            </div>
                    </li>
                </div>
                <div id="myModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h3 id="myModalLabel1">Edit Menu</h3>
                        </div>
                        <div class="modal-body">
                            <form id="modalFrm" class="form-horizontal" action="#">
                                <div class="control-group">
                                   <label class="control-label">Menu Caption*</label>
                                   <div class="controls">
                                        <input type="text" placeholder="Caption" name="caption" class="span8 m-wrap">
                                   </div>
                                </div>
                                
                                <div class="control-group">
                                   <label class="control-label">URL*</label>
                                   <div class="controls">
                                        <input type="text" value="" name="url" placeholder="URL" class="span8 m-wrap">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="location" value=""/>
                            <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                            <button type="submit" class="btn blue" id="btn_save">Save</button>
                        </div>
                </div>
                <form id="orderFrm" action="<?php echo base_url().'admin/menu/update_order'?>" method="POST">
                    <input type="hidden" value="" name="location"/>
                    <input type="hidden" value="" name="order_set"/>
                    <input type="hidden" value="<?php echo $this->utility->getSecurity()?>" name="frmSecurity"/>
                </form>
		<script>
			jQuery(document).ready(function() {		
				App.setPage("settings");  // set current page
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>