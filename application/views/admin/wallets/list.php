<?php 
    $status=array('Not Verified','Active','Inactive');
    echo $header;
?>
<div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                        <div class="span12">
                                <!-- BEGIN STYLE CUSTOMIZER -->

                                <!-- BEGIN PAGE TITLE & BREADCRUMB-->			
                                <h3 class="page-title">
                                        <?php echo $title; ?>				
                                        <small><?php echo $sub_heading;?></small>
                                </h3>
                                <ul class="breadcrumb">
                                        <li>
                                                <i class="icon-home"></i>
                                                <a href="<?php echo base_url().'admin';?>">Home</a> 
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <li><a href="<?php echo base_url().'admin/user';?>"><?php echo $title;?></a></li>
                                        <li class="pull-right no-text-shadow">
                                                <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                                        <i class="icon-calendar"></i>
                                                        <span></span>
                                                        <i class="icon-angle-down"></i>
                                                </div>
                                        </li>
                                </ul>
                                <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="container-fluid">
                        <div class="row-fluid">
                                <div class="span12">
                                  <!-- BEGIN VALIDATION STATES-->
                                  <div class="portlet box green">
                                         <div class="portlet-title">
                                                <h4><?php echo $title;?></h4>
                                         </div>
                                         <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <?php echo $this->utility->showMsg();?>
                                                <h3>All Users</h3>
                                                <div class="row-fluid">
                                                        <a class="icon-btn span2 pull-right" href="<?php echo base_url().'admin/user/add'?>">
                                                            <i class="icon-plus"></i>
                                                            <div>Add Users</div>
                                                        </a>
                                                        <a class="icon-btn span2 pull-right" href="<?php echo base_url().'admin/user/account_types'?>">
                                                            <i class="icon-sitemap"></i>
                                                            <div>Account Type</div>
                                                        </a>
                                                </div>
                                                <table class="table table-hover">
                                                        <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>From</th>
                                                                    <th>To</th>
                                                                    <th>Description</th>
                                                                    <th>Wallet Balance</th>
                                                                    <th>Transfer amount</th>
                                                                    <th class="no_sort">Action</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        if(!empty($rows))
                                                            foreach($rows as $key=>$value)
                                                            {
                                                                ?>
                                                            <tr>
                                                                <td><?php echo $key+1;?></td>
                                                                <td><?php echo getUserName($value['sender_id']);?></td>
                                                                <td><?php echo getUserName($value['receiver_id']);?></td>
                                                                <td><?php echo $value['description'];?></td>
                                                                <td><?php echo getWalletBalance($value['sender_id']);?></td>
                                                                <td><?php echo $value['amount'];?></td>
                                                                <td>
                                                                        
    <!-- <a  href="<?php //echo base_url().'admin/walletManager/withdrawAmount/'.base64_encode($value['receiver_id']);?>">
        <span class="label label-default"><i class="fa fa-database"></i> Release</span>
    </a> -->

    <a href="javascript:void(0);" onclick="check_balance('<?php echo getWalletBalance($value['sender_id'])?>','<?php echo $value['amount']?>')">
        <span class="label label-default"><i class="fa fa-database"></i> Release</span>
    </a>

    <form action="<?php echo base_url().'admin/walletManager/withdrawAmount/'.base64_encode($value['receiver_id']);?>" method="post" id="walletForm" name="walletForm">
    </form>


                                                                </td>
                                                            </tr>
                                                                <?php
                                                                }
                                                                if(!$rows)
                                                                    echo '<tr><td colspan="6">No Users found</td></tr>';
                                                                ?>

                                                        </tbody>
                                                </table>
                                         </div>
                                  </div>
                                  <!-- END VALIDATION STATES-->
                           </div>
                        </div>
                </div>
        </div>
        <!-- END PAGE CONTAINER-->		
</div>
<script>
        jQuery(document).ready(function() 
        {		
                App.setPage("settings");  
                App.init(); 
        });
</script>

<script type="text/javascript">
    function check_balance(wallet_balance,amount)
    {
        if(wallet_balance < amount)
        {
            alert("Unsufficient balance!!!");       
        }
        else
        {
            document.getElementById("walletForm").submit();
        }
    }
</script>
<?php echo $footer;?>