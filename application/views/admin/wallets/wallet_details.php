<?php 
    $status=array('Not Verified','Active','Inactive');
    echo $header;
?>
<div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                        <div class="span12">
                                <!-- BEGIN STYLE CUSTOMIZER -->

                                <!-- BEGIN PAGE TITLE & BREADCRUMB-->			
                                <h3 class="page-title">
                                        <?php echo $title; ?>				
                                        <small><?php echo $sub_heading;?></small>
                                </h3>
                                <ul class="breadcrumb">
                                        <li>
                                                <i class="icon-home"></i>
                                                <a href="<?php echo base_url().'admin';?>">Home</a> 
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url().'admin/user';?>"><?php echo "Users Manager";?></a>
                                            <i class="icon-angle-right"></i>
                                        </li>
                                        <li><a href="#"><?php echo $title;?></a></li>
                                </ul>
                                <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="container-fluid">
                        <div class="row-fluid">
                                <div class="span12">
                                
                                  <div class="portlet box green">
                                         <div class="portlet-title">
                                                <h4><?php echo $title;?></h4>
                                         </div>
                                         <div class="portlet-body form">
                                            
                                                <?php echo $this->utility->showMsg();?>
                                         
                                                
                                                <div class="row-fluid">
                                                  
                        <div class="span4 responsive" data-tablet="span6" data-desktop="span4">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="icon-money"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><?php echo "$".$users['wallets'][0]['amount'];?></div>
                                    <div class="desc">Wallet Balance</div>
                                </div>
                                                       
                            </div>
                        </div>   

                         

                            <div class="portlet-body">
                            <div class="portlet-title">
                                <h4><i class="icon-shopping-cart"></i>Transaction Details</h4>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                    <a href="javascript:;" class="reload"></a>
                                    <a href="javascript:;" class="remove"></a>
                                </div>
                            </div>
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th>Transaction No</th>
                                            <th>Amount</th>
                                            <th>Transaction Type</th>
                                            <th>Sender Wallet</th>
                                            <th>Receiver Wallet</th>
                                            <th>Transaction Date</th>
                                            <th>Transaction status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        foreach ($transactions as $key => $value) 
                                        {
                                            ?>
                                            
                                        <tr>
                                            <td><?php echo $value->txn_no;?></td>
                                            <td><?php echo $value->amount;?></td>
                                            <td>
                                            <?php 
                                            if($value->txn_type==1)
                                            {
                                                echo "Add amount from paypal";
                                            }
                                            elseif($value->txn_type==2)
                                            {
                                                echo "Transfer amount to another user";
                                            }
                                            elseif($value->txn_type==3) 
                                            {
                                                echo "Send amount to paypal";
                                            }
                                            ?>
                                            </td>
                                            <td><?php echo getUsername($value->sender_wallet_id);?></td>
                                            <td><?php echo getUsername($value->receiver_wallet_id);?></td>
                                            <td><?php echo $value->date;?></td>
                                            <td>
                                            <?php 
                                            if($value->txn_status==1)
                                            {
                                                echo "Complete";
                                            }
                                            elseif($value->txn_status==2)
                                            {
                                                echo "Pending";
                                            }
                                            elseif($value->txn_status==3) 
                                            {
                                                echo "Failed";
                                            }
                                            ?>
                                            </td>
                                        </tr>

                                        <?php 
                                        }
                                            ?>
                                        
                                    </tbody>
                                </table>
                            </div>    
                            


                                                </div>
                                               
                                         </div>
                                  </div>
                                    <!-- END VALIDATION STATES-->
                           </div>
                        </div>
                </div>
        </div>
        <!-- END PAGE CONTAINER-->		
</div>
<script>
        jQuery(document).ready(function() {		
                App.setPage("settings");  // set current page
                App.init(); // init the rest of plugins and elements
        });
</script>
<?php echo $footer;?>