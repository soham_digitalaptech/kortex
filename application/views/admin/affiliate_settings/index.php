<?php 
        echo $header;
?>
<div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                        <div class="span12">
                                <!-- BEGIN STYLE CUSTOMIZER -->

                                <!-- BEGIN PAGE TITLE & BREADCRUMB-->			
                                <h3 class="page-title">
                                        <?php echo $title; ?>				
                                        <small><?php echo $sub_heading;?></small>
                                </h3>
                                <ul class="breadcrumb">
                                        <li>
                                                <i class="icon-home"></i>
                                                <a href="<?php echo base_url().'admin';?>">Home</a> 
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <li><a href="<?php echo base_url().'admin/ads';?>"><?php echo $title;?></a></li>
                                        <li class="pull-right no-text-shadow">
                                                <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                                        <i class="icon-calendar"></i>
                                                        <span></span>
                                                        <i class="icon-angle-down"></i>
                                                </div>
                                        </li>
                                </ul>
                                <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="container-fluid">
                        <div class="row-fluid">
                                <div class="span12">
                                  <!-- BEGIN VALIDATION STATES-->
                                  <div class="portlet box green">
                                         <div class="portlet-title">
                                                <h4><?php echo $title;?></h4>
                                         </div>
                                         <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <?php echo $this->utility->showMsg();?>
                                                <h3>Affiliate Settings</h3>
                                                <a href="<?php echo base_url().'admin/ads/add'?>"><button class="btn green pull-right" type="button">Add New</button></a>
                                                <table class="table table-hover">
                                                        <thead>
                                                                <tr>
                                                                   
                                                                    <th>Affiliate Settings</th>
                                                                    <th class="hidden-480">Action</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                
                                                                <tr>
                                                                        <td>Affiliate Banners</td>
                                                                        <td><a href="<?php echo base_url().'admin/affiliateSettings/viewBanners'?>"><span class="label label-success"><i class="fa fa-unlock"></i>View Banners </span></a></td> 
                                                                </tr>
                                                                <tr>
                                                                        <td>Affiliate Text</td>
                                                                        <td><a href="<?php echo base_url().'admin/settings/edit/affiliate_text'?>"><span class="label label-success"><i class="fa fa-unlock"></i> Edit </span></a></td> 
                                                                </tr>
                                                                <tr>
                                                                        <td>Term of Condition</td>
                                                                        <td><a href="<?php echo base_url().'admin/cms/edit/affiliate-program-terms-and-conditions'?>"><span class="label label-success"><i class="fa fa-unlock"></i> Edit </span></a></td> 
                                                                </tr>

                                                        </tbody>
                                                </table>
                                         </div>
                                  </div>
                                  <!-- END VALIDATION STATES-->
                           </div>
                        </div>
                </div>
        </div>
        <!-- END PAGE CONTAINER-->		
</div>
<script>
        jQuery(document).ready(function() {		
                App.setPage("settings");  // set current page
                App.init(); // init the rest of plugins and elements
        });
</script>
<?php echo $footer;?>