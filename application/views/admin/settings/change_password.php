	<?php echo $header;?>
	<script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
	<script type="text/javascript">
		jQuery(function($){
			var form2 = $('#change_password');
			var error2 = $('.alert-error', form2);
			var success2 = $('.alert-success', form2);

			form2.validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-inline', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				ignore: "",
				rules: {
					old_password: {
						minlength: 6,
						required: true
					},
					new_password: {
						minlength: 6,
						required: true,
					},
					con_password: {
						minlength: 6,
						required: true
					},
				},
				/*
				messages: { // custom messages for radio buttons and checkboxes
					membership: {
						required: "Please select a Membership type"
					},
					service: {
						required: "Please select  at least 2 types of Service",
						minlength: jQuery.format("Please select  at least {0} types of Service")
					}
				},
				
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter("#form_2_education_chzn");
					} else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
						error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
					} else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
						error.addClass("no-left-padding").insertAfter("#form_2_service_error");
					} else {
						error.insertAfter(element); // for other inputs, just perform default behavoir
					}
				},
				*/
				invalidHandler: function (event, validator) { //display error alert on form submit   
					success2.hide();
					error2.show();
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.help-inline').removeClass('ok'); // display OK icon
					$(element)
						.closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
				},

				unhighlight: function (element) { // revert the change dony by hightlight
					$(element)
						.closest('.control-group').removeClass('error'); // set error class to the control group
				},

				success: function (label) {
					if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
						label
							.closest('.control-group').removeClass('error').addClass('success');
						label.remove(); // remove error label here
					} else { // display success icon for other inputs
						label
							.addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
						.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
					}
				},

				submitHandler: function (form) {
                                    form2.unbind('submit');
					error2.hide();
					form2.submit();
				}

			});

			//apply validation on chosen dropdown value change, this only needed for chosen dropdown integration.
			$('.chosen, .chosen-with-diselect', form2).change(function () {
				form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
			});
		});
	</script>
	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>				
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url().'admin/settings/change_password';?>"><?php echo $title;?></a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
						  <!-- BEGIN VALIDATION STATES-->
						  <div class="portlet box green">
							 <div class="portlet-title">
								<h4><?php echo $title;?></h4>
							 </div>
							 <div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3>Change Admin Account Password</h3>
								<form action="<?php echo base_url().'admin/settings/change_password';?>" method="POST" id="change_password" class="form-horizontal">
								   <div class="alert alert-error hide">
									  <button class="close" data-dismiss="alert"></button>
									  You have some form errors. Please check below.
								   </div>
								   <?php echo $this->utility->showMsg();?>
								   <div class="control-group">
									  <label class="control-label">Old Password<span class="required">*</span></label>
									  <div class="controls">
										 <input type="password" name="old_password" data-required="1" class="span6 m-wrap"/>
									  </div>
								   </div>
								   <div class="control-group">
									  <label class="control-label">New Password<span class="required">*</span></label>
									  <div class="controls">
										 <input name="new_password" type="password" class="span6 m-wrap"/>
									  </div>
								   </div>
								   <div class="control-group">
									  <label class="control-label">Confirm Password<span class="required">*</span></label>
									  <div class="controls">
										 <input name="con_password" type="password" class="span6 m-wrap"/>
									  </div>
								   </div>
								   
								   <div class="form-actions">
									  <button type="submit" class="btn green">Change</button>
								   </div>
								   <input type="hidden" name="frmSecurity" value="<?php echo $this->utility->getSecurity();?>"/>
								</form>
								<!-- END FORM-->
							 </div>
						  </div>
						  <!-- END VALIDATION STATES-->
					   </div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<script>
			jQuery(document).ready(function() {		
				App.setPage("settings");  // set current page
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>