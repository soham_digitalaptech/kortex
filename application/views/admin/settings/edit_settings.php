	<?php echo $header;?>
	<link href="assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
	<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
	<script type="text/javascript">
		jQuery(function($){
			var form2 = $('#edit_settings');
			var error2 = $('.alert-error', form2);
			var success2 = $('.alert-success', form2);

			form2.validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-inline', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				ignore: "",
				rules: {
					old_password: {
						minlength: 6,
						required: true
					},
					new_password: {
						minlength: 6,
						required: true,
					},
					con_password: {
						minlength: 6,
						required: true
					},
				},
				/*
				messages: { // custom messages for radio buttons and checkboxes
					membership: {
						required: "Please select a Membership type"
					},
					service: {
						required: "Please select  at least 2 types of Service",
						minlength: jQuery.format("Please select  at least {0} types of Service")
					}
				},
				
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter("#form_2_education_chzn");
					} else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
						error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
					} else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
						error.addClass("no-left-padding").insertAfter("#form_2_service_error");
					} else {
						error.insertAfter(element); // for other inputs, just perform default behavoir
					}
				},
				*/
				invalidHandler: function (event, validator) { //display error alert on form submit   
					success2.hide();
					error2.show();
					App.scrollTo(error2, -200);
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.help-inline').removeClass('ok'); // display OK icon
					$(element)
						.closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
				},

				unhighlight: function (element) { // revert the change dony by hightlight
					$(element)
						.closest('.control-group').removeClass('error'); // set error class to the control group
				},

				success: function (label) {
					if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
						label
							.closest('.control-group').removeClass('error').addClass('success');
						label.remove(); // remove error label here
					} else { // display success icon for other inputs
						label
							.addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
						.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
					}
				},

				submitHandler: function (form) {
					form2.unbind('submit');
					form2.submit();
				}

			});

			//apply validation on chosen dropdown value change, this only needed for chosen dropdown integration.
			$('.chosen, .chosen-with-diselect', form2).change(function () {
				form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
			});
		});
	</script>
	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>					
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url().'admin/settings/general';?>">General Settings</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url().'admin/settings/edit/'.$rows['slug'];?>"><?php echo $title;?></a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
						  <!-- BEGIN VALIDATION STATES-->
						  <div class="portlet box green">
							 <div class="portlet-title">
								<h4><?php echo $title;?></h4>
							 </div>
							 <div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3>Edit Affiliate Text</h3>
								<form action="<?php echo base_url().'admin/settings/edit/'.$rows['slug'];?>" method="POST" id="edit_settings" class="form-horizontal" enctype="multipart/form-data">
								   <div class="alert alert-error hide">
									  <button class="close" data-dismiss="alert"></button>
									  You have some form errors. Please check below.
								   </div>
									<?php echo $this->utility->showMsg();?>
									<div class="control-group">
										<label class="control-label">Settings Name</label>
										<div class="controls">
											<input name="new_password" type="text" class="span6 m-wrap" value="<?php echo $rows['setting_name'];?>" disabled />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Settings Value<span class="required">*</span></label>
										<div class="controls">
										<?php
											if($rows['is_text'])
											{
										?>
											<input name="setting_value" type="text" class="span6 m-wrap" value="<?php echo $rows['setting_value'];?>"/>
											<input name="is_text" type="hidden" class="span6 m-wrap" value="1"/>
										<?php
											}
										?>
										<?php
											if($rows['is_textarea'])
											{
										?>
										<textarea rows="3" class="span6 m-wrap" name="setting_value"><?php echo $rows['setting_value'];?></textarea>
										<input name="is_textarea" type="hidden" class="span6 m-wrap" value="1"/>
										<?php
											}
										?>
										<?php
											if($rows['is_image'])
											{
										?>
										<div data-provides="fileupload" class="fileupload fileupload-new">
											<input type="hidden" value="" name="">
											<div style="width: 200px; height: 150px;" class="fileupload-new thumbnail">
											<?php echo (!empty($rows['setting_value']))?'<img src="'.base_url().'uploads/site_settings/'.$rows['setting_value'].'" alt="'.$rows['setting_name'].'"/>':'<img alt="no_image" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">';?>
											   
											</div>
											<div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
											<div>
											   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
											   <span class="fileupload-exists">Change</span>
											   <input type="file" class="default" name="image"></span>
											   <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
											</div>
										 </div>
										 <input name="is_image" type="hidden" class="span6 m-wrap" value="1"/>
										<?php
											}
											if($rows['instruction'])
											{
										?>
											<span class="label label-important">NOTE!</span>
											<span>
											 <?php echo $rows['instruction'];?>
											</span>
										<?php
											}
										?>
										</div>
									</div>
							   
								   <div class="form-actions">
									  <button type="submit" class="btn green">Change</button>
								   </div>
								   <input type="hidden" name="frmSecurity" value="<?php echo $this->utility->getSecurity();?>"/>
								</form>
								<!-- END FORM-->
							 </div>
						  </div>
						  <!-- END VALIDATION STATES-->
					   </div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<script>
			jQuery(document).ready(function() {		
				App.setPage("settings");  // set current page
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>