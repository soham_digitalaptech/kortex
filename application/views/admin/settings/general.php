		
	<?php echo $header;?>
	<link href="assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>				
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url().'admin/settings/general';?>"><?php echo $title;?></a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
						  <!-- BEGIN VALIDATION STATES-->
						  <div class="portlet box green">
							 <div class="portlet-title">
								<h4><?php echo $title;?></h4>
							 </div>
							 <div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3>All Site Settings</h3>
								<table class="table table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Setting</th>
											<th>Setting Value</th>
											<th class="hidden-480">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										foreach($rows as $key=>$value)
										{
										?>
										<tr>
											<td><?php echo $key+1;?></td>
											<td><?php echo $value['setting_name'];?></td>
											<td>
											<?php 
												if($value['is_image'])
												{
											?>
												<div class="fileupload fileupload-new" data-provides="fileupload">
													<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
														<img alt="<?php echo $value['setting_value'];?>" src="<?php echo (!empty($value['setting_value']))?base_url().'uploads/site_settings/'.$value['setting_value']:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image';?>">											   
													</div>
												 </div>
											<?php
												}
												else
													echo $value['setting_value'];
											?>
											</td>
											<td><a href="<?php echo base_url().'admin/settings/edit/'.$value['slug'];?>"><span class="label label-success">Edit</span></a></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							 </div>
						  </div>
						  <!-- END VALIDATION STATES-->
					   </div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<script>
			jQuery(document).ready(function() {		
				App.setPage("settings");  // set current page
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>