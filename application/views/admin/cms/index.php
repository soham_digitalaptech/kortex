	<?php 
		$max=-99;
		foreach($rows as $value)
		{
			if($value['order']>$max)
				$max=$value['order'];
		}
		echo $header;
	?>
	<link href="assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
	<script type="text/javascript">
		jQuery(function($){
			$('body').on('change','select[name=order]',function(){
				var order=$(this).val();
				var product_id=$(this).attr('product_id');
				$('#change_order_frm').find('input[name=order]').val(order);
				$('#change_order_frm').find('input[name=product_id]').val(product_id);
				$('#change_order_frm').unbind('submit');
				$('#change_order_frm').submit();
			});
		});
	</script>
	<!-- BEGIN PAGE -->
		<form id="change_order_frm" action="<?php echo base_url().'admin/cms/change_order'?>" method="POST">
			<input type="hidden" name="product_id" value="0"/>
			<input type="hidden" name="order" value="0"/>
			<input type="hidden" name="frmSecurity" value="<?php echo $this->utility->getSecurity()?>"/>
		</form>
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>				
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url().'admin';?>">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url().'admin/cms';?>"><?php echo $title;?></a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
						  <!-- BEGIN VALIDATION STATES-->
						  <div class="portlet box green">
							 <div class="portlet-title">
								<h4><?php echo $title;?></h4>
							 </div>
							 <div class="portlet-body form">
								<!-- BEGIN FORM-->
								<?php echo $this->utility->showMsg();?>
								<h3>All CMS Pages</h3>
								<a href="<?php echo base_url().'admin/cms/add'?>"><button class="btn green pull-right" type="button">Add New Page</button></a>
								<table class="table table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Title</th>
											<th class="hidden-480 nosort">Order</th>
											<th>Created</th>
                                                                                        <th>Modified</th>
                                                                                        <th>Status</th>
											<th class="hidden-480">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										foreach($rows as $key=>$value)
										{
										?>
										<tr>
											<td><?php echo $key+1;?></td>
											<td><?php echo $value['title'];?></td>
											<td class="hidden-480">
												<select class="span7 m-wrap" name="order" product_id="<?php echo base64_encode($value['id']);?>">
													<?php
													for($i=1;$i<=$max;$i++)
													{
														$selected="";
														if($value['order']==$i)
															$selected="selected";
													?>
													<option value="<?php echo $i?>" <?php echo $selected;?>><?php echo $i?></option>
													<?php
													}
													?>
												</select>
											</td>
											<td><?php echo ($value['date_of_created'])?date('d-m-y h:i A',$value['date_of_created']):'';?></td>
											<td><?php echo ($value['date_of_modified'])?date('d-m-y h:i A',$value['date_of_modified']):'';?></td>
											<td>
												<span class="label <?php echo ($value['status'])?'label-success':'label-important';?>"><?php echo ($value['status'])?'Active':'Blocked'?></span>
											</td>
											<td>
												<a class="btn mini purple" href="<?php echo base_url().'admin/cms/edit/'.$value['slug'];?>">
													<i class="icon-edit"></i>
													Edit
												</a>
												<a class="btn mini blue-stripe" href="#">View</a>
												<?php 
												if($value['status'])
												{
												?>
												<a class="btn mini red" href="<?php echo base_url().'admin/cms/change_status/'.$value['slug'].'/'.$this->utility->getSecurity();?>">
													<i class="icon-lock"></i>
													Blocked It
												</a>
												<?php
												}
												else
												{
												?>
												<a class="btn mini green" href="<?php echo base_url().'admin/cms/change_status/'.$value['slug'].'/'.$this->utility->getSecurity();?>">
													<i class="icon-unlock"></i>
													Active Now
												</a>
												<?php
												}
												?>
											</td>
										</tr>
										<?php
										}
										if(!$rows)
											echo '<tr><td colspan="6">No Pages found<td></tr>';
										?>
                                                                                
									</tbody>
								</table>
							 </div>
						  </div>
						  <!-- END VALIDATION STATES-->
					   </div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<script>
			jQuery(document).ready(function() {		
				App.setPage("settings");  // set current page
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>