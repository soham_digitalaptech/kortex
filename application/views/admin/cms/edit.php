	<?php echo $header;?>
	<script src="ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
	<script type="text/javascript">
		jQuery(function($){
			var form2 = $('#edit_settings');
			var error2 = $('.alert-error', form2);
			var success2 = $('.alert-success', form2);

			form2.validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-inline', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				ignore: "",
				rules: {
					title: {
						required: true
					},
					short_description: {
						required: true,
					},
					description: {
						required: true
					},
                                        status: {
						required: true
					},
                                        is_in_menu: {
						required: true
					},
				},
				/*
				messages: { // custom messages for radio buttons and checkboxes
					membership: {
						required: "Please select a Membership type"
					},
					service: {
						required: "Please select  at least 2 types of Service",
						minlength: jQuery.format("Please select  at least {0} types of Service")
					}
				},
				
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter("#form_2_education_chzn");
					} else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
						error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
					} else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
						error.addClass("no-left-padding").insertAfter("#form_2_service_error");
					} else {
						error.insertAfter(element); // for other inputs, just perform default behavoir
					}
				},
				*/
				invalidHandler: function (event, validator) { //display error alert on form submit   
					success2.hide();
					error2.show();
					App.scrollTo(error2, -200);
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.help-inline').removeClass('ok'); // display OK icon
					$(element)
						.closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
				},

				unhighlight: function (element) { // revert the change dony by hightlight
					$(element)
						.closest('.control-group').removeClass('error'); // set error class to the control group
				},

				success: function (label) {
					if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
						label
							.closest('.control-group').removeClass('error').addClass('success');
						label.remove(); // remove error label here
					} else { // display success icon for other inputs
						label
							.addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
						.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
					}
				},

				submitHandler: function (form) {
                                        form2.unbind('submit');
					error2.hide();
					form2.submit();
				}

			});

			//apply validation on chosen dropdown value change, this only needed for chosen dropdown integration.
			$('.chosen, .chosen-with-diselect', form2).change(function () {
				form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
			});
		});
	</script>
	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>					
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url().'admin';?>">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url().'admin/cms/index';?>">CMS Pages</a>
								<i class="icon-angle-right"></i>
							</li>
							<?php
							if(!empty($rows['slug']))
							{
							?>
							<li><a href="<?php echo base_url().'admin/cms/edit/'.$rows['slug'];?>"><?php echo $rows['title'];?></a></li>
							<?php
							}
							else
							{
							?>
							<li><a href="<?php echo base_url().'admin/cms/add';?>"><?php echo $title;?></a></li>
							<?php
							}
							?>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
						  <!-- BEGIN VALIDATION STATES-->
						  <div class="portlet box green">
							 <div class="portlet-title">
								<h4><?php echo $title;?></h4>
							 </div>
							 <div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form action="<?php echo (!empty($rows['slug']))?base_url().'admin/cms/edit/'.$rows['slug']:base_url().'admin/cms/add';?>" method="POST" id="edit_settings" class="form-horizontal" enctype="multipart/form-data">
								   <div class="alert alert-error hide">
									  <button class="close" data-dismiss="alert"></button>
									  You have some form errors. Please check below.
								   </div>
									<?php echo $this->utility->showMsg();?>
									<div class="control-group">
										<label class="control-label">Title*</label>
										<div class="controls">
											<input name="title" type="text" class="span6 m-wrap" value="<?php echo (!empty($rows['title']))?$rows['title']:'';?>" />
										</div>
									</div>
                                                                        <div class="control-group">
										<label class="control-label">Status*</label>
										<div class="controls">
                                                                                    <select tabindex="1" data-placeholder="Page Status?" class="span6 m-wrap" name="status">
                                                                                        <option value="1" <?php echo (!empty($rows['status']))?(($rows['status'])?'selected':''):'';?>>Active</option>
                                                                                        <option value="0" <?php echo (!empty($rows['status']))?((!$rows['status'])?'selected':''):'';?>>Blocked</option>
                                                                                     </select>
										</div>
									</div>
                                                                        <div class="control-group" style="display:none">
										<label class="control-label">Is in Navigation Menu*</label>
										<div class="controls">
                                                                                    <select tabindex="1" data-placeholder="Is in Menu?" class="span6 m-wrap" name="is_in_menu">
                                                                                        <option value="1" <?php echo (!empty($rows['is_in_menu']))?(($rows['is_in_menu'])?'selected':''):'';?>>Yes</option>
                                                                                        <option value="0" <?php echo (empty($rows['is_in_menu']))?'selected':'';?>>No</option>
                                                                                     </select>
										</div>
									</div>
									<?php
									if(!empty($rows['slug']))
									{
									?>
									<div class="control-group" style="display:none">
										<label class="control-label">Page URL*</label>
										<div class="controls">
                                                                                    <div class="input-prepend">
                                                                                        <span class="add-on"><?php echo base_url().'page/'?></span><input type="text" name="slug" placeholder="Page URL" value="<?php echo $rows['slug'];?>" class="span6 m-wrap">
                                                                                    </div>
										</div>
									</div>
									<?php
									}
									?>
                                                                        
                                                                        <div class="control-group">
										<label class="control-label">Description*</label>
										<div class="controls">
                                                                                    <textarea name="descriptionx" class="span6 m-wrap" rows="3"><?php echo (!empty($rows['description']))?$rows['description']:'';?></textarea>
                                                                                     <script>
                                                                                        // Replace the <textarea id="editor1"> with a CKEditor
                                                                                        // instance, using default configuration.
                                                                                        CKEDITOR.replace( 'descriptionx',{
                                                                                                filebrowserBrowseUrl : '/fileman/index.html',
                                                                                                filebrowserImageBrowseUrl : '/fileman/index.html?type=image',
                                                                                                removeDialogTabs: 'link:upload;image:upload'
                                                                                        });
                                                                                    </script>
										</div>
									</div>
                                                                        <div class="control-group">
										<label class="control-label">Short Description*</label>
										<div class="controls">
                                                                                    <textarea name="short_description" class="span6 m-wrap" rows="3"><?php echo (!empty($rows['short_description']))?$rows['short_description']:''?></textarea>
										</div>
									</div>
                                                                        <div class="control-group">
										<label class="control-label">Meta Title</label>
										<div class="controls">
                                                                                    <textarea name="meta_title" class="span6 m-wrap" rows="3"><?php echo (!empty($rows['meta_title']))?$rows['meta_title']:''?></textarea>
										</div>
									</div>
                                                                        <div class="control-group">
										<label class="control-label">Meta Keyword</label>
										<div class="controls">
                                                                                    <textarea name="meta_keyword" class="span6 m-wrap" rows="3"><?php echo (!empty($rows['meta_keyword']))?$rows['meta_keyword']:''?></textarea>
										</div>
									</div>
                                                                        <div class="control-group">
										<label class="control-label">Meta Description</label>
										<div class="controls">
                                                                                    <textarea name="meta_description" class="span6 m-wrap" rows="3"><?php echo (!empty($rows['meta_description']))?$rows['meta_description']:''?></textarea>
										</div>
									</div>
                                                                        <?php
                                                                        if(!empty($rows['date_of_modified']))
                                                                        {
                                                                        ?>
                                                                        <div class="control-group">
										<label class="control-label">Last Modified</label>
										<div class="controls">
                                                                                    <input name="last_modified" type="text" class="span6 m-wrap" value="<?php echo date('d-m-y h:i A',$rows['date_of_modified']);?>" disabled=""/>
										</div>
									</div>
                                                                        <?php
                                                                        }
                                                                        ?>
							   
								   <div class="form-actions">
									  <button type="submit" class="btn green"><?php echo (!empty($rows['slug']))?'Change':'Add New'?></button>
								   </div>
								   <input type="hidden" name="frmSecurity" value="<?php echo $this->utility->getSecurity();?>"/>
								</form>
								<!-- END FORM-->
							 </div>
						  </div>
						  <!-- END VALIDATION STATES-->
					   </div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<script>
			jQuery(document).ready(function() {		
				App.setPage("cms");  // set current page
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>