	<?php echo $header;?>
	<script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
	<script type="text/javascript">
		jQuery(function($){
			var form2 = $('#edit_settings');
			var error2 = $('.alert-error', form2);
			var success2 = $('.alert-success', form2);

			form2.validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-inline', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				ignore: "",
				rules: {
					msg: {
						required: true
					},
                                        status: {
						required: true
					},
				},
				invalidHandler: function (event, validator) { //display error alert on form submit   
					success2.hide();
					error2.show();
					App.scrollTo(error2, -200);
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.help-inline').removeClass('ok'); // display OK icon
					$(element)
						.closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
				},

				unhighlight: function (element) { // revert the change dony by hightlight
					$(element)
						.closest('.control-group').removeClass('error'); // set error class to the control group
				},

				success: function (label) {
					if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
						label
							.closest('.control-group').removeClass('error').addClass('success');
						label.remove(); // remove error label here
					} else { // display success icon for other inputs
						label
							.addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
						.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
					}
				},

				submitHandler: function (form) {
                                        form2.unbind('submit');
					error2.hide();
					form2.submit();
				}

			});

			//apply validation on chosen dropdown value change, this only needed for chosen dropdown integration.
			$('.chosen, .chosen-with-diselect', form2).change(function () {
				form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
			});
		});
	</script>
	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>					
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url().'admin';?>">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url().'admin/notifications/index';?>">All Notifications</a>
								<i class="icon-angle-right"></i>
							</li>
							<?php
							if(!empty($rows['id']))
							{
							?>
							<li><a href="<?php echo base_url().'admin/notifications/edit/'.base64_encode($rows['id']);?>"><?php echo $title;?></a></li>
							<?php
							}
							else
							{
							?>
							<li><a href="<?php echo base_url().'admin/notifications/add';?>"><?php echo $title;?></a></li>
							<?php
							}
							?>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
						  <!-- BEGIN VALIDATION STATES-->
						  <div class="portlet box green">
							 <div class="portlet-title">
								<h4><?php echo $title;?></h4>
							 </div>
							 <div class="portlet-body form">
								<!-- BEGIN FORM-->
                                                                <form action="<?php echo (!empty($rows['id']))?base_url().'admin/notifications/edit/'.  base64_encode($rows['id']):base_url().'admin/notifications/add';?>" method="POST" id="edit_settings" class="form-horizontal" enctype="multipart/form-data">
								   <div class="alert alert-error hide">
									  <button class="close" data-dismiss="alert"></button>
									  You have some form errors. Please check below.
								   </div>
									<?php echo $this->utility->showMsg();?>
									<div class="control-group">
										<label class="control-label">Message*</label>
										<div class="controls">
                                                                                    <textarea name="msg" class="span6 m-wrap" rows="3"><?php echo (!empty($rows['msg']))?$rows['msg']:''?></textarea>
										</div>
									</div>
                                                                        <div class="control-group">
                                                                                    <label class="control-label">Priority*</label>
                                                                                    <div class="controls">
                                                                                        <select class="span6 m-wrap" name="priority">
                                                                                            <option value="3" <?php if(!empty($rows['priority']) && $rows['priority']==3){ ?> selected="selected"<?php }?>>General</option>
                                                                                            <option value="2" <?php if(!empty($rows['priority']) && $rows['priority']==2){ ?> selected="selected"<?php }?>>Medium</option>
                                                                                            <option value="1" <?php if(!empty($rows['priority']) && $rows['priority']==1){ ?> selected="selected"<?php }?>>Urgent</option>
                                                                                         </select>
                                                                                    </div>
                                                                            </div>
                                                                        <div class="control-group">
										<label class="control-label">Status*</label>
										<div class="controls">
                                                                                    <select name="status">
                                                                                        <option value="1" <?php if(!empty($rows['priority']) && $rows['status']==1){ ?> selected="selected" <?php }?>>Active</option>
                                                                                        <option value="2" <?php if(!empty($rows['priority']) && $rows['status']==2){ ?> selected="selected" <?php }?>>Outdated</option>
                                                                                     </select>
										</div>
									</div>
									
                                                                        <?php
                                                                        if(!empty($rows['date_of_post']))
                                                                        {
                                                                        ?>
                                                                        <div class="control-group">
										<label class="control-label">Date of Post</label>
										<div class="controls">
                                                                                    <input name="last_modified" type="text" class="span6 m-wrap" value="<?php echo date('d-m-y h:i A',$rows['date_of_post']);?>" disabled=""/>
										</div>
									</div>
                                                                        <?php
                                                                        }
                                                                        ?>
							   
								   <div class="form-actions">
									  <button type="submit" class="btn green"><?php echo (!empty($rows['id']))?'Change':'Add New'?></button>
								   </div>
								   <input type="hidden" name="frmSecurity" value="<?php echo $this->utility->getSecurity();?>"/>
								</form>
								<!-- END FORM-->
							 </div>
						  </div>
						  <!-- END VALIDATION STATES-->
					   </div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<script>
			jQuery(document).ready(function() {		
				App.setPage("notifications");  // set current page
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>