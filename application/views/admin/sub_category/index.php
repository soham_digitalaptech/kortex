<?php 
        echo $header;
?>
<div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                        <div class="span12">
                                <!-- BEGIN STYLE CUSTOMIZER -->

                                <!-- BEGIN PAGE TITLE & BREADCRUMB-->			
                                <h3 class="page-title">
                                        <?php echo $title; ?>				
                                        <small><?php echo $sub_heading;?></small>
                                </h3>
                                <ul class="breadcrumb">
                                        <li>
                                                <i class="icon-home"></i>
                                                <a href="<?php echo base_url().'admin';?>">Home</a> 
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <li><a href="<?php echo base_url().'admin/project_subcategory';?>"><?php echo $title;?></a></li>
                                        <!-- <li class="pull-right no-text-shadow">
                                                <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                                        <i class="icon-calendar"></i>
                                                        <span></span>
                                                        <i class="icon-angle-down"></i>
                                                </div>
                                        </li> -->
                                </ul>
                                <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="container-fluid">
                        <div class="row-fluid">
                                <div class="span12">
                                  <!-- BEGIN VALIDATION STATES-->
                                  <div class="portlet box green">
                                         <div class="portlet-title">
                                                <h4><?php echo $title;?></h4>
                                         </div>
                                         <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <?php echo $this->utility->showMsg();?>
                                                <h3>All Project Sub Category</h3>
                                                <a href="<?php echo base_url().'admin/project_subcategory/add'?>">
                                                    <button class="btn green pull-right" type="button">Add New</button>
                                                </a>
                                                <table class="table table-hover">
                                                        <thead>
                                                                <tr>
                                                                        <th>#</th>
                                                                        <th>Subcategory Title</th>
                                                                        <th>Subcategory Description</th>
                                                                        <th>Status</th>
                                                                        <th class="hidden-480">Action</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                foreach($rows as $key=>$value)
                                                                {
                                                                ?>
                                                                <tr>
                                                                        <td><?php echo $key+1;?></td>
                                                                        <td><?php echo $value['subcategory_title'];?></td>
                                                                        <td><?php echo $value['subcategory_description'];?></td>
                                                                        
                                                                        <td>
                                                                            <span class="label <?php echo ($value['status'])?'label-success':'label-important';?>">
                                                                                <?php echo ($value['status'])?'Active':'Blocked'?>
                                                                            </span>
                                                                        </td>
                                                                        <td>
                                                                                <a class="btn mini purple" href="<?php echo base_url().'admin/project_subcategory/edit/'.base64_encode($value['id']);?>">
                                                                                        <i class="icon-edit"></i>
                                                                                        Edit
                                                                                </a>
                                                                                <?php 
                                                                                if($value['status'])
                                                                                {
                                                                                ?>
                                                                                <a class="btn mini red" href="<?php echo base_url().'admin/project_subcategory/change_status/'.base64_encode($value['id']).'/'.$this->utility->getSecurity();?>">
                                                                                        <i class="icon-lock"></i>
                                                                                        Blocked It
                                                                                </a>
                                                                                <?php
                                                                                }
                                                                                else
                                                                                {
                                                                                ?>
                                                                                <a class="btn mini green" href="<?php echo base_url().'admin/project_subcategory/change_status/'.base64_encode($value['id']).'/'.$this->utility->getSecurity();?>">
                                                                                        <i class="icon-unlock"></i>
                                                                                        Active Now
                                                                                </a>
                                                                                <?php
                                                                                }
                                                                                ?>
                                                                                
                                                                                <a class="btn mini red confirmME" href="<?php echo base_url().'admin/project_subcategory/delete/'.base64_encode($value['id']).'/'.$this->utility->getSecurity();?>">Delete</a>
                                                                        </td>
                                                                </tr>
                                                                <?php
                                                                }
                                                                if(!$rows)
                                                                        echo '<tr><td colspan="6">No Project Sub Category found<td></tr>';
                                                                ?>

                                                        </tbody>
                                                </table>
                                         </div>
                                  </div>
                                  <!-- END VALIDATION STATES-->
                           </div>
                        </div>
                </div>
        </div>
        <!-- END PAGE CONTAINER-->		
</div>
<script>
        jQuery(document).ready(function() {		
                App.setPage("settings");  // set current page
                App.init(); // init the rest of plugins and elements
        });
</script>
<?php echo $footer;?>