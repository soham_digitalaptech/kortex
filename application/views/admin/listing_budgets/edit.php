	<?php echo $header;?>
	<script src="ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
	<link rel="stylesheet" href="plugins/select2/select2.min.css">
	<script src="plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
		jQuery(function($){
			var form2 = $('#edit_settings');
			var error2 = $('.alert-error', form2);
			var success2 = $('.alert-success', form2);

			form2.validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-inline', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				ignore: "",
				rules: {
					ads_company: {
						required: true
					},
                                        ads_size: {
						required: true
					},
					ads_code: {
						required: true,
					},
					status: {
						required: true
					},
				},
				/*
				messages: { // custom messages for radio buttons and checkboxes
					membership: {
						required: "Please select a Membership type"
					},
					service: {
						required: "Please select  at least 2 types of Service",
						minlength: jQuery.format("Please select  at least {0} types of Service")
					}
				},
				
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter("#form_2_education_chzn");
					} else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
						error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
					} else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
						error.addClass("no-left-padding").insertAfter("#form_2_service_error");
					} else {
						error.insertAfter(element); // for other inputs, just perform default behavoir
					}
				},
				*/
				invalidHandler: function (event, validator) { //display error alert on form submit   
					success2.hide();
					error2.show();
					App.scrollTo(error2, -200);
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.help-inline').removeClass('ok'); // display OK icon
					$(element)
						.closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
				},

				unhighlight: function (element) { // revert the change dony by hightlight
					$(element)
						.closest('.control-group').removeClass('error'); // set error class to the control group
				},

				success: function (label) {
					if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
						label
							.closest('.control-group').removeClass('error').addClass('success');
						label.remove(); // remove error label here
					} else { // display success icon for other inputs
						label
							.addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
						.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
					}
				},

				submitHandler: function (form) {
					error2.hide();
					form2.unbind('submit');
					form2.submit();
				}

			});

			//apply validation on chosen dropdown value change, this only needed for chosen dropdown integration.
			$('.chosen, .chosen-with-diselect', form2).change(function () {
				form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
			});
            $(".select2").select2();
		});
	</script>
	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>					
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url().'admin';?>">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url().'admin/listing_budgets';?>">All Listing Budgets</a>
								<i class="icon-angle-right"></i>
							</li>
							
							<?php
							if(!empty($rows['id']))
							{
							?>
                    			<li><a href="<?php echo base_url().'admin/listing_budgets/edit/'.base64_encode($rows['id']);?>"><?php echo $title;?></a></li>
							<?php
							}
							else
							{
							?>
								<li><a href="<?php echo base_url().'admin/listing_budgets/add';?>"><?php echo $title;?></a></li>
							<?php
							}
							?>
							
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
						  <!-- BEGIN VALIDATION STATES-->
						  <div class="portlet box green">
							<div class="portlet-title">
								<h4><?php echo $title;?></h4>
							</div>
							<div class="portlet-body form">
							
    							<form action="<?php echo (!empty($rows['id']))?base_url().'admin/listing_budgets/edit/'.base64_encode($rows['id']):base_url().'admin/listing_budgets/add';?>" method="POST" id="edit_settings" class="form-horizontal" enctype="multipart/form-data">
								   	
								   	<div class="alert alert-error hide">
									  <button class="close" data-dismiss="alert"></button>
									  You have some form errors. Please check below.
								   	</div>
									<?php echo $this->utility->showMsg();?>
									
									<div class="control-group">
										<label class="control-label">Listing Types*</label>
										<div class="controls">
                                            <select class="form-control select2" multiple="multiple" name="listing_type_id[]" data-placeholder="Select Listing Type" style="width: 100%;">
												<?php
												foreach($listing_types as $value)
												{
													$selected="";
													if(in_array($value['id'],$selected_listings))
														$selected="selected";
												?>
												<option value="<?php echo base64_encode($value['id'])?>" <?php echo $selected;?>><?php echo $value['title'];?></option>
												<?php
												}
												?>
											</select>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Description*</label>
										<div class="controls">
                                           <textarea class="span6 m-wrap" rows="3" name="descriptionx"><?php echo (!empty($rows['description']))?$rows['description']:'';?></textarea>
										   <script>
												// Replace the <textarea id="editor1"> with a CKEditor
												// instance, using default configuration.
												CKEDITOR.replace( 'descriptionx',{
														filebrowserBrowseUrl : '/fileman/index.html',
														filebrowserImageBrowseUrl : '/fileman/index.html?type=image',
														removeDialogTabs: 'link:upload;image:upload'
												});
											</script>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">Price*</label>
										<div class="controls">
                                            <input name="price" type="text" class="span6 m-wrap" value="<?php echo (!empty($rows['price']))?$rows['price']:'';?>" />
										</div>
									</div>
    
                                    <div class="control-group">
										<label class="control-label">Status*</label>
										<div class="controls">
                                            <select tabindex="1" data-placeholder="Page Status?" class="span6 m-wrap" name="status">
                                        		<option value="1" <?php if(!empty($rows['status'])){ if($rows['status']=='1'){?> selected <?php }}?>>Active</option>
                                        		<option value="0" <?php if(!empty($rows['status'])){ if($rows['status']=='0'){?> selected <?php }}?>>Blocked</option>
                                            </select>
										</div>
									</div>
									
								   <div class="form-actions">
									  <button type="submit" class="btn green"><?php echo (!empty($rows['id']))?'Change':'Add'?></button>
								   </div>
								   <input type="hidden" name="frmSecurity" value="<?php echo $this->utility->getSecurity();?>"/>
								</form>
								
							 </div>
						  </div>
						  <!-- END VALIDATION STATES-->
					   </div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<script>
			jQuery(document).ready(function() {		
				App.setPage("cms");  // set current page
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>