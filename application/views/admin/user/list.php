<?php 
    $status=array('Not Verified','Active','Inactive');
    echo $header;
?>
<div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                        <div class="span12">
                                <!-- BEGIN STYLE CUSTOMIZER -->

                                <!-- BEGIN PAGE TITLE & BREADCRUMB-->			
                                <h3 class="page-title">
                                        <?php echo $title; ?>				
                                        <small><?php echo $sub_heading;?></small>
                                </h3>
                                <ul class="breadcrumb">
                                        <li>
                                                <i class="icon-home"></i>
                                                <a href="<?php echo base_url().'admin';?>">Home</a> 
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <li><a href="<?php echo base_url().'admin/user';?>"><?php echo $title;?></a></li>
                                        <li class="pull-right no-text-shadow">
                                                <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                                        <i class="icon-calendar"></i>
                                                        <span></span>
                                                        <i class="icon-angle-down"></i>
                                                </div>
                                        </li>
                                </ul>
                                <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="container-fluid">
                        <div class="row-fluid">
                                <div class="span12">
                                  <!-- BEGIN VALIDATION STATES-->
                                  <div class="portlet box green">
                                         <div class="portlet-title">
                                                <h4><?php echo $title;?></h4>
                                         </div>
                                         <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <?php echo $this->utility->showMsg();?>
                                                <h3>All Users</h3>
                                                <div class="row-fluid">
                                                        <a class="icon-btn span2 pull-right" href="<?php echo base_url().'admin/user/add'?>">
                                                            <i class="icon-plus"></i>
                                                            <div>Add Users</div>
                                                        </a>
                                                        <a class="icon-btn span2 pull-right" href="<?php echo base_url().'admin/user/account_types'?>">
                                                            <i class="icon-sitemap"></i>
                                                            <div>Account Type</div>
                                                        </a>
                                                </div>
                                                <table class="table table-hover">
                                                        <thead>
                                                                <tr>
                                                                        <th>#</th>
                                                                        <th>Username</th>
                                                                        <th>Name</th>
                                                                        <th>Email</th>
                                                                        <th>Account Type</th>
                                                                        <th>Status</th>
                                                                        <th>Last Login</th>
                                                                        <th class="no_sort">Action</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                if(!empty($users))
                                                                foreach($users as $key=>$value)
                                                                {
                                                                ?>
                                                                    <tr>
                                                                        <td><?php echo $key+1;?></td>
                                                                        <td><?php echo $value['username'];?></td>
                                                                        <td><?php echo $value['name'];?></td>
                                                                        <td><?php echo $value['email'];?></td>
                                                                        <td><?php echo $value['user_types'][0]['type_name'];?></td>
                                                                        <td><span class="label <?php echo ($value['status']==0)?'label-warning':($value['status']==1)?'label-success':'label-important'?>"><?php echo $status[$value['status']];?></span></td>
                                                                        <td><?php echo (!empty($value['last_login']))?date('d-m-Y h:i A',$value['last_login']):'';?></td>
                                                                        <td>
                                                                            <a href="<?php echo base_url().'admin/user/edit/'.base64_encode($value['id'])?>"><span class="label label-info"><i class="fa fa-edit"></i> Edit</span></a>
        <?php if($value['user_type_id']!=1):?>
            <a href="<?php echo base_url().'admin/user/permission/'.base64_encode($value['id']);?>">
                <span class="label label-default"><i class="fa fa-database"></i> Permission</span>
            </a>
        <?php endif;?>

                        <?php if($value['user_type_id']!=1):?>
                            <a href="<?php echo base_url().'admin/wallets/wallet_details/'.base64_encode($value['id']);?>">
                                <span class="label label-default"><i class="fa fa-database"></i> Wallet Details</span>
                            </a>
                        <?php endif;?>    

                        <?php if($value['user_type_id']!=1):?>
                            <a class="" href="<?php echo base_url().'admin/user/affiliates/'.base64_encode($value['id']).'/'.$this->utility->getSecurity()?>">                               
                                <span class="label <?php echo ($value['isAffiliate']==0)?'label-important':'label-success'?>">
                                    <?php echo ($value['isAffiliate']==0)?'Affiliate Registration':'Affiliate Details'?>
                                </span>
                            </a>
                        <?php endif;?>
                                                                            
                        <?php if($value['user_type_id']!=1):?>
                            <a href="<?php echo base_url().'admin/user/change_status/'.base64_encode($value['id']).'/'.$this->utility->getSecurity();?>">               
                                <span class="label <?php echo ($value['status']==0 || $value['status']==2)?'label-success':'label-warning'?>">
                                    <?php echo ($value['status']==0 || $value['status']==2)?'<i class="fa fa-unlock"></i> Turn Active':'<i class="fa fa-lock"></i> Turn Inactive'?>
                                </span>
                            </a>
                        <?php endif;?>

        <?php if($value['user_type_id']!=1):?>
            <a class="confirmME" href="<?php echo base_url().'admin/user/delete/'.base64_encode($value['id']).'/'.$this->utility->getSecurity()?>">
                <span class="label label-important"><i class="fa fa-trash"></i> Delete</span>
            </a>
        <?php endif;?>

                                                                        </td>
                                                                    </tr>
                                                                <?php
                                                                }
                                                                if(!$users)
                                                                        echo '<tr><td colspan="6">No Users found<td></tr>';
                                                                ?>

                                                        </tbody>
                                                </table>
                                         </div>
                                  </div>
                                  <!-- END VALIDATION STATES-->
                           </div>
                        </div>
                </div>
        </div>
        <!-- END PAGE CONTAINER-->		
</div>
<script>
        jQuery(document).ready(function() {		
                App.setPage("settings");  // set current page
                App.init(); // init the rest of plugins and elements
        });
</script>
<?php echo $footer;?>