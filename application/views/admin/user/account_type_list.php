<?php 
    $status=array('Not Verified','Active','Inactive');
    echo $header;
?>
<div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                        <div class="span12">
                                <!-- BEGIN STYLE CUSTOMIZER -->

                                <!-- BEGIN PAGE TITLE & BREADCRUMB-->			
                                <h3 class="page-title">
                                        <?php echo $title; ?>				
                                        <small><?php echo $sub_heading;?></small>
                                </h3>
                                <ul class="breadcrumb">
                                        <li>
                                                <i class="icon-home"></i>
                                                <a href="<?php echo base_url().'admin';?>">Home</a> 
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url().'admin/user';?>">All User</a>
                                            <i class="icon-angle-right"></i>
                                        </li>
                                        <li><a href="<?php echo base_url().'admin/user/account_types';?>"><?php echo $title;?></a></li>
                                        <li class="pull-right no-text-shadow">
                                            <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                                <i class="icon-calendar"></i>
                                                <span></span>
                                                <i class="icon-angle-down"></i>
                                            </div>
                                        </li>
                                </ul>
                                <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="container-fluid">
                        <div class="row-fluid">
                                <div class="span12">
                                  <!-- BEGIN VALIDATION STATES-->
                                  <div class="portlet box green">
                                         <div class="portlet-title">
                                                <h4><?php echo $title;?></h4>
                                         </div>
                                         <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <?php echo $this->utility->showMsg();?>
                                                <h3>All User Account Type</h3>
                                                <div class="row-fluid">
                                                        <a class="icon-btn span2 pull-right" href="<?php echo base_url().'admin/user/account_types/add'?>">
                                                            <i class="icon-plus"></i>
                                                            <div>Add Type</div>
                                                        </a>
                                                </div>
                                                <table class="table table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Type Name</th>
                                                                <th class="no_sort">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                if(!empty($types))
                                                                {
                                                                    foreach($types as $key=>$value)
                                                                    {
                                                                    ?>
                                                                        <tr>
                                                                            <td><?php echo $key+1;?></td>
                                                                            <td><?php echo $value['type_name'];?></td>
                                                                            <td>
                                                                                <a href="<?php echo base_url().'admin/user/account_types/edit/'.base64_encode($value['id'])?>"><span class="label label-info"><i class="fa fa-edit"></i> Edit</span></a>
                                                                                <?php if($value['id']!=1):?><a class="confirmME" href="<?php echo base_url().'admin/user/account_types/delete/'.base64_encode($value['id']).'/'.$this->utility->getSecurity()?>"><span class="label label-important"><i class="fa fa-trash"></i> Delete</span></a><?php endif;?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php
                                                                    }
                                                                }
                                                                else
                                                                    echo '<tr>'
                                                                    . '<td>No Account Type are found</td></tr>';
                                                                ?>
                                                        </tbody>
                                                </table>
                                         </div>
                                  </div>
                                  <!-- END VALIDATION STATES-->
                           </div>
                        </div>
                </div>
        </div>
        <!-- END PAGE CONTAINER-->		
</div>
<script>
        jQuery(document).ready(function() {		
                App.setPage("settings");  // set current page
                App.init(); // init the rest of plugins and elements
        });
</script>
<?php echo $footer;?>