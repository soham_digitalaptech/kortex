<?php echo $header;?>
<script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
        jQuery(function($){
                var form2 = $('#edit_settings');
                var error2 = $('.alert-error', form2);
                var success2 = $('.alert-success', form2);
                form2.validate({
                        errorElement: 'span', //default input error message container
                        errorClass: 'help-inline', // default input error message class
                        focusInvalid: false, // do not focus the last invalid input
                        ignore: "",
                        rules: {
                            type_name: {
                                required: true,
                            },
                        },
                        /*
                        messages: { // custom messages for radio buttons and checkboxes
                                membership: {
                                        required: "Please select a Membership type"
                                },
                                service: {
                                        required: "Please select  at least 2 types of Service",
                                        minlength: jQuery.format("Please select  at least {0} types of Service")
                                }
                        },

                        errorPlacement: function (error, element) { // render error placement for each input type
                                if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
                                        error.insertAfter("#form_2_education_chzn");
                                } else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
                                        error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
                                } else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
                                        error.addClass("no-left-padding").insertAfter("#form_2_service_error");
                                } else {
                                        error.insertAfter(element); // for other inputs, just perform default behavoir
                                }
                        },
                        */
                        invalidHandler: function (event, validator) { //display error alert on form submit   
                                success2.hide();
                                error2.show();
                                App.scrollTo(error2, -200);
                        },

                        highlight: function (element) { // hightlight error inputs
                                $(element)
                                        .closest('.help-inline').removeClass('ok'); // display OK icon
                                $(element)
                                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                        },

                        unhighlight: function (element) { // revert the change dony by hightlight
                                $(element)
                                        .closest('.control-group').removeClass('error'); // set error class to the control group
                        },

                        success: function (label) {
                                if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
                                        label
                                                .closest('.control-group').removeClass('error').addClass('success');
                                        label.remove(); // remove error label here
                                } else { // display success icon for other inputs
                                        label
                                                .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                                }
                        },

                        submitHandler: function (form) {
                                error2.hide();
                                form2.unbind('submit');
                                form2.submit();
                        }

                });
        });
</script>
<!-- BEGIN PAGE -->
<div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                        <div class="span12">
                                <!-- BEGIN STYLE CUSTOMIZER -->

                                <!-- BEGIN PAGE TITLE & BREADCRUMB-->			
                                <h3 class="page-title">
                                        <?php echo $title; ?>					
                                        <small><?php echo $sub_heading;?></small>
                                </h3>
                                <ul class="breadcrumb">
                                        <li>
                                                <i class="icon-home"></i>
                                                <a href="<?php echo base_url().'admin';?>">Home</a> 
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <li>
                                                <a href="<?php echo base_url().'admin/user';?>">All User</a>
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <li>
                                                <a href="<?php echo base_url().'admin/user/account_types';?>">All Account Types</a>
                                        </li>
                                        <?php
                                        if(empty($id))
                                        {
                                        ?>
                                        <li>
                                                <a href="<?php echo base_url().'admin/user/account_types/add';?>">Add New Types</a>
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                        <li>
                                                <a href="<?php echo base_url().'admin/user/account_types/edit/'.$id;?>"><?php echo 'Edit '.$rows['type_name'];?></a>
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <?php
                                        }
                                        ?>
                                        <li class="pull-right no-text-shadow">
                                                <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                                        <i class="icon-calendar"></i>
                                                        <span></span>
                                                        <i class="icon-angle-down"></i>
                                                </div>
                                        </li>
                                </ul>
                                <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="container-fluid">
                        <div class="row-fluid">
                                <div class="span12">
                                  <!-- BEGIN VALIDATION STATES-->
                                  <div class="portlet box green">
                                         <div class="portlet-title">
                                                <h4><?php echo $title;?></h4>
                                         </div>
                                         <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo (!empty($id))?base_url().'admin/user/account_types/edit/'.$id:base_url().'admin/user/account_types/add'?>" method="POST" id="edit_settings" class="form-horizontal" enctype="multipart/form-data">
                                                   <div class="alert alert-error hide">
                                                          <button class="close" data-dismiss="alert"></button>
                                                          You have some form errors. Please check below.
                                                   </div>
                                                        <?php echo $this->utility->showMsg();?>


                                                        <div class="control-group">
                                                                <label class="control-label">Type Name *</label>
                                                                <div class="controls">
                                                                    <input name="type_name" type="text" class="span6 m-wrap" value="<?php echo (!empty($rows['type_name']))?$rows['type_name']:'';?>" />
                                                                </div>
                                                        </div>
                                                        
                                                        <div class="form-actions">
                                                               <button type="submit" class="btn green"><?php echo (!empty($id))?'Change':'Add'?></button>
                                                        </div>
                                                        <input type="hidden" name="frmSecurity" value="<?php echo $this->utility->getSecurity();?>"/>
                                                </form>
                                                <!-- END FORM-->
                                         </div>
                                  </div>
                                  <!-- END VALIDATION STATES-->
                           </div>
                        </div>
                </div>
        </div>
        <!-- END PAGE CONTAINER-->		
    </div>
    <script>
            jQuery(document).ready(function() {		
                    App.setPage("cms");  // set current page
                    App.init(); // init the rest of plugins and elements
            });
    </script>
    <?php echo $footer;?>