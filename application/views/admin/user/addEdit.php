<?php echo $header;?>
<script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
        jQuery(function($){
                var form2 = $('#edit_settings');
                var error2 = $('.alert-error', form2);
                var success2 = $('.alert-success', form2);
                var loadPassword = function()
                {
                    var div=$('#template_1').html();
                    $('#password_container').html(div);
                };
                var clearPassword = function()
                {
                    $('#password_container').html('');
                };
                form2.validate({
                        errorElement: 'span', //default input error message container
                        errorClass: 'help-inline', // default input error message class
                        focusInvalid: false, // do not focus the last invalid input
                        ignore: "",
                        rules: {
                            password: {
                                minlength: 6,
                                required: true
                            },
                            username: {
                                required: true,
                            },
                            con_password: {
                                minlength: 6,
                                required: true,
                                equalTo:'#password',
                            },
                            user_type_id: {
                                required: true,
                            },
                            status: {
                                required: true,
                            },
                        },
                        /*
                        messages: { // custom messages for radio buttons and checkboxes
                                membership: {
                                        required: "Please select a Membership type"
                                },
                                service: {
                                        required: "Please select  at least 2 types of Service",
                                        minlength: jQuery.format("Please select  at least {0} types of Service")
                                }
                        },

                        errorPlacement: function (error, element) { // render error placement for each input type
                                if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
                                        error.insertAfter("#form_2_education_chzn");
                                } else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
                                        error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
                                } else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
                                        error.addClass("no-left-padding").insertAfter("#form_2_service_error");
                                } else {
                                        error.insertAfter(element); // for other inputs, just perform default behavoir
                                }
                        },
                        */
                        invalidHandler: function (event, validator) { //display error alert on form submit   
                                success2.hide();
                                error2.show();
                                App.scrollTo(error2, -200);
                        },

                        highlight: function (element) { // hightlight error inputs
                                $(element)
                                        .closest('.help-inline').removeClass('ok'); // display OK icon
                                $(element)
                                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                        },

                        unhighlight: function (element) { // revert the change dony by hightlight
                                $(element)
                                        .closest('.control-group').removeClass('error'); // set error class to the control group
                        },

                        success: function (label) {
                                if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
                                        label
                                                .closest('.control-group').removeClass('error').addClass('success');
                                        label.remove(); // remove error label here
                                } else { // display success icon for other inputs
                                        label
                                                .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                                }
                        },

                        submitHandler: function (form) {
                                error2.hide();
                                form2.unbind('submit');
                                form2.submit();
                        }

                });

                $('input[name="is_master_account"]').on('ifClicked', function (event) {
                    if(this.value=="0")
                        $('select[name=master_account_customer_id]').show();
                    else if(this.value=="1")
                    {
                        $('select[name=master_account_customer_id]').hide();
                    }
                });
                $('#change_password').click(function(){
                    if($(this).attr('status')=="false")
                    {
                        loadPassword();
                        $('#change_password').attr('status','true');
                        $('#change_password').html('Off Password?');
                    }
                    else
                    {
                        clearPassword();
                        $('#change_password').attr('status','false');
                        $('#change_password').html('Change Password?');
                    }
                });
                $('select[name=user_type_id]').change(function(){
                    if($(this).val()==-99)
                        window.location.href = '<?php echo base_url().'admin/user/account_types/add'?>';
                });
                <?php
                if(empty($admin_id))
                {
                ?>
                    loadPassword();
                <?php
                }
                ?>
        });
</script>
<!-- BEGIN PAGE -->
<div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                        <div class="span12">
                                <!-- BEGIN STYLE CUSTOMIZER -->

                                <!-- BEGIN PAGE TITLE & BREADCRUMB-->			
                                <h3 class="page-title">
                                        <?php echo $title; ?>					
                                        <small><?php echo $sub_heading;?></small>
                                </h3>
                                <ul class="breadcrumb">
                                        <li>
                                                <i class="icon-home"></i>
                                                <a href="<?php echo base_url().'admin';?>">Home</a> 
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <li>
                                                <a href="<?php echo base_url().'admin/user';?>">All User</a>
                                                <i class="icon-angle-right"></i>
                                        </li>
                                        <?php
                                        if(!empty($rows['id']))
                                        {
                                        ?>
                                        <li><a href="<?php echo base_url().'admin/user/edit/'.base64_encode($user['id']);?>"><?php echo 'Editing ('.$user['username'].')';?></a></li>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                        <li><a href="<?php echo base_url().'admin/user/add';?>"><?php echo $title;?></a></li>
                                        <?php
                                        }
                                        ?>
                                        <li class="pull-right no-text-shadow">
                                                <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                                        <i class="icon-calendar"></i>
                                                        <span></span>
                                                        <i class="icon-angle-down"></i>
                                                </div>
                                        </li>
                                </ul>
                                <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="container-fluid">
                        <div class="row-fluid">
                                <div class="span12">
                                  <!-- BEGIN VALIDATION STATES-->
                                  <div class="portlet box green">
                                         <div class="portlet-title">
                                                <h4><?php echo $title;?></h4>
                                         </div>
                                         <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo (!empty($admin_id))?base_url().'admin/user/edit/'.$admin_id:base_url().'admin/user/add'?>" method="POST" id="edit_settings" class="form-horizontal" enctype="multipart/form-data">
                                                   <div class="alert alert-error hide">
                                                          <button class="close" data-dismiss="alert"></button>
                                                          You have some form errors. Please check below.
                                                   </div>
                                                        <?php echo $this->utility->showMsg();?>


                                                        <div class="control-group">
                                                                <label class="control-label">Username *</label>
                                                                <div class="controls">
                                                                    <input name="username" type="text" class="span6 m-wrap" value="<?php echo (!empty($user['username']))?$user['username']:'';?>" />
                                                                </div>
                                                        </div>
                                                        <?php
                                                        if(!empty($admin_id))
                                                        {
                                                        ?>
                                                        <div class="control-group">
                                                                <label class="control-label"></label>
                                                                <div class="controls">
                                                                    <button type="button" id="change_password" status="false" class="btn btn-info btn-sm">Change Password?</button>
                                                                </div>
                                                        </div>
                                                        <?php
                                                        }
                                                        ?>
                                                        <div id="password_container"></div>
                                                        <div class="control-group">
                                                                <label class="control-label">Name *</label>
                                                                <div class="controls">
                                                                    <input name="name" type="text" class="span6 m-wrap" value="<?php echo (!empty($user['name']))?$user['name']:'';?>" />
                                                                </div>
                                                        </div>
                                                        <div class="control-group">
                                                                <label class="control-label">E Mail *</label>
                                                                <div class="controls">
                                                                    <input name="email" type="text" class="span6 m-wrap" value="<?php echo (!empty($user['email']))?$user['email']:'';?>" />
                                                                </div>
                                                        </div>
                                                        <?php
                                                        $showRight=true;
                                                        if(!empty($admin_id))
                                                        {
                                                            if($user['id']==$this->session->userdata('admin_id'))
                                                                $showRight=false;
                                                        }
                                                        if($showRight)
                                                        {
                                                        ?>
                                                        <div class="control-group">
                                                            <label class="control-label">User Type*</label>
                                                            <div class="controls">
                                                                <select tabindex="1" data-placeholder="User Type" class="span6 m-wrap" name="user_type_id">
                                                                    <option value="">Select User Type</option>
                                                                    <?php
                                                                    foreach($admin_type as $value)
                                                                    {
                                                                        $selected="";
                                                                        if(!empty($user['user_type_id']))
                                                                        {
                                                                            if($user['user_type_id']==$value['id'])
                                                                                $selected="selected";
                                                                        }
                                                                    ?>
                                                                    <option value="<?php echo $value['id']?>" <?php echo $selected;?>><?php echo $value['type_name'];?></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    <option value="-99">+ Add new User Type</option>
                                                                 </select>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                                <label class="control-label">Status*</label>
                                                                <div class="controls">
                                                                    <select tabindex="1" data-placeholder="Page Status?" class="span6 m-wrap" name="status">
                                                                        <option value="1" <?php echo (!empty($user['status']))?($user['status']==1)?'selected':'':'';?>>Active</option>
                                                                        <option value="2" <?php echo (!empty($user['status']))?($user['status']==2)?'selected':'':'';?>>Blocked</option>
                                                                     </select>
                                                                </div>
                                                        </div>
                                                        <?php
                                                        }
                                                        ?>
                                                   <div class="form-actions">
                                                          <button type="submit" class="btn green"><?php echo (!empty($user['id']))?'Change':'Add'?></button>
                                                   </div>
                                                   <input type="hidden" name="frmSecurity" value="<?php echo $this->utility->getSecurity();?>"/>
                                                </form>
                                                <!-- END FORM-->
                                         </div>
                                  </div>
                                  <!-- END VALIDATION STATES-->
                           </div>
                        </div>
                </div>
        </div>
        <!-- END PAGE CONTAINER-->		
    </div>
    <script>
            jQuery(document).ready(function() {		
                    App.setPage("cms");  // set current page
                    App.init(); // init the rest of plugins and elements
            });
    </script>
    <?php echo $footer;?>

<div id="template_1" style="display: none;">
    <div class="control-group">
        <label class="control-label">Password *</label>
        <div class="controls">
            <input id="password" name="password" type="password" class="span6 m-wrap" value="" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Confirm Password *</label>
        <div class="controls">
            <input name="con_password" type="password" class="span6 m-wrap" value="" />
        </div>
    </div>
</div>

