<div id="dashboard" style="display:none">
				<div class="row-fluid">
					<div class="span6">
						<!-- BEGIN PORTLET-->
						<div class="portlet solid bordered light-grey">
							<div class="portlet-title">
								<h4><i class="icon-bar-chart"></i>Site Visits</h4>
								<div class="tools">
									<div class="btn-group pull-right" data-toggle="buttons-radio">
										<a href="javascript:;" class="btn mini">Users</a>
										<a href="javascript:;" class="btn mini active">Feedbacks</a>
									</div>
								</div>
							</div>
							<div class="portlet-body">
								<div id="site_statistics_loading">
									<img src="assets/img/loading.gif" alt="loading" />
								</div>
								<div id="site_statistics_content" class="hide">
									<div id="site_statistics" class="chart"></div>
								</div>
							</div>
						</div>
						<!-- END PORTLET-->
					</div>
					<div class="span6">
						<!-- BEGIN PORTLET-->
						<div class="portlet solid light-grey bordered">
							<div class="portlet-title">
								<h4><i class="icon-bullhorn"></i>Activities</h4>
								<div class="tools">
									<div class="btn-group pull-right" data-toggle="buttons-radio">
										<a href="javascript:;" class="btn blue mini active">Users</a>
										<a href="javascript:;" class="btn blue mini">Orders</a>
									</div>
								</div>
							</div>
							<div class="portlet-body">
								<div id="site_activities_loading">
									<img src="assets/img/loading.gif" alt="loading" />
								</div>
								<div id="site_activities_content" class="hide">
									<div id="site_activities" style="height:100px;"></div>
								</div>
							</div>
						</div>
						<!-- END PORTLET-->
						<!-- BEGIN PORTLET-->
						<div class="portlet solid bordered light-grey">
							<div class="portlet-title">
								<h4><i class="icon-signal"></i>Server Load</h4>
								<div class="tools">
									<div class="btn-group pull-right" data-toggle="buttons-radio">
										<a href="" class="btn red mini active">
										<span class="hidden-phone">Database</span>
										<span class="visible-phone">DB</span></a>
										<a href="javascript:;" class="btn red mini">Web</a>
									</div>
								</div>
							</div>
							<div class="portlet-body">
								<div id="load_statistics_loading">
									<img src="assets/img/loading.gif" alt="loading" />
								</div>
								<div id="load_statistics_content" class="hide">
									<div id="load_statistics" style="height:108px;"></div>
								</div>
							</div>
						</div>
						<!-- END PORTLET-->
					</div>
				</div>
			</div>