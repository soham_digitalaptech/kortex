	<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li>
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!--
					<form class="sidebar-search">
						<div class="input-box">
							<a href="javascript:;" class="remove"></a>
							<input type="text" placeholder="Search..." />				
							<input type="button" class="submit" value=" " />
						</div>
					</form>
					-->
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				
				<li class="start ">
					<a href="<?php echo base_url().'admin';?>">
					<i class="icon-home"></i> 
					<span class="title">Dashboard</span>
					<span class=""></span>
					</a>
				</li>
                
                <li class="start ">
					<a href="<?php echo base_url().'admin/user';?>">
					<i class="icon-user"></i> 
					<span class="title">User Manager</span>
					<span class=""></span>
					</a>
				</li>
                <!--
                <li class="start ">
					<a href="<?php echo base_url().'admin/menu';?>">
					<i class="icon-list"></i> 
					<span class="title">Menu Manager</span>
					<span class=""></span>
					</a>
				</li>
                -->
                
                <li class="start ">
					<a href="<?php echo base_url().'admin/slider_master';?>">
					<i class="icon-film"></i> 
					<span class="title">Slider Gallery</span>
					<span class=""></span>
					</a>
				</li>
                                
                <li class="start ">
					<a href="<?php echo base_url().'admin/cms';?>">
					<i class="icon-file"></i> 
					<span class="title">CMS Pages</span>
					<span class=""></span>
					</a>
				</li>
				<li class="start ">
					<a href="<?php echo base_url().'admin/ads';?>">
					<i class="icon-bar-chart"></i> 
					<span class="title">Ads Manager</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/packages';?>">
					<i class="icon-th-large"></i> 
					<span class="title">Packages Manager</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/affiliate_manager';?>">
					<i class="icon-qrcode"></i> 
					<span class="title">Affiliate Manager</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/skills';?>">
					<i class="icon-trophy"></i> 
					<span class="title">Skill Manager</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/project_category';?>">
					<i class="icon-columns"></i> 
					<span class="title">Project Category</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/project_subcategory';?>">
					<i class="icon-table"></i> 
					<span class="title">Project Sub Category</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/static_project_titles';?>">
					<i class="icon-pushpin"></i> 
					<span class="title">Static Project Titles</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/project_budget_ranges';?>">
					<i class="icon-money"></i> 
					<span class="title">Project Budget Ranges</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/listing_types';?>">
					<i class="icon-sitemap"></i> 
					<span class="title">Listing Types</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/experience_level';?>">
					<i class="icon-list-alt"></i> 
					<span class="title">Experience Levels</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/widget_manager';?>">
					<i class="icon-list-alt"></i> 
					<span class="title">Widget Management</span>
					<span class=""></span>
					</a>
				</li>

				<li class="start">
					<a href="<?php echo base_url().'admin/listing_budgets';?>">
					<i class="icon-bullhorn"></i> 
					<span class="title">Listing Budgets</span>
					<span class=""></span>
					</a>
				</li>

                                
                <li class="start">
					<a href="<?php echo base_url().'admin/notifications';?>">
					<i class="icon-bullhorn"></i> 
					<span class="title">Notification</span>
					<span class=""></span>
					</a>
				</li>

				
				<li class="has-sub">
					<a href="javascript:;">
						<i class="icon-cogs"></i>
						<span class="title">Settings</span>
						<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="<?php echo base_url().'admin/settings/general';?>">General Settings</a></li>
						<li><a href="<?php echo base_url().'admin/settings/change_password';?>">Change Password</a></li>
					</ul>
				</li>
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->