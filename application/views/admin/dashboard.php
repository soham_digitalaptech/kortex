	<?php echo $header;?>
	<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $title; ?>				
							<small><?php echo $sub_heading;?></small>
						</h3>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'admin/home';?>"><?php echo $title;?></a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
					<?php echo $this->utility->showMsg();?>
					<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
                                                <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat yellow">
								<div class="visual">
									<i class="icon-list"></i>
								</div>
								<div class="details">
									<div class="number"></div>
									<div class="desc">Menu Manager</div>
								</div>
								<a class="more" href="<?php echo base_url().'admin/menu';?>">
								View <i class="m-icon-swapright m-icon-white"></i>
								</a>						
							</div>
						</div>
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat blue">
								<div class="visual">
									<i class="icon-film"></i>
								</div>
								<div class="details">
									<div class="number">
										
									</div>
									<div class="desc">									
										Slider Manager
									</div>
								</div>
								<a class="more" href="<?php echo base_url()?>admin/slider">
								View <i class="m-icon-swapright m-icon-white"></i>
								</a>						
							</div>
						</div>
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat red">
								<div class="visual">
									<i class="icon-file"></i>
								</div>
								<div class="details">
									<div class="number"></div>
									<div class="desc">CMS Pages</div>
								</div>
								<a class="more" href="<?php echo base_url().'admin/cms';?>">
								View <i class="m-icon-swapright m-icon-white"></i>
								</a>						
							</div>
						</div>
						<div class="span3 responsive" data-tablet="span6  fix-offset" data-desktop="span3">
							<div class="dashboard-stat purple">
								<div class="visual">
									<i class="icon-bar-chart"></i>
								</div>
								<div class="details">
									<div class="number"></div>
									<div class="desc">Ads Manager</div>
								</div>
								<a class="more" href="<?php echo base_url()?>admin/ads">
								View <i class="m-icon-swapright m-icon-white"></i>
								</a>						
							</div>
						</div>
                                                <div class="clearfix"></div>
                                                <div style="margin-left: 0;" class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat pink">
								<div class="visual">
									<i class="icon-bullhorn"></i>
								</div>
								<div class="details">
									<div class="number"></div>
									<div class="desc">Notification</div>
								</div>
								<a class="more" href="<?php echo base_url().'admin/notifications';?>">
								View <i class="m-icon-swapright m-icon-white"></i>
								</a>						
							</div>
						</div>
                                                <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat gray">
								<div class="visual">
									<i class="icon-cogs"></i>
								</div>
								<div class="details">
									<div class="number"></div>
									<div class="desc">General Settings</div>
								</div>
								<a class="more" href="<?php echo base_url().'admin/settings/general';?>">
								View <i class="m-icon-swapright m-icon-white"></i>
								</a>						
							</div>
						</div>
					</div>					
				</div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<script>
			jQuery(document).ready(function() {		
				App.init(); // init the rest of plugins and elements
			});
		</script>
		<?php echo $footer;?>