<footer class="footer_bg">
    <div class="footer_inner">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7">
                    <div class="footer_left">
                        <div class="row">

                            <div class="col-sm-3">
                                <div class="footer_listblock">
                                    <h4>Network</h4>
                                    <ul class="footer_list">
                                        <li><a href="javascript:void(0)">Projects</a></li>
                                        <li><a href="javascript:void(0)">Contests</a></li>
                                        <li><a href="javascript:void(0)">Sitemap</a></li>
                                        <li><a href="javascript:void(0)">Freelancer Local</a></li>
                                        <li><a href="javascript:void(0)">Services</a></li>
                                        <li><a href="javascript:void(0)">Escrow</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="footer_listblock">
                                    <h4>About</h4>
                                    <ul class="footer_list">
                                        <li><a href="javascript:void(0)">About us</a></li>
                                        <li><a href="javascript:void(0)">How it Works</a></li>
                                        <li><a href="javascript:void(0)">Team</a></li>
                                        <li><a href="javascript:void(0)">Security</a></li>
                                        <li><a href="javascript:void(0)">Report Bug</a></li>
                                        <li><a href="javascript:void(0)">Fees &amp; Charges</a></li>
                                        <li><a href="javascript:void(0)">Investor</a></li>
                                        <li><a href="javascript:void(0)">Quotes</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="footer_listblock">
                                    <h4>PRESS</h4>
                                    <ul class="footer_list">
                                        <li><a href="javascript:void(0)">In the News</a></li>
                                        <li><a href="javascript:void(0)">Press Releases</a></li>
                                        <li><a href="javascript:void(0)">Testimonials</a></li>
                                        <li><a href="javascript:void(0)">Timeline</a></li>                        
                                    </ul>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="footer_listblock">
                                    <h4>GET IN TOUCH</h4>
                                    <ul class="footer_list">
                                        <li><a href="javascript:void(0)">Get Support</a></li>
                                        <li><a href="javascript:void(0)">Advertise with Us</a></li>
                                        <li><a href="javascript:void(0)">Careers</a></li>
                                        <li><a href="javascript:void(0)">Community</a></li>
                                        <li><a href="javascript:void(0)">Affiliate Program</a></li>
                                        <li><a href="javascript:void(0)">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-md-4">
                    <div class="footer_listblock clearfix">
                        <h4>Follow Us</h4>
                        <a class="footer_social" href="javascript:void(0)"><img alt="" src="images/fb.png"></a>
                        <a class="footer_social" href="javascript:void(0)"><img alt="" src="images/tw.png"></a>
                        <a class="footer_social" href="javascript:void(0)"><img alt="" src="images/pint.png"></a>
                        <a class="footer_social" href="javascript:void(0)"><img alt="" src="images/gplus.png"></a>
                        <a class="footer_social" href="javascript:void(0)"><img alt="" src="images/linkedin.png"></a>

                        <div class="btn-group lang_btn">
                            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-default language_btn dropdown-toggle" type="button"><span class="glyphicon glyphicon-globe"></span> US (International) / English </button>
                            <!-- <ul class="dropdown-menu">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#">Separated link</a></li>
                            </ul> -->
                        </div>

                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="footer_buttom">                  
                        <ul class="footer_buttomlink">
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms and Conditions </a></li>
                            <li><a href="#">Copyright Infringement Policy</a></li>
                            <li><a href="#">Code of Conduct</a></li>
                        </ul>
                        <p class="copyright">Copyright &copy; 2016 KORTX</p>

                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
</div>
</body>
</html>
