<html>
    <head>
        <base href="<?php echo base_url();?>"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="images/favicon.ico">

        <title><?php echo $title;?></title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet"> 

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet"> 
        <link href="fonts/fonts.css" rel="stylesheet"> 
        <link href="css/responsive.css" rel="stylesheet"> 


        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>   

    </head>
    <div class="innerwrapper-bg">
        <img src="images/reg-bg.png" alt="" class="reg_bg">
        <header class="log_header">
            <div class="header-container">
                <div class="row">
                    <div class="col-sm-4">
                        <a href="#" class="logo"><img src="images/logo.png" alt=""></a>
                    </div>
                    <div class="col-sm-8">
                        <div class="action_btns">                        
                            <a class="toplink register" href="#"><img alt="" src="images/login.png"> <span>Login</span></a>
                        </div>  
                    </div>
                </div>
            </div>  

        </header>