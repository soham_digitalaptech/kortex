<!DOCTYPE html>
<html>
    <head>
        <base href="<?php echo base_url(); ?>"/>
        <title><?php echo $title; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <style type="text/css">
            .error{
                color:red;
            }
        </style>
        <script src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/common.js"></script>
    </head>
    <body>
        <div style="float: right">
            <nav>
                <a href="<?php echo base_url(); ?>look_up_symptoms">Look Up Symptoms</a> |
                <a href="<?php echo base_url(); ?>">Look up a Treatment</a> |
                <a href="<?php echo base_url(); ?>">Healthy Behaviors</a> |
                <a href="<?php echo base_url(); ?>submit">Submit</a> |
                <?php if($this->session->userdata('USER_ID')==''){ ?>
                <a href="<?php echo base_url(); ?>register">Register</a> |
                <a href="<?php echo base_url(); ?>login">Login</a>
                <?php }else{ ?>
                <a href="<?php echo base_url(); ?>profile1">My Account</a> | 
                <a href="<?php echo base_url(); ?>home/logout">Logout</a>
                <?php } ?>
            </nav>
        </div>
