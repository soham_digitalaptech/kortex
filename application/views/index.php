<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="<?php echo base_url(); ?>"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="">

        <title><?php echo $title;?></title>

        <!-- Bootstrap core CSS -->
        <link href="fonts/fonts.css" rel="stylesheet">
        <link href="styles/reset.css" rel="stylesheet">
        <link href="styles/layout.css" rel="stylesheet">
        <link href="styles/application.css" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
		<div class="loader-section">
			
		</div>
        
            <div class="main-container">
                <header class="page-header">
                    <h1  class="animatable logo">
                        <a href="javascript:void(0)" class="black_logo"><img src="images/logo.png" alt="kortx"></a>
                        <a href="javascript:void(0)" class="white_logo"><img src="images/logo1.png" alt="kortx"></a>
                    </h1>
                    <div class="header_right">
                        <a href="#" class="btn_blue">Looking for work?</a>
                        <a href="#" class="animatable loging toplink"><img src="images/login.png" alt=""> <span>Login</span></a>
                        <a href="#" class="animatable loging toplink register"><img src="images/registration.png" alt=""> <span>Register</span></a>
                    </div>
                </header>

                <section class="main-block first_block block-1 active">
                    <div class="container">
                        <div class="heading_top is-hidden">
                            <?php echo (!empty($widgets['find_big_data_experts_instantly']))?html_entity_decode($widgets['find_big_data_experts_instantly']):'';?>
                            <a href="#" class="btn_blue">Post a Project</a>
                        </div>
                        <div class="arrow01"></div>
                        <div class="searh-icon"></div>
                        <div class="arrow02"></div>
                        <div class="man-img is-hidden"></div>
                    </div>
                </section>

                <section class="main-block second_block block-2 is-hidden">
                    <div class="container">

                        <div class="title is-hidden">
                            <?php echo (!empty($widgets['select_expertise']))?html_entity_decode($widgets['select_expertise']):'';?>
                       </div>

                        <div class="activity_part">

                            <div class="circle" id="datascience">
                                <img alt="" src="images/icon_01.png">
                                <span>Data<br> Science</span>
                            </div>

                            <div class="circle" id="artificialintelligence">
                                <img alt="" src="images/icon_02.png">
                                <span>Artificial<br> Intelligence</span>
                            </div>

                            <div class="circle" id="biometrics">
                                <img alt="" src="images/icon_03.png">
                                <span>Biometrics</span>
                            </div>

                            <div class="circle" id="predictiveanalytics">
                                <img alt="" src="images/icon_04.png">
                                <span>Predictive<br> Analytics</span>
                       </div>

                            <div class="circle" id="machinelearning">
                                <img alt="" src="images/icon_05.png">
                                <span>Machine<br> Learning</span>
                            </div>

                            <div class="circle" id="dataprocessing" >
                                <img alt="" src="images/icon_06.png">
                                <span>Data <br>Processing</span>
                            </div>
                            <div class="circle" id="youchoose">
                                <img alt="" src="images/icon_07.png">
                                <span>You Choose</span>
                            </div>

                        </div>
                        <!--<a  href="javascript:void(0)" class="down_arrow"><span>Scroll</span></a>-->
                    </div>
                </section>

                <section class="main-block third_block block-3 is-hidden">
                    <div class="container">
                        <div class="title">
                            <?php echo (!empty($widgets['submit_a_project'])?html_entity_decode($widgets['submit_a_project']):''
                               ); ?>
                        </div>
                        <div class="phone_img">
                            <img src="images/phone.png" alt="">
                            <img src="images/avatar01.png" alt="" class="ph_avator01 is-hidden">
                            <img src="images/card.png" class="card" alt="">
                            <div  class="phone_text" >
                                <?php echo (!empty($widgets['submit_a_project_phone_text'])?html_entity_decode($widgets['submit_a_project_phone_text']):''); ?>
                            </div>
                            <a href="javascript:void(0)" class="place ">Submit</a>
                        </div>
                    </div>
                </section>
                <div class="third_block-bg is-hidden"></div>

                <section class="main-block fourth_block block-4 is-hidden">
                    <div class="dark_bg">
                        <div class="container">
                            <div class="ticket"></div>
                            <?php echo (!empty($widgets['a_big_data_marketplace'])?html_entity_decode($widgets['a_big_data_marketplace']):'');?>
                        </div>
                    </div>
                </section>

                <section class="main-block fifth_block block-5 is-hidden">
                    <div class="container">
                        <div class="title">
                            <?php echo (!empty($widgets['make_a_selection'])?html_entity_decode($widgets['make_a_selection']):''); ?>           
                        </div>
                        <div class="phone_img">
                            <div class="overlay_bg">         
                                <img alt="" src="images/ph_overlay_logo.png" class="ovrlay">  
                                <?php echo (!empty($widget['make_a_selection_phone_text'])?html_entity_decode($widget['make_a_selection_phone_text']):'');?>
                                <p>Once you have selected an expert, get the terms agreed!</p>
                            </div>
                            <div class="selection_box">
                                <img src="images/phone.png" alt="">
                            </div>
                        </div>
                        <div class="make_selection">
                            <img alt="" src="images/card.png" class="card">    
                            <img  src="images/masha.png" alt="" class="ph_avator01">
                            <div class="phone_text">
                               <?php echo (!empty($widgets['make_a_selection_block_1'])?html_entity_decode($widgets['make_a_selection_block_1']):'');?>
                            </div>
                        </div>
                        <div class="make_selection1">
                            <img alt="" src="images/card.png" class="card">    
                            <img  src="images/illia.png" alt="" class="ph_avator01">
                            <div class="phone_text">
                               <?php echo (!empty($widgets['make_a_selection_block_2'])?html_entity_decode($widgets['make_a_selection_block_2']):'');?>
                            </div>
                        </div>
                        <div class="make_selection2">
                            <img alt="" src="images/card.png" class="card">    
                            <img  src="images/vika.png" alt="" class="ph_avator01">
                            <div class="phone_text">
                               <?php echo (!empty($widgets['make_a_selection_block_3'])?html_entity_decode($widgets['make_a_selection_block_3']):'');?>           
                            </div>
                            <a href="javascript:void(0)" class="place ">Hire</a>
                        </div>
                        <div class="make_selection3">
                            <img alt="" src="images/card.png" class="card">    
                            <img  src="images/timofey.png" alt="" class="ph_avator01">
                            <div class="phone_text">
                               <?php echo (!empty($widgets['make_a_selection_block_4'])?html_entity_decode($widgets['make_a_selection_block_4']):'');?>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="main-block sixth_block block-6 is-hidden">
                    <div class="dark_bg">
                        <div class="container">
                            <h2 class="chat_heading">Agree a price<br>and terms</h2>
                            <div class="chat_boxarea">
                                <div class="incm_chat">
                                    <img src="images/incm-msg.png" alt="">
                                    <span>Vicky has accepted the project</span>
                                </div>
                                <div class="chat_01">
                                    <img src="images/chat01.png" alt="">
                                    <span>Hi Vicky! Thanks for accepting <br>our project. Our budget is $8500</span>
                                </div>
                                <div class="chat_02">
                                    <img src="images/chat02.png" alt="">
                                    <span>Hi Sam!<br>Would you like me to <br>sign an NDA first?</span>
                                </div>
                                <div class="chat_03">
                                    <img src="images/chat01.png" alt="">
                                    <span>Sure! I'm excited to work <br>on this with you :)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="main-block seventh_block block-7 is-hidden">
                    <div class="container">
                        <h2 class="getdone">Get it done</h2>
                        <span class="two_avatar">
                            <img src="images/two-pic.png" alt="">
                        </span>
                        <a href="#" class="start_now">Start Now</a>

                        <div class="phone">
                            <img alt="" src="images/phone.png" class="empty-phone">
                            <div class="ph_bg">
                                <img alt="" src="images/girl_avatar.png" class="phone_head">
                                <h4>Give feedback</h4>
                                <span class="fed_button one">Awesome!</span>
                                <span class="fed_button two">Not bad</span>
                                <span class="fed_button three">Never again!</span>
                            </div>  
                        </div>
                    </div>
                </section>

                <section class="main-block eightth_block block-8 is-hidden">
                    <div class="container">
                        <div class="footer_content">
                            <h2>Ready to start?</h2>
                            <p>Register now to begin your Data journey</p>
                            <a href="#" class="footer_btn">Hire now</a>
                            <a href="#" class="footer_btn">Work</a>
                            <div class="clearfix"></div>
                            <a href="#" class="footer_icon"><img alt="" src="images/linkedin.png"></a>
                            <a href="#" class="footer_icon"><img alt="" src="images/footer_twitter.png"></a>               
                        </div>
                    </div>
                    <div class="footer_link">
                        <a href="#">Investors</a>
                        <a href="#">Privacy policy</a>
                        <a href="#">Media kit</a> 
                        <a href="#">Contact us </a> 
                    </div>
                </section>

                <a  href="javascript:void(0)" class="arrow-up bounce"></a>
                <a  href="javascript:void(0)" class="arrow-down bounce"></a>
            </div>

        <!--  scripts  -->
        <script src="scripts/libs/jquery-min.js"></script>
        <script src="scripts/app/kortx.js"></script>


        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                windowW = $(window).width();
                windowH = $(window).height();

                var fontS = 16;
                var bodyWidth = $('html').width(),
                        width = 1680;

                if (windowW / windowH > 1.7) {
                    var width = 2086;

                }
                if (windowH > windowW) {
                    bodyWidth = windowW * 1.15;
                }

                fontS = fontS * bodyWidth / width;
                $('body').css({
                    fontSize: fontS + 'px'
                });
            });
        </script>


    </body>
</html>