<?php
require('AppController.php');
class Error extends AppController
{
    public function __construct()
    {
        parent:: __construct();
    }
    public function error_404($slug="")
    {
        $data=$this->header_footer('Page Not Found');
        $this->load->view('errors/404',$data);
    }
}
?>