<?php
require('AppController.php');

/*
Listing Budgets Class Extended from AppController

The Listing_budgets class is responsible for managing the project sub categories 

@author: Sandipan Biswas 
*/

class Listing_budgets extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('listing_budgets_model');
		$this->load->model('listing_types_model');
		$this->load->model('system_models/listing_budget_types_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Listing budgets Management',array(
                'sub_heading'=>'Listing budgets Management'
        ));
		$listing_budgets=$this->listing_budgets_model->find(array('where'=>'listing_budgets.status <> 3 ORDER BY listing_budgets.id ASC'));
		$data['rows']=array();
		if(!empty($listing_budgets['listing_budgets']))
			$data['rows']=$listing_budgets['listing_budgets'];
        //$data['rows'] = $this->listing_budgets_model->fetchRecord(array('status <> '=> 3),array('id','asc'));

        $this->load->view('admin/listing_budgets/index',$data);
    }

    /*
    This is a method for Adding the project budget. 

    @author: Sandipan Biswas

    @access: public

    @return: void   
    */
    
    public function add()
    {
        $data = $this->header_footer('Add New Listing budgets');
            
        $data['rows'] = array();

        if($this->input->post())
        {
            if(!$this->update())
                $data['rows'] = $this->input->post();
        }
		$data['listing_types']=$this->listing_types_model->fetchRecord(array('status <>'=>3));
        $data['projectList'] = $this->listing_budgets_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
            
        $this->load->view('admin/listing_budgets/edit',$data);
    }

    /*
    This is a method for Editing the project budget. 

    @author: Sandipan Biswas

    @access: public

    @return: void   
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);

            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Listing budgets Selected','ERROR');
                redirect(base_url().'admin/listing_budgets');
            }
            
            $isExist = $this->listing_budgets_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            $isExist = $this->listing_budgets_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Listing budgets not found','ERROR');
                            redirect(base_url().'admin/listing_budgets');
            }
            
            $data = $this->header_footer('Editing Project Types');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }
			$selected_listings=array();
			$selelect_listing_types=$this->listing_budget_types_model->fetchRecord(array('listing_budget_id'=>$id));
			foreach($selelect_listing_types as $value)
			{
				array_push($selected_listings,$value['listing_type_id']);
			}
			$data['selected_listings']=$selected_listings;
			$data['listing_types']=$this->listing_types_model->fetchRecord(array('status <>'=>3));
            $data['projectList'] = $this->listing_budgets_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

            $this->load->view('admin/listing_budgets/edit',$data);
    }

    /*
    This is a method for changing the status of the project budgets. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $id,$frmSecurity

    @return: void   
    */
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Listing budgets Selected','ERROR');
                redirect(base_url().'admin/listing_budgets');
            }
            $isExist = $this->listing_budgets_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Listing budgets Selected','ERROR');
                            redirect(base_url().'admin/listing_budgets');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/listing_budgets');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->listing_budgets_model->addEdit($data,array('id'=>$id));

            $this->utility->setMsg('Listing budgets status change successfully','SUCCESS');
            
            redirect(base_url().'admin/listing_budgets');
    }

    /*
    This is a method for deleting the project budgets. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $id,$frmSecurity

    @return: void   
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Listing budgets Selected','ERROR');
                redirect(base_url().'admin/listing_budgets');
            }
            
            $isExist = $this->listing_budgets_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Listing budgets Selected','ERROR');
                            redirect(base_url().'admin/listing_budgets');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                    redirect(base_url().'admin/listing_budgets');
            }
            
            $this->listing_budgets_model->addEdit(array('status'=>3),array('id'=>$id));

            $this->utility->setMsg($isExist['ads_size'].'Listing budgets is Deleted','SUCCESS');
            
            redirect(base_url().'admin/listing_budgets');
    }
    
    /*
    This is a method for add/edit the project budgets. 

    @author: Sandipan Biswas

    @access: private

    @parameters: $id

    @return: void   
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!= $this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('descriptionx','Budget Description','required');
        $this->form_validation->set_rules('price','Budget','required');
    
    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
        }
    
        if($id)
        {
            $data = array(
                'description' => $this->utility->info_cleanQuery($this->input->post('descriptionx')),
                'price' => $this->utility->info_cleanQuery($this->input->post('price')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_creation' => date('Y-m-d')
                );

            $this->listing_budgets_model->addEdit($data,array('id'=>$id));
            $listing_budget_id = $id;
        }
        else
        {
            $data = array(
                'description' => $this->utility->info_cleanQuery($this->input->post('descriptionx')),
                'price' => $this->utility->info_cleanQuery($this->input->post('price')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_creation' => date('Y-m-d')
                );

            $listing_budget_id = $this->listing_budgets_model->addEdit($data);
        }
		$this->listing_budget_types_model->delete(array('listing_budget_id'=>$listing_budget_id));
		$listing_types=array();
		foreach($this->input->post('listing_type_id') as $value)
		{
			$listing_type_id=base64_decode($value);
			if(!$listing_type_id || !is_numeric($listing_type_id))
				continue;
			$temp=array(
				'listing_budget_id'=>$listing_budget_id,
				'listing_type_id'=>$listing_type_id
			);
			array_push($listing_types,$temp);
		}
		$this->listing_budget_types_model->insert_batch($listing_types);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/listing_budgets/edit/'.base64_encode($listing_budget_id));

    }
}
?>