<?php
require('AppController.php');

/*
This is Experience_level Class for CodeIgniter

Experience_level Class Extended from AppController

The Experience_level class is responsible for managing all the Experience 

@author: Sandipan Biswas 
*/

class Experience_level extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('experience_level_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Experience level Management',array(
                'sub_heading'=>'Experience level Management'
        ));

        $data['rows'] = $this->experience_level_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/experience_level/index',$data);
    }
    

    /*
    This is a method for adding the Experience level. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */

    public function add()
    {
            $data = $this->header_footer('Add New Experience level');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->experience_level_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
            
            $this->load->view('admin/experience_level/edit',$data);
    }


    /*
    This is a method for editing the experience level. 

    @author: Sandipan Biswas 

    @parameter(s): $id

    @return: void
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);

            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Experience level Selected','ERROR');
                redirect(base_url().'admin/experience_level');
            }
            
            $isExist = $this->experience_level_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            $isExist = $this->experience_level_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                $this->utility->setMsg('Experience level not found','ERROR');
                redirect(base_url().'admin/experience_level');
            }
            
            $data = $this->header_footer('Editing Experience level');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->experience_level_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

            $this->load->view('admin/experience_level/edit',$data);
    }
    

    /*
    This is a method for changing the status of the experience level from active to inactive and vice versa. 

    @author: Sandipan Biswas 

    @parameter(s): $id,$frmSecurity

    @return: void
    */
    

    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Experience level Selected','ERROR');
                redirect(base_url().'admin/experience_level');
            }
            $isExist = $this->experience_level_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                $this->utility->setMsg('Invalid Experience level Selected','ERROR');
                redirect(base_url().'admin/experience_level');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/experience_level');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->experience_level_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Project change successfully','SUCCESS');
            redirect(base_url().'admin/experience_level');
    }


    /*
    This is a method for deleting the experience level from database. 

    @author: Sandipan Biswas 

    @parameter(s): $id,$frmSecurity

    @return: void
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Experience level Selected','ERROR');
                redirect(base_url().'admin/experience_level');
            }
            
            $isExist = $this->experience_level_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Experience level Selected','ERROR');
                            redirect(base_url().'admin/experience_level');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                    redirect(base_url().'admin/experience_level');
            }
            $this->experience_level_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Project is Deleted','SUCCESS');
            redirect(base_url().'admin/experience_level');
    }
    

    /*
    This is a method for updating the experience level of database. 

    @author: Sandipan Biswas 

    @parameter(s): $id

    @access: private

    @return: void
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!= $this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('name','Experience level','required');
    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
        }
    
        if($id)
        {
            $data = array(
                'name' => $this->utility->info_cleanQuery($this->input->post('name')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_creation' => (int)time()
                );

            $this->experience_level_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
        {
            $data = array(
                'name' => $this->utility->info_cleanQuery($this->input->post('name')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_modification' => (int)time()
                );

            $adsID = $this->experience_level_model->addEdit($data);
        }
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/experience_level/edit/'.base64_encode($adsID));

    }
}
?>