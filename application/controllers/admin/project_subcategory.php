<?php
require('AppController.php');

/*
This is project category Class v2.2 for CodeIgniter

project_category Class Extended from AppController

The project_category class is responsible for managing the project sub categories 

@author: Sandipan Biswas 
*/

class Project_subcategory extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('project_subcategory_model');
        $this->load->model('project_category_model');

    }
    
    public function index()
    {
        $data = $this->header_footer('Project Subcategory Management',array(
                'sub_heading'=>'Project Subcategory Management'
        ));

        $data['rows'] = $this->project_subcategory_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/sub_category/index',$data);
    }

    /*
    This is a method for Adding project sub category. 

    @author: Sandipan Biswas

    @access: public

    @return: void   
    */
    
    public function add()
    {
            $data = $this->header_footer('Add New Sub Category');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }

            $data['categoryList'] = $this->project_category_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
            
            $this->load->view('admin/sub_category/edit',$data);
    }

    /*
    This is a method for Editing project sub category. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $id

    @return: void   
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Sub Category Selected','ERROR');
                redirect(base_url().'admin/project_subcategory');
            }
            
            $isExist = $this->project_subcategory_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            $isExist = $this->project_subcategory_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Project Sub Category not found','ERROR');
                            redirect(base_url().'admin/project_subcategory');
            }
            
            $data = $this->header_footer('Editing Project Sub Category');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $data['categoryList'] = $this->project_category_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

            $this->load->view('admin/sub_category/edit',$data);
    }

    /*
    This is a method for changing the project sub category status from active to in active and vice versa. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $id,$frmSecurity

    @return: void   
    */
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Sub Category Selected','ERROR');
                redirect(base_url().'admin/project_subcategory');
            }
            $isExist = $this->project_subcategory_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Sub Category Selected','ERROR');
                            redirect(base_url().'admin/project_subcategory');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/project_subcategory');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->project_subcategory_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Project Sub Category change successfully','SUCCESS');
            redirect(base_url().'admin/project_subcategory');
    }

    /*
    This is a method for deleting the project sub category status from active to in active and vice versa. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $id,$frmSecurity

    @return: void   
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Sub Category Selected','ERROR');
                redirect(base_url().'admin/project_subcategory');
            }
            
            $isExist = $this->project_subcategory_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Sub Category Selected','ERROR');
                            redirect(base_url().'admin/project_subcategory');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/project_subcategory');
            }
            $this->project_subcategory_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Sub Category is Deleted','SUCCESS');
            redirect(base_url().'admin/project_subcategory');
    }
    
    /*
    This is a method for adding/editing the project sub category status from active to in active and vice versa. 

    @author: Sandipan Biswas

    @access: private

    @parameters: $id

    @return: void   
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('subcategory_title','Subcategory Title','required');
        $this->form_validation->set_rules('category_id','Category Id','required');
        $this->form_validation->set_rules('subcategory_description','Project Description','required');

    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');

                return false;
        }

        $data = array(
                    'subcategory_title'=>$this->utility->info_cleanQuery($this->input->post('subcategory_title')),
                    'category_id'=>$this->utility->info_cleanQuery($this->input->post('category_id')),
                    'subcategory_description'=>$this->utility->info_cleanQuery($this->input->post('subcategory_description')),
                    'status'=>$this->utility->info_cleanQuery($this->input->post('status'))
                    );   

        if($id)
        {
            $this->project_subcategory_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->project_subcategory_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/project_subcategory/edit/'.base64_encode($adsID));
    }
}
?>