<?php
require('AppController.php');

/*
This is Project_budget_ranges Class for CodeIgniter

Project_budget_ranges Class Extended from AppController

The Project_budget_ranges class is responsible for managing all the Notifications 

@author: Sandipan Biswas 
*/

class Project_budget_ranges extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('project_budget_ranges_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Project Budget Range Management',array(
            'sub_heading'=>'Project Budget Range Management'
        ));

        $data['rows'] = $this->project_budget_ranges_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/project_budget_ranges/index',$data);
    }

    /*
    This is a method for adding the project budget range. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */

    
    public function add()
    {
            $data = $this->header_footer('Add New Project');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->project_budget_ranges_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
            
            $this->load->view('admin/project_budget_ranges/edit',$data);
    }

    /*
    This is a method for editing the project budget range. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);

            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Selected','ERROR');
                redirect(base_url().'admin/project_budget_ranges');
            }
            
            $isExist = $this->project_budget_ranges_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            $isExist = $this->project_budget_ranges_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Project not found','ERROR');
                            redirect(base_url().'admin/project_budget_ranges');
            }
            
            $data = $this->header_footer('Editing Project');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->project_budget_ranges_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

            $this->load->view('admin/project_budget_ranges/edit',$data);
    }

    /*
    This is a method for changing the status of the project budget range from active to inactive and vice versa. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */
    
    
    public function change_status($id,$frmSecurity)
    { 
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Selected','ERROR');
                redirect(base_url().'admin/project_budget_ranges');
            }
            $isExist = $this->project_budget_ranges_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Project Selected','ERROR');
                            redirect(base_url().'admin/project_budget_ranges');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/project_budget_ranges');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->project_budget_ranges_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Project change successfully','SUCCESS');
            redirect(base_url().'admin/project_budget_ranges');
    }

    /*
    This is a method for deleting the project budget range. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id,$frmSecurity

    @return: void
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Static Project Selected','ERROR');
                redirect(base_url().'admin/project_budget_ranges');
            }
            
            $isExist = $this->project_budget_ranges_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Project Titles Selected','ERROR');
                            redirect(base_url().'admin/project_budget_ranges');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                    redirect(base_url().'admin/project_budget_ranges');
            }
            $this->project_budget_ranges_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Project is Deleted','SUCCESS');
            redirect(base_url().'admin/project_budget_ranges');
    }
    


    /*
    This is a method for updating(add/edit) the project budget range to database. 

    @author(s): Sandipan Biswas 

    @access: private

    @parameters: $id

    @return: void
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!= $this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('title','Static Project Title','required');
        $this->form_validation->set_rules('description','Project Description','required');
        $this->form_validation->set_rules('job_type','Job Type','required');
        $this->form_validation->set_rules('min_budget','Min Budget','required');
        $this->form_validation->set_rules('max_budget','Max Budget','required');

        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
        }
    
        if($id)
        {
            $data = array(
                'title' => $this->utility->info_cleanQuery($this->input->post('title')),
                'description' => $this->utility->info_cleanQuery($this->input->post('description')),
                'job_type' => $this->utility->info_cleanQuery($this->input->post('job_type')),
                'min_budget' => $this->utility->info_cleanQuery($this->input->post('min_budget')),
                'max_budget' => $this->utility->info_cleanQuery($this->input->post('max_budget')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_creation' => date('Y-m-d')
                );

            $this->project_budget_ranges_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
        {
            $data = array(
                'title' => $this->utility->info_cleanQuery($this->input->post('title')),
                'description' => $this->utility->info_cleanQuery($this->input->post('description')),
                'job_type' => $this->utility->info_cleanQuery($this->input->post('job_type')),
                'min_budget' => $this->utility->info_cleanQuery($this->input->post('min_budget')),
                'max_budget' => $this->utility->info_cleanQuery($this->input->post('max_budget')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_modification' => date('Y-m-d')
                );

            $adsID = $this->project_budget_ranges_model->addEdit($data);
        }
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/project_budget_ranges/edit/'.base64_encode($adsID));
    }
}
?>