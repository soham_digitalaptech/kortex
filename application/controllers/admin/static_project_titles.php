<?php
require('AppController.php');

/*
This is Slider_master Class for CodeIgniter

Slider_master Class Extended from AppController

The Slider_master class is responsible for managing all the sliders 

@author: Sandipan Biswas 
*/

class Static_project_titles extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('static_project_titles_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Static Project Management',array(
                'sub_heading'=>'Static Project Management'
        ));

        $data['rows'] = $this->static_project_titles_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/static_project_titles/index',$data);
    }

    /*
    This is a method for adding the Static Project Titles. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */
    
    public function add()
    {
            $data = $this->header_footer('Add New Project Titles');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->static_project_titles_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
            
            $this->load->view('admin/static_project_titles/edit',$data);
    }
    
    
    /*
    This is a method for editing the Static Project Titles. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id

    @return: void
    */

    public function edit($id="")
    {
            $id = base64_decode($id);

            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Titles Selected','ERROR');
                redirect(base_url().'admin/static_project_titles');
            }
            
            $isExist = $this->static_project_titles_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            $isExist = $this->static_project_titles_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Project Titles not found','ERROR');
                            redirect(base_url().'admin/static_project_titles');
            }
            
            $data = $this->header_footer('Editing Project Titles');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->static_project_titles_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

            $this->load->view('admin/static_project_titles/edit',$data);
    }
    
    
    /*
    This is a method for changing the status of the Static Project Titles from active to inactive and vice versa. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id

    @return: void
    */

    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Selected','ERROR');
                redirect(base_url().'admin/static_project_titles');
            }
            $isExist = $this->static_project_titles_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Project Selected','ERROR');
                            redirect(base_url().'admin/static_project_titles');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/static_project_titles');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->static_project_titles_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Project change successfully','SUCCESS');
            redirect(base_url().'admin/static_project_titles');
    }

    /*
    This is a method for deleting the Static Project Titles. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id,$frmSecurity

    @return: void
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Static Project Selected','ERROR');
                redirect(base_url().'admin/static_project_titles');
            }
            
            $isExist = $this->static_project_titles_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Project Titles Selected','ERROR');
                            redirect(base_url().'admin/static_project_titles');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/static_project_titles');
            }
            $this->static_project_titles_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Project is Deleted','SUCCESS');
            redirect(base_url().'admin/static_project_titles');
    }
    
    /*
    This is a method for updating the Static Project Titles. 

    @author(s): Sandipan Biswas 

    @access: private

    @parameters: $id

    @return: void
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!= $this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('title','Static Project Title','required');
        $this->form_validation->set_rules('description','Project Description','required');

    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');

                return false;
        }
    

        if($id)
        {
            $data = array(
                'title'=>$this->utility->info_cleanQuery($this->input->post('title')),
                'description'=>$this->utility->info_cleanQuery($this->input->post('description')),
                'date_of_creation'=>date('Y-m-d')
                );

            $this->static_project_titles_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
        {
            
            $data = array(
                'title'=>$this->utility->info_cleanQuery($this->input->post('title')),
                'description'=>$this->utility->info_cleanQuery($this->input->post('description')),
                'date_of_modification'=>date('Y-m-d')
                );

            $adsID = $this->static_project_titles_model->addEdit($data);
        }
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/static_project_titles/edit/'.base64_encode($adsID));
    }
}
?>