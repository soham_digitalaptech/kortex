<?php
require('AppController.php');

/*
This is User Class for CodeIgniter

User Class Extended from AppController

The User class is responsible for managing all the users 

@author: Sandipan Biswas 
*/

class User extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->checkSuperAdmin();
        $this->load->model('users_model');
        $this->load->model('system_models/user_types_model');
        $this->load->model('system_models/modules_model');
        $this->load->model('system_models/user_permissions_model');
    }
    
    public function index() 
    {
        $data = $this->header_footer('User Management',array('sub_heading' => 'all system users'));
        $users = $this->users_model->find(array('where' => 'users.status <> 3'),array('id','desc'));
        
        $data['users'] = $users['users'];
        $this->load->view('admin/user/list',$data);
    }

    /*
    This is a method for giving the permission to the User. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id

    @return: void
    */

    public function permission($id="")
    {
        $userModules=array();
        $id=base64_decode($id);
        if(!$id || !is_numeric($id))
        {
            $this->utility->setMsg('Invalid user selected','ERROR');
            redirect(base_url().'admin/user');
        }
        $user=$this->users_model->fetchRow(array('id'=>$id));
        if(!$user)
        {
            $this->utility->setMsg('Selected user not found','ERROR');
            redirect(base_url().'admin/user');
        }
        $modules=$this->modules_model->fetchRecord();
        if($this->input->post())
        {
            $this->allotModule($id);
        }
        $userModulesData=$this->user_permissions_model->find(array('where'=>'user_id = '.$id));
        if(!empty($userModulesData))
        {
            foreach($userModulesData['user_permissions'] as $value)
            {
                array_push($userModules,$value['module_id']);
            }
        }
        else
        {
            $userModulesData['user_permissions']=array();
        }
        $data=$this->header_footer($user['username'].'\'s Permissions ',array(
                                    'sub_heading'=>'user permission list'
        ));
        $data['user']=$user;
        $data['user_modules']=$userModulesData['user_permissions'];
        $data['user_module_ids']=$userModules;
        $data['id']=base64_encode($id);
        $data['modules']=$modules;
        $this->load->view('admin/user/permission',$data);
    }


    /*
    This is a method for changing the permission to the User. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */  

    public function change_permission()
    {
        $id=base64_decode($this->input->post('id'));
        if(empty($id))
            $this->output('Invalid User Module Selected','ERROR');
        if(!is_numeric($id))
            $this->output('Invalid User Module Selected','ERROR');
        $isExist=$this->user_permissions_model->fetchRow(array('id'=>$id));
        if(!$isExist)
            $this->output('Selected module not found','ERROR');
        if($this->input->post('frmSecurity')!=$this->utility->getSecurity())
        {
            $this->output('Invalid Security Token','ERROR');
        }
        $data=array();
        if($this->input->post('read_access'))
        {
            if($this->input->post('read_access')==1)
                $data['read_access']=1;
            else
                $data['read_access']=0;
        }

        if($this->input->post('write_access'))
        {
            if($this->input->post('write_access')==1)
            {
                $data['read_access']=1;
                $data['write_access']=1;
            }
            else
                $data['write_access']=0;
        }

        $this->user_permissions_model->addEdit($data,array('id'=>$id));
        $this->output('Success','SUCCESS');
    }

    /*
    This is a method for managing the account_types(add/edit/delete) of the User. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id,$action,$frmSecurity

    @return: void
    */

    public function account_types($action="list",$id="",$frmSecurity="") 
    {
        $id=base64_decode($id);
        
        if($action=="list")
        {
            $data=$this->header_footer('Manage User Types',array(
                                        'sub_heading'=>'all system users type'
            ));
            $types=$this->user_types_model->fetchRecord(array(),array('id','desc'));
            $data['types']=$types;
            $this->load->view('admin/user/account_type_list',$data);
        }

        if($action=="add" || $action=="edit")
        {
            $data=$this->header_footer('Add User Type',array(
                                        'sub_heading'=>'new users type'
            ));
            $data['rows']=array();
            if($action=="edit")
            {
                if(!$id || !is_numeric($id))
                {
                    $this->utility->setMsg('Invalid User Type ID','ERROR');
                    redirect(base_url().'admin/user/account_types');
                }
                $data=$this->header_footer('Edit User Type',array(
                                            'sub_heading'=>'new users type'
                ));
                $data['rows']=$this->user_types_model->fetchRow(array('id'=>$id));
                if(!$data['rows'])
                {
                    $this->utility->setMsg('User Type not found','ERROR');
                    redirect(base_url().'admin/user/account_types');
                }
                $data['id']=  base64_encode($id);
            }
            
            if($this->input->post())
            {
                if(!$this->update_account_type($id))
                {
                    $data['rows']=$this->input->post();
                }
            }
            $this->load->view('admin/user/add_edit_account_type',$data);
        }
        if($action=="delete")
        {
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                $this->utility->setMsg('Invalid Security Token','ERROR');
                redirect(base_url().'admin/user/account_types');
            }
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid User Type ID','ERROR');
                redirect(base_url().'admin/user/account_types');
            }
            $type=$this->user_types_model->fetchRow(array('id'=>$id));
            if(!$type)
            {
                $this->utility->setMsg('Selected User type is not found','ERROR');
                redirect(base_url().'admin/user/account_types');
            }
            $isExists=$this->users_model->fetchRecord(array('user_type_id'=>$id));
            if($isExists)
            {
                $this->utility->setMsg('Sorry!! Unable to delete '.$type['type_name'].'<br/>Some user holding this type','ERROR');
                redirect(base_url().'admin/user/account_types');
            }
            $this->user_types_model->delete(array('id'=>$id));
            $this->utility->setMsg($type['type_name'].' Type successfully deleted','SUCCESS');
            redirect(base_url().'admin/user/account_types');
        }
    }


    /*
    This is a method for adding the user. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */

    public function add()
    {
        $data=$this->header_footer('Add New User',array(
                                    'sub_heading'=>'add new user'
        ));
        $data['user']=array();
        if($this->input->post())
        {
            if(!$this->addAction())
                $data['user']=$this->input->post();
        }
        
        $data['admin_type']=$this->user_types_model->fetchRecord();
        $this->load->view('admin/user/addEdit',$data);
    }

    /*
    This is a method for editing the user. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $admin_id

    @return: void
    */
    
    public function edit($admin_id="")
    {
        $adminID = base64_decode($admin_id);
        if(!$adminID || !is_numeric($adminID))
        {
            $this->utility->setMsg('Invalid user id','ERROR');
            redirect(base_url().'admin/user');
        }
        $userDetails=$this->users_model->fetchRow(array('id'=>$adminID));
        if(!$userDetails)
        {
            $this->utility->setMsg('User not found','ERROR');
            redirect(base_url().'admin/user');
        }
        $data=$this->header_footer('Edit User ('.$userDetails['username'].')',array(
                                    'sub_heading'=>'edit user'
        ));
        $data['user']=$userDetails;
        if($this->input->post())
        {
            if(!$this->editAction($adminID))
                $data['user']=$this->input->post();
        }
        $data['admin_id']=  base64_encode($adminID);
        $data['admin_type']=$this->user_types_model->fetchRecord();
        $this->load->view('admin/user/addEdit',$data);
    }



    /*
    This is a method for deleting the user. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id,$frmSecurity

    @return: void
    */

    public function delete($id="",$frmSecurity="")
    {
        $time=(int)time();
        if($this->utility->getSecurity()!=$frmSecurity)
        {
            $this->utility->setMsg('Invalid Security Token','ERROR');
            redirect(base_url().'admin/user');
        }
        $id=base64_decode($id);
        if(!$id || !is_numeric($id))
        {
            $this->utility->setMsg('Invalid User Type ID','ERROR');
            redirect(base_url().'admin/user/account_types');
        }
        $user=$this->users_model->fetchRow(array('id'=>$id));
        if(!$user)
        {
            $this->utility->setMsg('Selected User is not found','ERROR');
            redirect(base_url().'admin/user');
        }
        $data=array(
            'status'=>3,
            'date_of_modification'=>$time
        );
        $this->users_model->addEdit($data,array('id'=>$id));
        $this->utility->setMsg('('.$user['username'].') is successfully deleted','SUCCESS');
        redirect(base_url().'admin/user');
    }
    

    /*
    This is a method for changing the status of the user from active to inactive and vice versa. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id,$frmSecurity

    @return: void
    */

    public function change_status($id="",$frmSecurity="")
    {
        $status=array('Not Verified','Active','Blocked');
        $time=(int)time();
        if($this->utility->getSecurity()!=$frmSecurity)
        {
            $this->utility->setMsg('Invalid Security Token','ERROR');
            redirect(base_url().'admin/user');
        }
        $id=base64_decode($id);
        if(!$id || !is_numeric($id))
        {
            $this->utility->setMsg('Invalid User Type ID','ERROR');
            redirect(base_url().'admin/user/account_types');
        }
        $user=$this->users_model->fetchRow(array('id'=>$id));
        if(!$user)
        {
            $this->utility->setMsg('Selected User is not found','ERROR');
            redirect(base_url().'admin/user');
        }
        $data=array(
            'status'=>2,
            'date_of_modification'=>$time
        );
        $statusFlag="ERROR";
        if($user['status']==2)
        {
            $data['status']=1;
            $statusFlag="SUCCESS";
        }
        $this->users_model->addEdit($data,array('id'=>$id));
        $this->utility->setMsg('('.$user['username'].') is now '.$status[$data['status']],$statusFlag);
        redirect(base_url().'admin/user');
    }


    /*
    This is a method for managing the affiliation of the user. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id,$frmSecurity

    @return: void
    */

    public function affiliates($id="",$frmSecurity="")
    {
        $status=array('Not Verified','Active','Blocked');
        $time=(int)time();
        if($this->utility->getSecurity()!=$frmSecurity)
        {
            $this->utility->setMsg('Invalid Security Token','ERROR');
            redirect(base_url().'admin/user');
        }
        $id=base64_decode($id);
        if(!$id || !is_numeric($id))
        {
            $this->utility->setMsg('Invalid User Type ID','ERROR');
            redirect(base_url().'admin/user/account_types');
        }
        $user=$this->users_model->fetchRow(array('id'=>$id));
        if(!$user)
        {
            $this->utility->setMsg('Selected User is not found','ERROR');
            redirect(base_url().'admin/user');
        }
        $data=array(
            'isAffiliate'=>1,
            
        );
        $statusFlag="SUCCESS";
        $msg = "successfully unregistered to affiliation";
        

        if($user['isAffiliate']=='0')
        {
            $data['isAffiliate']='1';
            $statusFlag="SUCCESS";
            
        }
        
   

        $this->users_model->addEdit($data,array('id'=>$id));
        //$this->utility->setMsg('('.$user['username'].') has completed affiliate registration successfully',$statusFlag);
        if($user['isAffiliate']=='0')
        {
            $this->utility->setMsg('('.$user['username'].') is now successfully registered to affiliation',$statusFlag);
        }
        
        redirect(base_url().'admin/user/affiliation/'.$id);
    }

    /*
    This is a method for managing the affiliation of the user. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id

    @return: void
    */

    public function affiliation($id="") 
    {
        //$data = $this->header_footer('User Management',array('sub_heading'=>'all system users'));
        
        
        $data = $this->header_footer('Affiliation',array('sub_heading'=>'all system users'));
        $users = $this->users_model->find(array('where'=>'users.id ='.$id));
        $data['users'] = $users['users'];


        $this->load->view('admin/user/affiliation',$data);
    }

    /*
    This is a method for updating the account type of the user. 

    @author(s): Sandipan Biswas 

    @access: private

    @parameters: $id

    @return: void
    */
    
    private function update_account_type($id="")
    {
        $time=(int)time();
        if($this->input->post('frmSecurity')!=$this->utility->getSecurity())
        {
            $this->utility->setMsg('Invalid Security Token','ERROR');
            return false;
        }
        $this->form_validation->set_rules('type_name','Type Name','required');
        if(!$this->form_validation->run())
        {
            $this->utility->setMsg(validation_errors(),'ERROR');
            return false;
        }
        $data=array(
            'type_name'=>$this->utility->info_cleanQuery($this->input->post('type_name')),
            'date_of_modification'=>$time,
        );
        if($id)
        {
            $this->user_types_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Updated','SUCCESS');
        }
        else{
            $data['date_of_creation']=$time;
            $id=$this->user_types_model->addEdit($data);
            $this->utility->setMsg('Sucessfully Added','SUCCESS');
        }
        redirect(base_url().'admin/user/account_types/edit/'.base64_encode($id));
    }

    /*
    This is a method for adding the user information to the database. 

    @author(s): Sandipan Biswas 

    @access: private

    @return: void
    */
    
    private function addAction()
    {
        $time=(int)time();
        if($this->input->post('frmSecurity')!=$this->utility->getSecurity())
        {
            $this->utility->setMsg('Invalid Security Token','ERROR');
            return false;
        }
        $this->form_validation->set_rules('username','Username','required');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('user_type_id','User Type','required|numeric');
        $this->form_validation->set_rules('status','Status','required|numeric');
        if(!$this->form_validation->run())
        {
            $this->utility->setMsg(validation_errors(),'ERROR');
            return false;
        }
        $isExist=$this->users_model->fetchRow(array('username'=>$this->utility->info_cleanQuery('username'),'status <>'=>2));
        if($isExist)
        {
            $this->utility->setMsg('Username Already Exists','ERROR');
            return false;
        }
        $data=array(
            'username'=>$this->utility->info_cleanQuery($this->input->post('username')),
            'password'=>sha1($this->utility->info_cleanQuery($this->input->post('password'))),
            'user_type_id'=>$this->utility->info_cleanQuery($this->input->post('user_type_id')),
            'name'=>$this->utility->info_cleanQuery($this->input->post('name')),
            'email'=>$this->utility->info_cleanQuery($this->input->post('email')),
            'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
            'date_of_creation'=>$time,
            'date_of_modification'=>$time
        );
        $adminID=$this->users_model->addEdit($data);
        $this->utility->setMsg('User Successfully Created','SUCCESS');
        redirect(base_url().'admin/user/edit/'.  base64_encode($adminID));
    }
    
    
    /*
    This is a method for editing the user information to the database. 

    @author(s): Sandipan Biswas 

    @access: private

    @parameters: $adminID

    @return: void
    */

    private function editAction($adminID)
    {
        $time=(int)time();
        if($this->input->post('frmSecurity')!=$this->utility->getSecurity())
        {
            $this->utility->setMsg('Invalid Security Token','ERROR');
            return false;
        }
        $this->form_validation->set_rules('username','Username','required');
        if($this->session->userdata('admin_id')!=$adminID)
        {
            $this->form_validation->set_rules('user_type_id','User Type','required|numeric');
            $this->form_validation->set_rules('status','Status','required|numeric');
        }
        if(!$this->form_validation->run())
        {
            $this->utility->setMsg(validation_errors(),'ERROR');
            return false;
        }
        $isExist=$this->users_model->fetchRow(array('username'=>$this->utility->info_cleanQuery('username'),'status <>'=>2,'id <>'=>$adminID));
        if($isExist)
        {
            $this->utility->setMsg('Username Already Exists','ERROR');
            return false;
        }
        $data=array(
            'username'=>$this->utility->info_cleanQuery($this->input->post('username')),
            'user_type_id'=>$this->utility->info_cleanQuery($this->input->post('user_type_id')),
            'name'=>$this->utility->info_cleanQuery($this->input->post('name')),
            'email'=>$this->utility->info_cleanQuery($this->input->post('email')),
            'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
            'date_of_modification'=>$time
        );
        if($this->session->userdata('admin_id')==$adminID)
        {
            unset($data['user_type_id']);
            unset($data['status']);
        }
        if($this->input->post('password'))
            $data['password']=sha1($this->utility->info_cleanQuery($this->input->post('password')));
        $this->users_model->addEdit($data,array('id'=>$adminID));
        if($adminID==$this->session->userdata('admin_id'))
            $this->register_veriables($this->users_model->fetchRow(array('id'=>$adminID)));
        $this->utility->setMsg('User Successfully Updated','SUCCESS');
        redirect(base_url().'admin/user/edit/'.  base64_encode($adminID));
    }

    /*
    This is a method for registering the user information to the database. 

    @author(s): Sandipan Biswas 

    @access: private

    @parameters: $data

    @return: void
    */
    
    private function register_veriables($data)
    {
            foreach($data as $key=>$value)
            {
                    $session_key=$key;
                    if($key=='id')
                            $session_key="admin_id";
                    if($key=="password")
                            continue;
                    $this->session->set_userdata($session_key,$value);
            }
    }

    /*
    This is a method for alloting the module of the user. 

    @author(s): Sandipan Biswas 

    @access: private

    @parameter: $id

    @return: void
    */

    private function allotModule($id)
    {
        $time=(int)time();
        $insertBatch=array();
        if($this->input->post('frmSecurity')!=$this->utility->getSecurity())
        {
            $this->utility->setMsg('Security Token Expired','ERROR');
            redirect(base_url().'admin/user/permission/'.  base64_encode($id));
        }
        $this->user_permissions_model->delete(array('user_id'=>$id));
        foreach($this->input->post('selected_modules') as $value)
        {
            $data=array(
                'user_id'=>$id,
                'module_id'=>base64_decode($value),
                'date_of_creation'=>$time
            );
            array_push($insertBatch,$data);
        }
        if($insertBatch)
        {
            $this->user_permissions_model->insert_batch($insertBatch);
            $this->utility->setMsg(count($insertBatch).' Module(s) has been updated');
            redirect(base_url().'admin/user/permission/'.  base64_encode($id));
        }
    }


    /*
    This is a method for initializing the status of the user. 

    @author(s): Sandipan Biswas 

    @access: private

    @return: void
    */

    private function initialize()
    {
        $return=array(
                      'result' =>'',
                      'status'=>'initiate',
                      'cause'=>'Nothing is happening..'
        );
        return $return;
    }

    /*
    This is a method for showing the output of the status of a user. 

    @author(s): Sandipan Biswas 

    @access: private

    @parameters: $cause,$status,$data

    @return: status
    */
  
    private function output($cause="",$status="error",$data=array())
    {
       $return=$this->initialize();
       $return['result']=$data;
       $return['cause']=$cause;
       $return['status']='error';
       if(strtolower($status)=="success")
           $return['status']='success';
       echo json_encode($return);
       die;
    }
}
?>