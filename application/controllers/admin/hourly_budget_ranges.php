<?php
require('AppController.php');
class Hourly_budget_ranges extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('hourly_budget_ranges_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Hourly Project Management',array(
                'sub_heading'=>'Hourly Project Management'
        ));

        $data['rows'] = $this->hourly_budget_ranges_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/hourly_budget_ranges/index',$data);
    }
    
    public function add()
    {
            $data = $this->header_footer('Add New Project');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->hourly_budget_ranges_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
            
            $this->load->view('admin/hourly_budget_ranges/edit',$data);
    }
    
    public function edit($id="")
    {
            $id = base64_decode($id);

            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Selected','ERROR');
                redirect(base_url().'admin/hourly_budget_ranges');
            }
            
            $isExist = $this->hourly_budget_ranges_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            $isExist = $this->hourly_budget_ranges_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Project not found','ERROR');
                            redirect(base_url().'admin/hourly_budget_ranges');
            }
            
            $data = $this->header_footer('Editing Project');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->hourly_budget_ranges_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

            $this->load->view('admin/hourly_budget_ranges/edit',$data);
    }
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Selected','ERROR');
                redirect(base_url().'admin/hourly_budget_ranges');
            }
            $isExist = $this->hourly_budget_ranges_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Project Selected','ERROR');
                            redirect(base_url().'admin/hourly_budget_ranges');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/hourly_budget_ranges');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->hourly_budget_ranges_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Project change successfully','SUCCESS');
            redirect(base_url().'admin/hourly_budget_ranges');
    }
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Static Project Selected','ERROR');
                redirect(base_url().'admin/static_project_titles');
            }
            
            $isExist = $this->hourly_budget_ranges_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Project Titles Selected','ERROR');
                            redirect(base_url().'admin/static_project_titles');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                    redirect(base_url().'admin/static_project_titles');
            }
            $this->hourly_budget_ranges_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Project is Deleted','SUCCESS');
            redirect(base_url().'admin/hourly_budget_ranges');
    }
    


    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!= $this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('title','Static Project Title','required');
        $this->form_validation->set_rules('description','Project Description','required');
        $this->form_validation->set_rules('min_budget','Min Budget','required');
        $this->form_validation->set_rules('max_budget','Max Budget','required');


    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
        }
    


        if($id)
        {
            $data = array(
                'title' => $this->utility->info_cleanQuery($this->input->post('title')),
                'description' => $this->utility->info_cleanQuery($this->input->post('description')),
                'min_budget' => $this->utility->info_cleanQuery($this->input->post('min_budget')),
                'max_budget' => $this->utility->info_cleanQuery($this->input->post('max_budget')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_creation' => date('Y-m-d')
                );

            $this->hourly_budget_ranges_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
        {
            $data = array(
                'title' => $this->utility->info_cleanQuery($this->input->post('title')),
                'description' => $this->utility->info_cleanQuery($this->input->post('description')),
                'min_budget' => $this->utility->info_cleanQuery($this->input->post('min_budget')),
                'max_budget' => $this->utility->info_cleanQuery($this->input->post('max_budget')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_modification' => date('Y-m-d')
                );

            $adsID = $this->hourly_budget_ranges_model->addEdit($data);
        }
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/hourly_budget_ranges/edit/'.base64_encode($adsID));

    }
}
?>