<?php
require('AppController.php');
class Notifications extends AppController
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
                $this->load->model('notifications_model');
		$this->checkLogin();
	}
	public function index()
	{
		$data=$this->header_footer('All Notifications');
		$data['rows']=$this->notifications_model->fetchRecord();
		$this->load->view('admin/notifications/index',$data);
	}
	public function add()
	{
		$data=$this->header_footer('Add New Notification');
		$data['rows']=array();
		if($this->input->post())
		{
			if(!$this->addAction())
				$data['rows']=$this->input->post();
		}
		$this->load->view('admin/notifications/edit',$data);
	}
	public function edit($id)
	{
		$data['id']=$id;
                $id=base64_decode($id);
                /*****************Check id is Valid or Not*****************/
                if(empty($id) || !is_numeric($id))
                {
                    $this->utility->setMsg('Invalid Notification Selected','ERROR');
                    redirect(base_url().'admin/notifications');
                }

		$isExist=$this->notifications_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
		if(!$isExist)
		{
				$this->utility->setMsg('Invalid Notification Selected','ERROR');
				redirect(base_url().'admin/notifications');
		}
		$data=$this->header_footer('Editing Notification');
		$data['rows']=$isExist;
		if($this->input->post())
		{
			if(!$this->update($id))
				$data['rows']=$this->input->post();
		}
		$this->load->view('admin/notifications/edit',$data);
	}
	
	public function change_status($id,$frmSecurity)
	{
		$status=array('','Active','Outdated');
                $id=base64_decode($id);
		/*****************Check id is Valid or Not*****************/
                if(empty($id) || !is_numeric($id))
                {
                    $this->utility->setMsg('Invalid Notification Selected','ERROR');
                    redirect(base_url().'admin/notifications');
                }

                if($this->utility->getSecurity()!=$frmSecurity)
                {
                    $this->utility->setMsg('Session doesn\'t matched','ERROR');
                    redirect(base_url().'admin/notifications');
                }
		$isExist=$this->notifications_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
		if(!$isExist)
		{
				$this->utility->setMsg('Invalid Notification Selected','ERROR');
				redirect(base_url().'admin/notifications');
		}
		if($this->utility->getSecurity()!=$frmSecurity)
		{
			$this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
			redirect(base_url().'admin/notifications');
		}
		$data=array(
			'status'=>1
		);
		if($isExist['status']==1)
			$data['status']=2;
		$this->notifications_model->addEdit($data,array('id'=>$id));
		$this->utility->setMsg('Notification is Now '.$status[$data['status']],'SUCCESS');
		redirect(base_url().'admin/notifications');
	}
        
        public function delete($id,$frmSecurity)
	{
		$id=base64_decode($id);
		/*****************Check id is Valid or Not*****************/
                if(empty($id) || !is_numeric($id))
                {
                    $this->utility->setMsg('Invalid Notification Selected','ERROR');
                    redirect(base_url().'admin/notifications');
                }

                if($this->utility->getSecurity()!=$frmSecurity)
                {
                    $this->utility->setMsg('Session doesn\'t matched','ERROR');
                    redirect(base_url().'admin/notifications');
                }
		$isExist=$this->notifications_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
		if(!$isExist)
		{
				$this->utility->setMsg('Invalid Notification Selected','ERROR');
				redirect(base_url().'admin/notifications');
		}
		if($this->utility->getSecurity()!=$frmSecurity)
		{
			$this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
			redirect(base_url().'admin/notifications');
		}
		$this->notifications_model->delete(array('id'=>$this->utility->info_cleanQuery($id)));
		$this->utility->setMsg('Seleted Notification is Now Deleted','SUCCESS');
		redirect(base_url().'admin/notifications');
	}
	
	private function addAction()
	{
		$currentTime=(int)time();
		if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
		{
			$this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
			return false;
		}

		$this->form_validation->set_rules('status','Notification Status','required');
		$this->form_validation->set_rules('msg','Message','required');
		if(!$this->form_validation->run())
		{
			$this->utility->setMsg(validation_errors(),'ERROR');
			return false;
		}
		
		$data=array(
			'msg'=>$this->utility->info_cleanQuery($this->input->post('msg')),
			'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
			'date_of_post'=>$currentTime
		);
		
		$id=$this->notifications_model->addEdit($data);
		$this->utility->setMsg('Saved','SUCCESS');
		redirect(base_url().'admin/notifications/edit/'.base64_encode($id));
	}
        
        private function update($id)
        {
            $currentTime=(int)time();
            $isExist=$this->notifications_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
            {
                $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                return false;
            }

            $this->form_validation->set_rules('status','Notification Status','required');
            $this->form_validation->set_rules('msg','Message','required');
            if(!$this->form_validation->run())
            {
                    $this->utility->setMsg(validation_errors(),'ERROR');
                    return false;
            }

           $data=array(
                    'msg'=>$this->utility->info_cleanQuery($this->input->post('msg')),
                    'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                    'date_of_post'=>$currentTime
            );
            $this->notifications_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Saved','SUCCESS');
            redirect(base_url().'admin/notifications/edit/'.base64_encode($id));
        }
}
?>