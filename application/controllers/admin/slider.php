<?php
require('AppController.php');

/*
This is slider Class v2.2 for CodeIgniter

slider Class Extended from AppController

The slider class is responsible for managing all the skills of a freelancer 

@author: Sandipan Biswas 
*/

class Slider extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('users_model');
        $this->load->model('sliders_model');
        $this->checkLogin();
    }
    
    public function index()
    {
        $data = $this->header_footer('Slider Gallery',array(
                'sub_heading'=>'Slider Gallery Management'
        ));

        $data['rows'] = $this->sliders_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/skills/index',$data);
    }
    
    public function delete_image($id,$frmSecurity)
    {
        $source_path="uploads/slider/";
        $data['id']=$id;
        $id=base64_decode($id);
       
        if(empty($id) || !is_numeric($id))
        {
            $this->utility->setMsg('Invalid Image Selected','ERROR');
            redirect(base_url().'admin/slider');
        }
        
        if($this->utility->getSecurity()!=$frmSecurity)
        {
            $this->utility->setMsg('Session doesn\'t matched','ERROR');
            redirect(base_url().'admin/slider');
        }
        $isExist=$this->sliders_model->fetchRow(array('id'=>$id));
        if($isExist)
        {
            @unlink($source_path.$isExist['image']);
            @unlink($source_path.'thumb/'.$isExist['image']);
            $this->sliders_model->delete(array('id'=>$id));
            $this->utility->setMsg($isExist['image'].' slider image successfully deleted','SUCCESS');
        }
        else
        {
            $this->utility->setMsg('Image not found','ERROR');
        }
        redirect(base_url().'admin/slider');
    }
    
    public function upload()
    {
        $this->load->library('imagetransform');
        $pictures=array();
        $source_path="uploads/slider/";
        if($this->input->post())
        {
            if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
            {
                $this->utility->setMsg('Invalid Security Token','ERROR');
                redirect(base_url().'admin/slider');
            }
            
            foreach($_FILES['upload']['name'] as $key=>$value)
            {
                if($_FILES['upload']['name'][$key]!='')
                {
                    $file='';
                    $up=$this->imagetransform->upload("upload",$source_path,time().rand(0,100),$key,"array");
                    if($up)
                    {
                            $this->imagetransform->setQuality(100);
                            $this->imagetransform->crop($this->imagetransform->main_src,75,75, $source_path.'thumb/'.$this->imagetransform->main_img);
                            $file=$this->imagetransform->main_img;
                    }
                    if($file)
                    {
                        $data=array(
                            'image'=>$file
                        );
                        array_push($pictures,$data);
                    }
                }
            }

            if($pictures)
            {
                $this->sliders_model->insert_batch($pictures);
                $this->utility->setMsg('Successfully Uploaded','SUCCESS');
                redirect(base_url().'admin/slider');
            }
            else
            {
                $this->utility->setMsg('Sorry!! Some Internal problem has been occured','ERROR');
                redirect(base_url().'admin/slider');
            }
        }
        else {
            redirect(base_url().'admin/slider');
        }
    }


    public function view_slider()
    {
        
    }
}
?>