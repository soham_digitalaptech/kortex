<?php
require('AppController.php');
class Package_category extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('package_category_model');

    }
    
    public function index()
    {
        $data = $this->header_footer('Package Category Management',array(
                'sub_heading'=>'Package Category Management'
        ));

        $data['rows'] = $this->package_category_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/package_category/index',$data);
    }
    
    public function add()
    {
            $data = $this->header_footer('Add New Package Category');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }
            
            $this->load->view('admin/package_category/edit',$data);
    }
    
    public function edit($id="")
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Package Category Selected','ERROR');
                redirect(base_url().'admin/package_category');
            }
            
            $isExist = $this->package_category_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Package Category not found','ERROR');
                            redirect(base_url().'admin/package_category');
            }
            
            $data = $this->header_footer('Editing Package Category');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $this->load->view('admin/package_category/edit',$data);
    }
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Package Category Selected','ERROR');
                redirect(base_url().'admin/package_category');
            }
            $isExist = $this->package_category_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Package Category Selected','ERROR');
                            redirect(base_url().'admin/package_category');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/package_category');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->package_category_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Package Category change successfully','SUCCESS');
            redirect(base_url().'admin/package_category');
    }
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Package Category Selected','ERROR');
                redirect(base_url().'admin/package_category');
            }
            
            $isExist = $this->package_category_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Package Category Selected','ERROR');
                            redirect(base_url().'admin/package_category');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/package_category');
            }
            $this->package_category_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Package Category is Deleted','SUCCESS');
            redirect(base_url().'admin/package_category');
    }
    


    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('package_category_name','Package Category Name','required');
        //$this->form_validation->set_rules('category_description','Project Description','required');


    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');

                return false;
        }

        $data = array(
                    'package_category_name'=>$this->utility->info_cleanQuery($this->input->post('package_category_name')),
                    'status'=>$this->utility->info_cleanQuery($this->input->post('status'))
                    );
    


        if($id)
        {
            $this->package_category_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->package_category_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/package_category/edit/'.base64_encode($adsID));
    }
}
?>