<?php
require('AppController.php');
class Settings extends AppController
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
		$this->load->model('site_settings_model');
		$this->checkLogin();
	}
	public function change_password()
	{
		if($this->input->post())
		{
			if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
			{
				$this->utility->setMsg('Session expired.. Please try again..','ERROR');
				redirect(base_url().'admin/settings/change_password');
			}
			$this->form_validation->set_rules('old_password','Old Password','required');
			$this->form_validation->set_rules('new_password','New Password','required');
			if($this->form_validation->run())
			{
				$adminDetails=$this->users_model->fetchRow(array('id'=>$this->session->userdata('admin_id')));
				if($adminDetails['admin_password']!=sha1($this->utility->info_cleanQuery($this->input->post('old_password'))))
				{
					$this->utility->setMsg('Old Password doesn\'t match','ERROR');
					redirect(base_url().'admin/settings/change_password');
				}
				$data=array(
					'admin_password'=>sha1($this->utility->info_cleanQuery($this->input->post('new_password')))
				);
				$this->users_model->addEdit($data,array('id'=>$this->session->userdata('admin_id')));
				$this->utility->setMsg('Password changed','SUCCESS');
				redirect(base_url().'admin/settings/change_password');
			}
			else
			{
				$this->utility->setMsg(validation_errors(),'ERROR');
				redirect(base_url().'admin/settings/change_password');
			}
		}
		$data=$this->header_footer('Change Password');
		$this->load->view('admin/settings/change_password',$data);
	}
	public function general()
	{
		$data=$this->header_footer('General Settings');
		$data['rows']=$this->site_settings_model->fetchRecord(array(),array('order','ASC'));
		$this->load->view('admin/settings/general',$data);
	}
	public function edit($slug)
	{
		if(empty($slug))
		{
			$this->utility->setMsg('Invalid Site Setting Selected','ERROR');
			redirect(base_url().'admin/setiings/general');
		}
		$isExist=$this->site_settings_model->fetchRow(array('slug'=>$this->utility->info_cleanQuery($slug)));
		if(!$isExist)
		{
			$this->utility->setMsg('Invalid Site Setting Selected','ERROR');
			redirect(base_url().'admin/setiings/general');
		}
		if($this->input->post())
		{
			if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
			{
				$this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
				redirect(base_url().'admin/setiings/edit/'.$slug);
			}
			$data=array(
				'setting_value'=>'',
			);
			if($this->input->post('is_text') || $this->input->post('is_textarea'))
			{
				$data['setting_value']=$this->utility->info_cleanQuery($this->input->post('setting_value'));
			}
			if($this->input->post('is_image'))
			{
				$dir="uploads/site_settings/";
				if($_FILES['image']['name']!='')
				{
					$this->load->library('imagetransform');
					$file='';
					$up=$this->imagetransform->upload("image",$dir,time().rand(0,100));
					if($up)
					{
						$this->imagetransform->setQuality(100);
						$file=$this->imagetransform->main_img;
					}
					if($file)
					{
						@unlink($dir.$isExist['setting_value']);
						$data['setting_value']=$file;
					}
				}
			}
			$this->site_settings_model->addEdit($data,array('slug'=>$slug));
			$this->utility->setMsg('Saved','SUCCESS');
			redirect(base_url().'admin/settings/edit/'.$slug);
		}
		$data=$this->header_footer('Editing '.$isExist['setting_name']);
		$data['rows']=$isExist;
		$this->load->view('admin/settings/edit_settings',$data);
	}
}
?>