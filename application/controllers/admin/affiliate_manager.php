<?php
require('AppController.php');
class Affiliate_manager extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('affiliate_manager_model');
        $this->packages = json_decode(PACKAGE_INSTRUCTION,true);
    }
    
    public function index()
    {
        $data = $this->header_footer('Affiliate Management',array(
                'sub_heading'=>'Affiliate Management'
        ));

        $data['rows'] = $this->affiliate_manager_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/affiliate_manager/index',$data);
    }
    
    public function add()
    {
            $data = $this->header_footer('Add New Affiliates');
            
            $data['rows'] = array();
            
            if($this->input->post())
            {
                    if(!$this->update())
                    {
                        $data['rows'] = $this->input->post();
                    }
            }

            $isExist = $this->affiliate_manager_model->fetchColoumns('affiliate_manager');

            if(!empty($isExist))
            {

                $arr = explode("-",$isExist->affiliate_id); 
                $data['new_id'] = $arr[0]."-".($arr[1]++);
            
            }
            else
            {
                $data['new_id'] = "APF-01";
            }

            //$data['new_id'] = $arr[0]+"-"+($arr[1]++);

            $this->load->view('admin/affiliate_manager/edit',$data);
    }
    
    public function edit($id="")
    {
           $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Affiliate Selected','ERROR');
                redirect(base_url().'admin/affiliate_manager');
            }
            
            $isExist = $this->affiliate_manager_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Affiliate not found','ERROR');
                            redirect(base_url().'admin/affiliate_manager');
            }
            
            $data = $this->header_footer('Editing Affiliate');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $this->load->view('admin/affiliate_manager/edit',$data);
    }
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Affiliate Selected','ERROR');
                redirect(base_url().'admin/affiliate_manager');
            }
            $isExist = $this->affiliate_manager_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Affiliate Selected','ERROR');
                            redirect(base_url().'admin/affiliate_manager');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/affiliate_manager');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->affiliate_manager_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Affiliate status change successfully','SUCCESS');
            redirect(base_url().'admin/affiliate_manager');
    }
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Affiliate Selected','ERROR');
                redirect(base_url().'admin/affiliate_manager');
            }
            
            $isExist = $this->affiliate_manager_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Affiliate Selected','ERROR');
                            redirect(base_url().'admin/affiliate_manager');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/affiliate_manager');
            }
            $this->affiliate_manager_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Affiliate is Deleted','SUCCESS');
            redirect(base_url().'admin/affiliate_manager');
    }
    


    private function fillPackageArray($field_name,$value)
    {
        $this->packages[$field_name]['value']=$value;
    }

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!= $this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                return false;
        }

        $this->form_validation->set_rules('affiliate_id','Affiliate Id','required');

        $this->form_validation->set_rules('affiliate_name','Affiliate Name','required');

        $this->form_validation->set_rules('affiliate_type','Affiliate Type','required');

        $this->form_validation->set_rules('affiliate_payment','Affiliate Payment','required');


        if($this->input->post('affiliate_type')=='T')
        {
            $this->form_validation->set_rules('affiliate_text','Affiliate Text','required');
        }

        if($this->input->post('affiliate_payment')=='P')
        {
            $this->form_validation->set_rules('payment_duration','Payment Duration','required');
        }

    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
        }


                    if($_FILES['banner']['name']!='')
                    {

                        $dir="uploads/affiliate/";
               
                
                        if($_FILES['banner']['name']!='')
                        {
                            $this->load->library('imagetransform');
                            $file = '';
                            
                            $up = $this->imagetransform->upload('banner',$dir,time().rand(0,100));

                            if($up)
                            {
                                $this->imagetransform->setQuality(100);
                                $file = $this->imagetransform->main_img;
                            }
                           
                        }    
                    }






                if($this->input->post('affiliate_type') == 'T' && $this->input->post('affiliate_payment') == 'P')
                {
                    $data =  array(
                    'id'=>$this->utility->info_cleanQuery($this->input->post('id')),
                    'affiliate_id'=>$this->utility->info_cleanQuery($this->input->post('affiliate_id')),
                    'affiliate_name'=>$this->utility->info_cleanQuery($this->input->post('affiliate_name')),
                    'affiliate_type'=>$this->utility->info_cleanQuery($this->input->post('affiliate_type')),
                    'affiliate_payment'=>$this->utility->info_cleanQuery($this->input->post('affiliate_payment')), 
                    'payment_duration'=>$this->utility->info_cleanQuery($this->input->post('payment_duration')),
                    'affiliate_text'=>$this->utility->info_cleanQuery($this->input->post('affiliate_text')),
                    'banner'=>''     
                    );

                }
                else if($this->input->post('affiliate_type') == 'T' && $this->input->post('affiliate_payment') != 'P')
                {
                    $data =  array(
                    'id'=>$this->utility->info_cleanQuery($this->input->post('id')),
                    'affiliate_id'=>$this->utility->info_cleanQuery($this->input->post('affiliate_id')),
                    'affiliate_name'=>$this->utility->info_cleanQuery($this->input->post('affiliate_name')),
                    'affiliate_type'=>$this->utility->info_cleanQuery($this->input->post('affiliate_type')),
                    'affiliate_payment'=>$this->utility->info_cleanQuery($this->input->post('affiliate_payment')), 
                    'affiliate_text'=>$this->utility->info_cleanQuery($this->input->post('affiliate_text')),
                    'banner'=>''     
                    );

                }
                else if($this->input->post('affiliate_type') == 'B' && $this->input->post('affiliate_payment') == 'P')
                {
                    $data =  array(
                    'id'=>$this->utility->info_cleanQuery($this->input->post('id')),
                    'affiliate_id'=>$this->utility->info_cleanQuery($this->input->post('affiliate_id')),
                    'affiliate_name'=>$this->utility->info_cleanQuery($this->input->post('affiliate_name')),
                    'affiliate_type'=>$this->utility->info_cleanQuery($this->input->post('affiliate_type')),
                    'affiliate_payment'=>$this->utility->info_cleanQuery($this->input->post('affiliate_payment')), 
                    'payment_duration'=>$this->utility->info_cleanQuery($this->input->post('payment_duration')),
                    'affiliate_text'=>'', 
                    'banner'=>$this->utility->info_cleanQuery($file)     
                    );
                }
                else if($this->input->post('affiliate_type') == 'B' && $this->input->post('affiliate_payment') != 'P')
                {
                    $data =  array(
                    'id'=>$this->utility->info_cleanQuery($this->input->post('id')),
                    'affiliate_id'=>$this->utility->info_cleanQuery($this->input->post('affiliate_id')),
                    'affiliate_name'=>$this->utility->info_cleanQuery($this->input->post('affiliate_name')),
                    'affiliate_type'=>$this->utility->info_cleanQuery($this->input->post('affiliate_type')),
                    'affiliate_payment'=>$this->utility->info_cleanQuery($this->input->post('affiliate_payment')),
                    'affiliate_text'=>'', 
                    'banner'=>$this->utility->info_cleanQuery($file)     
                    );
                }
               

        if($id)
        {
            $this->affiliate_manager_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->affiliate_manager_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/affiliate_manager/edit/'.base64_encode($adsID));
    }
}
?>