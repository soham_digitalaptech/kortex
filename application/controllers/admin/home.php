<?php
require('AppController.php');
class Home extends AppController
{
	public function __construct()
	{
      parent:: __construct();
	  $this->checkLogin();
	}
	public function index()
	{
		$data=$this->header_footer('Dashboard',array(
													'sub_heading'=>'statistics and more'
												));
		$this->load->view('admin/dashboard',$data);
	}
}
?>