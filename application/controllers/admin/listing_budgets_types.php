<?php
require('AppController.php');
class Listing_budgets_types extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('listing_budgets_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Listing budgets Management',array(
                'sub_heading'=>'Listing budgets Management'
        ));

        $data['rows'] = $this->listing_budgets_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/listing_budgets/index',$data);
    }
    
    public function add()
    {
        $data = $this->header_footer('Add New Listing budgets');
            
        $data['rows'] = array();

        if($this->input->post())
        {
            if(!$this->update())
                $data['rows'] = $this->input->post();
        }

        $data['projectList'] = $this->listing_budgets_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
            
        $this->load->view('admin/listing_budgets/edit',$data);
    }
    
    public function edit($id="")
    {
            $id = base64_decode($id);

            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Listing budgets Selected','ERROR');
                redirect(base_url().'admin/listing_budgets');
            }
            
            $isExist = $this->listing_budgets_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            $isExist = $this->listing_budgets_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Listing budgets not found','ERROR');
                            redirect(base_url().'admin/listing_budgets');
            }
            
            $data = $this->header_footer('Editing Project Types');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->listing_budgets_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

            $this->load->view('admin/listing_budgets/edit',$data);
    }
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Listing budgets Selected','ERROR');
                redirect(base_url().'admin/listing_budgets');
            }
            $isExist = $this->listing_budgets_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Listing budgets Selected','ERROR');
                            redirect(base_url().'admin/listing_budgets');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/listing_budgets');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->listing_budgets_model->addEdit($data,array('id'=>$id));

            $this->utility->setMsg('Listing budgets status change successfully','SUCCESS');
            
            redirect(base_url().'admin/listing_budgets');
    }
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Listing budgets Selected','ERROR');
                redirect(base_url().'admin/listing_budgets');
            }
            
            $isExist = $this->listing_budgets_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Listing budgets Selected','ERROR');
                            redirect(base_url().'admin/listing_budgets');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                    redirect(base_url().'admin/listing_budgets');
            }
            
            $this->listing_budgets_model->addEdit(array('status'=>3),array('id'=>$id));

            $this->utility->setMsg($isExist['ads_size'].'Listing budgets is Deleted','SUCCESS');
            
            redirect(base_url().'admin/listing_budgets');
    }
    


    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!= $this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('description','Static Project Title','required');
        $this->form_validation->set_rules('price','Static Project Title','required');
    
    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
        }
    
        if($id)
        {
            $data = array(
                'description' => $this->utility->info_cleanQuery($this->input->post('description')),
                'price' => $this->utility->info_cleanQuery($this->input->post('price')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_creation' => date('Y-m-d')
                );

            $this->listing_budgets_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
        {
            $data = array(
                'description' => $this->utility->info_cleanQuery($this->input->post('description')),
                'price' => $this->utility->info_cleanQuery($this->input->post('price')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_creation' => date('Y-m-d')
                );

            $adsID = $this->listing_budgets_model->addEdit($data);
        }
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/listing_budgets/edit/'.base64_encode($adsID));

    }
}
?>