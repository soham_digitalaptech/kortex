<?php
require('AppController.php');

/*
This is project category Class v2.2 for CodeIgniter

project_category Class Extended from AppController

The project_category class is responsible for managing all the skills of a freelancer 

@author: Sandipan Biswas 
*/

class Project_category extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('project_category_model');

    }
    
    public function index()
    {
        $data = $this->header_footer('Project Category Management',array(
                'sub_heading'=>'Project Category Management'
        ));

        $data['rows'] = $this->project_category_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/category/index',$data);
    }

    /*
    This is a method for Adding project category. 

    @author: Sandipan Biswas

    @scope: public

    @return: void   
    */
    
    public function add()
    {
            $data = $this->header_footer('Add New Category');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }
            
            $this->load->view('admin/category/edit',$data);
    }

    /*
    This is a method for Editing project category. 

    @author: Sandipan Biswas

    @scope: public

    @parameters: $id

    @return: void   
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Category Selected','ERROR');
                redirect(base_url().'admin/project_category');
            }
            
            $isExist = $this->project_category_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Project Category not found','ERROR');
                            redirect(base_url().'admin/project_category');
            }
            
            $data = $this->header_footer('Editing Project Category');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $this->load->view('admin/category/edit',$data);
    }
    
    /*
    This is a method for changing the status of the project category from active to in active and vice versa. 

    @author: Sandipan Biswas

    @scope: public

    @parameters: $id,$frmSecurity

    @return: void   
    */

    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Category Selected','ERROR');
                redirect(base_url().'admin/project_category');
            }
            $isExist = $this->project_category_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Category Selected','ERROR');
                            redirect(base_url().'admin/project_category');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/project_category');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;




            $this->project_category_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Project Category change successfully','SUCCESS');
            redirect(base_url().'admin/project_category');
    }

    /*
    This is a method for deleting the project category. 

    @author(s): Sandipan Biswas

    @scope: public

    @parameters: $id,$frmSecurity

    @return: void   
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Skill Selected','ERROR');
                redirect(base_url().'admin/skills');
            }
            
            $isExist = $this->skills_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Skill Selected','ERROR');
                            redirect(base_url().'admin/skills');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/skills');
            }
            $this->skills_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Skill is Deleted','SUCCESS');
            redirect(base_url().'admin/skills');
    }
    
    /*
    This is a method for add/edit the project category. 

    @author: Sandipan Biswas

    @access: private

    @parameters: $id

    @return: void   
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }

        $this->form_validation->set_rules('category_title','Project Title','required');
        $this->form_validation->set_rules('category_description','Project Description','required');
    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');

                return false;
        }

        $data = array(
                    'category_title'=>$this->utility->info_cleanQuery($this->input->post('category_title')),
                    'category_description'=>$this->utility->info_cleanQuery($this->input->post('category_description')),
                    'status'=>$this->utility->info_cleanQuery($this->input->post('status'))
                    );

        if($id)
        {
            $this->project_category_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->project_category_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/project_category/edit/'.base64_encode($adsID));
    }
}
?>