<?php
require('AppController.php');
class AffiliateSettings extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('affiliate_settings_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Affiliate settings Management',array(
                'sub_heading'=>'Affiliate settings'
        ));
        $data['rows'] = $this->affiliate_settings_model->fetchRecord(array(),array('id','desc'));
        $this->load->view('admin/affiliate_settings/index',$data);
    }
    
    public function add()
    {
            $data = $this->header_footer('Add New Banners');
            $data['rows'] = array();
            
            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }
            

            $this->load->view('admin/affiliate_settings/edit',$data);
    }

    public function viewBanners()
    {
        $data = $this->header_footer('Affiliate Banners',array(
                'sub_heading'=>'Affiliate Banners'
        ));

        $data['rows'] = $this->affiliate_settings_model->fetchRecord(array(),array('id','desc'));
        $this->load->view('admin/affiliate_settings/affiliateBanners',$data);
    }

    public function viewText()
    {
        $data = $this->header_footer('Affiliate Banner Description',array(
                'sub_heading'=>'Affiliate Banner Description'
        ));

        $data['rows'] = $this->affiliate_settings_model->fetchRecord(array(),array('id','desc'));
        $this->load->view('admin/affiliate_settings/affiliateBannerText',$data);
    }
    
    public function edit($id="")
    {
            $id = base64_decode($id);
            
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Banner Selected','ERROR');
                redirect(base_url().'admin/affiliateSettings/viewBanners');
            }

            $isExist = $this->affiliate_settings_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            if(!$isExist)
            {
                            $this->utility->setMsg('Banners not found','ERROR');
                            redirect(base_url().'admin/affiliateSettings/viewBanners');
            }
            $data = $this->header_footer('Editing Banners');
            $data['rows'] = $isExist;
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }
            $this->load->view('admin/affiliate_settings/edit',$data);
    }
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Banner Selected','ERROR');
                redirect(base_url().'admin/affiliateSettings/viewBanners');
            }

            $isExist=$this->affiliate_settings_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Banner Selected','ERROR');
                            redirect(base_url().'admin/affiliateSettings/viewBanners');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/affiliateSettings/viewBanners');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;
            
            $this->affiliate_settings_model->addEdit($data,array('id'=>$id));

            if($data['status'])
            {
                $msg="Active";
            }
            else
            {
                $msg='Inactive';
            }

            $this->utility->setMsg($isExist['banner'].' is Now '.$msg,'SUCCESS');
            redirect(base_url().'admin/affiliateSettings/viewBanners');
    }
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Banner Selected','ERROR');
                redirect(base_url().'admin/affiliateSettings/viewBanners');
            }
            $isExist = $this->affiliate_settings_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Banner Selected','ERROR');
                            redirect(base_url().'admin/affiliateSettings/viewBanners');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/affiliateSettings/viewBanners');
            }
            $this->affiliate_settings_model->delete(array('id'=>$id));
            $this->utility->setMsg('Banner is Deleted','SUCCESS');
            redirect(base_url().'admin/affiliateSettings/viewBanners');
    }
    
    private function update($id="")
    {
        $currentTime = (int)time();
        if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                return false;
        }
      
        $this->form_validation->set_rules('banner_size','Banner Size','required');
          
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
        }

                        $dir="uploads/affiliate/";
                             
                        if($_FILES['banner_image']['name']!='')
                        {
                            $this->load->library('imagetransform');
                            $file = '';
                            
                            $up = $this->imagetransform->upload("banner_image",$dir,time().rand(0,100));

                            if($up)
                            {
                                $this->imagetransform->setQuality(100);
                                $file = $this->imagetransform->main_img;
                            }
                            
                            
                            $data = array(
                                    'banner_size'=>$this->utility->info_cleanQuery($this->input->post('banner_size')),
                                    'banner'=>$this->utility->info_cleanQuery($file)               
                                );
                           
                        }
                        else
                        {
                            $data = array(
                                    'banner_size'=>$this->utility->info_cleanQuery($this->input->post('banner_size'))           
                                );
                        }
                            
        if($id)
        {
            $this->affiliate_settings_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->affiliate_settings_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/affiliateSettings/edit/'.base64_encode($adsID));
    }
}
?>