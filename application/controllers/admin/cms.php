<?php
require('AppController.php');
class CMS extends AppController
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
        $this->load->model('cms_model');
		$this->checkLogin();
	}
	public function index()
	{
		$data=$this->header_footer('CMS Pages');
		$data['rows']=$this->cms_model->fetchRecord(array(),array('order','asc'));
		$this->load->view('admin/cms/index',$data);
	}

	/*
    This is a method for Adding cms pages. 

    @author: Sandipan Biswas

    @access: public

    @return: void   
    */

	public function add()
	{
		$data=$this->header_footer('Add New Page');
		$data['rows']=array();
		if($this->input->post())
		{
			if(!$this->addAction())
				$data['rows']=$this->input->post();
		}
		$this->load->view('admin/cms/edit',$data);
	}

	/*
    This is a method for Editing cms pages. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $slug

    @return: void   
    */

	public function edit($slug)
	{
		if(empty($slug))
		{
				$this->utility->setMsg('Invalid Page Selected','ERROR');
				redirect(base_url().'admin/cms');
		}
		$isExist=$this->cms_model->fetchRow(array('slug'=>$this->utility->info_cleanQuery($slug)));
		if(!$isExist)
		{
				$this->utility->setMsg('Invalid Page Selected','ERROR');
				redirect(base_url().'admin/cms');
		}
		$data=$this->header_footer('Editing '.$isExist['title'].' Page');
		$data['rows']=$isExist;
		if($this->input->post())
		{
			if(!$this->update($slug))
				$data['rows']=$this->input->post();
		}
		$this->load->view('admin/cms/edit',$data);
	}


	/*
    This is a method for Changing the status of the cms pages from active to inactive and vice versa. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $slug,$frmSecurity

    @return: void   
    */
	
	public function change_status($slug,$frmSecurity)
	{
		$status=array('Blocked','Active');
		if(empty($slug))
		{
				$this->utility->setMsg('Invalid Page Selected','ERROR');
				redirect(base_url().'admin/cms');
		}
		$isExist=$this->cms_model->fetchRow(array('slug'=>$this->utility->info_cleanQuery($slug)));
		if(!$isExist)
		{
				$this->utility->setMsg('Invalid Page Selected','ERROR');
				redirect(base_url().'admin/cms');
		}
		if($this->utility->getSecurity()!=$frmSecurity)
		{
			$this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
			redirect(base_url().'admin/cms');
		}
		$data=array(
			'status'=>1
		);
		if($isExist['status'])
			$data['status']=0;
		$this->cms_model->addEdit($data,array('slug'=>$slug));
		$this->utility->setMsg($isExist['title'].' Page is Now '.$status[$data['status']],'SUCCESS');
		redirect(base_url().'admin/cms');
	}

	/*
    This is a method for Changing the order of the cms pages. 

    @author: Sandipan Biswas

    @access: public

    @return: void   
    */
	
	public function change_order()
	{
		$productID=base64_decode($this->input->post('product_id'));
		$order=$this->input->post('order');
		if(empty($productID) || !is_numeric($productID))
		{
			$this->utility->setMsg('System tracked invalid cms selected','ERROR');
			redirect(base_url().'admin/cms');
		}
		if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
		{
			$this->utility->setMsg('Your security token has been expired..please try again','ERROR');
			redirect(base_url().'admin/cms');
		}
		
		$this->form_validation->set_rules('order','Order','required|numeric');
		if(!$this->form_validation->run())
		{
			$this->utility->setMsg(validation_errors(),'ERROR');
			redirect(base_url().'admin/cms');
		}
		$currentProduct=$this->cms_model->fetchRow(array('id'=>$productID));
		if(!$currentProduct)
		{
			$this->utility->setMsg('Selected cms not found','ERROR');
			redirect(base_url().'admin/cms');

		}
		$allSameTypeProducts=$this->cms_model->fetchRecord(array(),array('order','asc'));		
		$resultArray=$this->setOrder($currentProduct,$order,$allSameTypeProducts);
		foreach($resultArray as $value)
		{
			$this->cms_model->addEdit(array('order'=>$value['order']),array('id'=>$value['id']));
		}
		$this->utility->setMsg('Order Successfully Change');
		redirect(base_url().'admin/cms');
	}
	
	private function addAction()
	{
		$currentTime=(int)time();
		if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
		{
			$this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
			return false;
		}

		$this->form_validation->set_rules('status','Page Status','required');
		$this->form_validation->set_rules('is_in_menu','Is in Navigation Menu','required');
		$this->form_validation->set_rules('title','Title','required');
		$this->form_validation->set_rules('short_description','Short Description','required');
		$this->form_validation->set_rules('descriptionx','Description','required');
		if(!$this->form_validation->run())
		{
			$this->utility->setMsg(validation_errors(),'ERROR');
			return false;
		}
		
		$data=array(
			'title'=>$this->utility->info_cleanQuery($this->input->post('title')),
			'slug'=>str_replace(' ','-',strtolower($this->utility->info_cleanQuery($this->input->post('title')))),
			'short_description'=>$this->utility->info_cleanQuery($this->input->post('short_description')),
			'description'=>$this->utility->info_cleanQuery($this->input->post('descriptionx')),
			'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
			'is_in_menu'=>$this->utility->info_cleanQuery($this->input->post('is_in_menu')),
			'meta_title'=>$this->utility->info_cleanQuery($this->input->post('meta_title')),
			'meta_keyword'=>$this->utility->info_cleanQuery($this->input->post('meta_keyword')),
			'meta_description'=>$this->utility->info_cleanQuery($this->input->post('meta_description')),
			'date_of_modified'=>$currentTime,
			'date_of_created'=>$currentTime
		);
		$max=1;
		$sameTypeProduct=$this->cms_model->fetchRecord();
		foreach($sameTypeProduct as $value)
		{
			if($value['order']>=$max)
				$max=$value['order']+1;
		}
		$data['order']=$max;
		
		$id=$this->cms_model->addEdit($data);
		$isExist1=$this->cms_model->fetchRow(array('slug'=>$data['slug'],'id <>'=>$id));
		if($isExist1)
		{
			$data['slug']=$data['slug'].'-'.$id;
			$this->cms_model->addEdit($data);
		}
		$this->utility->setMsg('Saved','SUCCESS');
		redirect(base_url().'admin/cms/edit/'.$data['slug']);
	}
        
    /*
    This is a method for add/edit the cms pages. 

    @author: Sandipan Biswas

    @access: private

    @parameters: $slug

    @return: void   
    */

        private function update($slug)
        {
            $currentTime=(int)time();
            $isExist=$this->cms_model->fetchRow(array('slug'=>$this->utility->info_cleanQuery($slug)));
            if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
            {
                $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                return false;
            }

            $this->form_validation->set_rules('status','Page Status','required');
            $this->form_validation->set_rules('is_in_menu','Is in Navigation Menu','required');
            $this->form_validation->set_rules('title','Title','required');
            $this->form_validation->set_rules('short_description','Short Description','required');
            $this->form_validation->set_rules('descriptionx','Description','required');
            $this->form_validation->set_rules('slug','Page URL','required');
            if(!$this->form_validation->run())
            {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
            }
            
            $isExist1=$this->cms_model->fetchRow(array('slug'=>$this->utility->info_cleanQuery($this->input->post('slug')),'slug <>'=>$slug));
            
            if($isExist1)
            {
                $this->utility->setMsg('Your url is alloted for '.$isExist1['title'].'<br/>Please try another one.','ERROR');
                return false;
            }

            $data=array(
                'title'=>$this->utility->info_cleanQuery($this->input->post('title')),
                'slug'=>$this->utility->info_cleanQuery($this->input->post('slug')),
                'short_description'=>$this->utility->info_cleanQuery($this->input->post('short_description')),
                'description'=>$this->utility->info_cleanQuery($this->input->post('descriptionx')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'is_in_menu'=>$this->utility->info_cleanQuery($this->input->post('is_in_menu')),
                'meta_title'=>$this->utility->info_cleanQuery($this->input->post('meta_title')),
                'meta_keyword'=>$this->utility->info_cleanQuery($this->input->post('meta_keyword')),
                'meta_description'=>$this->utility->info_cleanQuery($this->input->post('meta_description')),
                'date_of_modified'=>$currentTime
            );

            if(!$isExist['date_of_created'])
                $data['date_of_created']=$currentTime;

            $this->cms_model->addEdit($data,array('slug'=>$slug));
            $this->utility->setMsg('Saved','SUCCESS');
            redirect(base_url().'admin/cms/edit/'.$data['slug']);
        }
}
?>