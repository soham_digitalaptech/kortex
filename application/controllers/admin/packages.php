<?php
require('AppController.php');

/*
This is Packages Class v2.2 for CodeIgniter

packages Class Extended from AppController

The packages class is responsible for managing all the packages 

@author: Sandipan Biswas 
*/

class Packages extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('packages_model');
        $this->packages = json_decode(PACKAGE_INSTRUCTION,true);
    }
    
    public function index()
    {
        $data = $this->header_footer('Packages Management',array(
                'sub_heading'=>'site Packages'
        ));

        $data['rows'] = $this->packages_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
        
        $data['packages'] = $this->packages_model->fetchRecord(array('status <> '=> 3));

        $data['package_category'] = $this->packages_model->fetchPackageCategory('package_category',array('status <> '=> 0));

        $this->load->view('admin/packages/index',$data);
    }

    /*
    This is a method for Adding package,package cost,package offer cost,package annual cost,
    annual offer cost,package logo,bids per month. 

    @author: Sandipan Biswas

    @access: public

    @return: void    
    */
    
    public function add()
    {
        $data = $this->header_footer('Add New Package');
        $data['rows'] = array();
 

        if($this->input->post())
        {
            foreach($this->packages as $fieldName=>$value)
            {
                if(in_array('required',$value['rules']))
                {

                    if(!$this->input->post($fieldName))
                    {
                        $this->utility->setMsg($fieldName.' is required field');


                        redirect(base_url().'admin/packages/add');
                    }

                   
                }

                    if($_FILES[$fieldName]['name']!='')
                    {

                        $dir="uploads/package/";
               
                
                        if($_FILES[$fieldName]['name']!='')
                        {
                            $this->load->library('imagetransform');
                            $file = '';
                            
                            $up = $this->imagetransform->upload($fieldName,$dir,time().rand(0,100));

                            if($up)
                            {
                                $this->imagetransform->setQuality(100);
                                $file = $this->imagetransform->main_img;
                            }
                           
                        }


                        $this->fillPackageArray($fieldName,$file);
                    }
                    else
                    {
                        $this->fillPackageArray($fieldName,$this->input->post($fieldName));
                    }
                 
            }


                
            $insert=array(
                'status'=>1,
                'others'=>serialize($this->packages)
            );
            $id = $this->packages_model->addEdit($insert);

            redirect(base_url().'admin/packages');
         
        }
        
            $data['package_instruction'] = $this->packages;


            $this->load->view('admin/packages/edit',$data);
    }

    /*
    This is a method for Editing packages. 

    @author: Sandipan Biswas

    @access: public

    @parameters: id

    @return: void    
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Packages Selected','ERROR');
                redirect(base_url().'admin/packages');
            }
            $isExist = $this->packages_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Packages not found','ERROR');
                            redirect(base_url().'admin/packages');
            }
            $data = $this->header_footer('Editing Packages');

            $data['package_instruction'] = unserialize($isExist['others']);
            
            


            ////////////////////////////////////
            if($this->input->post())
            {
    
                foreach($this->packages as $fieldName=>$value)
                {
                    if(in_array('required',$value['rules']))
                    {

                        if(!$this->input->post($fieldName))
                        {
                            $this->utility->setMsg($fieldName.' is required field');
                            redirect(base_url().'admin/packages/add');
                        }
                       
                    }

                    

                    if($_FILES[$fieldName]['name']!='')
                    {

                        $dir="uploads/package/";
               
                
                        if($_FILES[$fieldName]['name']!='')
                        {
                            $this->load->library('imagetransform');
                            $file = '';
                            
                            $up = $this->imagetransform->upload($fieldName,$dir,time().rand(0,100));

                            if($up)
                            {
                                $this->imagetransform->setQuality(100);
                                $file = $this->imagetransform->main_img;
                            }
                           
                        }


                        $this->fillPackageArray($fieldName,$file);
                    }
                    else
                    {
                        $this->fillPackageArray($fieldName,$this->input->post($fieldName));
                    }
                }

                

                $insert=array(
                    'status'=>1,

                    'others'=>serialize($this->packages)
                );

                $id = $this->packages_model->addEdit($insert,array('id'=>$id));
                
                redirect(base_url().'admin/packages');
            }
          


            $data['rows']=array('id'=>$id);
            $this->load->view('admin/packages/edit',$data);
    }
    
    /*
    This is a method for Changing the status of the packages from active to inactive and vice versa. 

    @author: Sandipan Biswas

    @scope: public

    @return: void    
    */
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Package Selected','ERROR');
                redirect(base_url().'admin/packages');
            }
            $isExist = $this->packages_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Package Selected','ERROR');
                            redirect(base_url().'admin/packages');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/packages');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->packages_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Package status change successfully','SUCCESS');
            redirect(base_url().'admin/packages');
    }
    
    /*
    This is a method for deleting packages. 

    @author: Sandipan Biswas

    @scope: public

    @parameters: $id,$frmSecurity

    @return: void    
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Package Selected','ERROR');
                redirect(base_url().'admin/packages');
            }
            
            $isExist = $this->packages_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Package Selected','ERROR');
                            redirect(base_url().'admin/packages');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/packages');
            }
            $this->packages_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Package is Deleted','SUCCESS');
            redirect(base_url().'admin/packages');
    }
    
    /*
    This is a method for filling the packages into the array. 

    @author: Sandipan Biswas

    @access: private

    @parameters: $field_name,$value

    @return: void    
    */

    private function fillPackageArray($field_name,$value)
    {
        $this->packages[$field_name]['value']=$value;
    }

    /*
    This is a method for fetching the packages from an array. 

    @author(s): Sandipan Biswas

    @access: public

    @return: void    
    */

    public function getPackages()
    {
        $id = $this->input->post('id');

        $data['packages'] = $this->packages_model->fetchPackages('packages',array('package_category_id = '=> $id));

        $new_array = array();

        $i=0;

        foreach($data['packages'] as $key => $value) 
        {
            $arr[] = unserialize($value->others);
        }

        $data['rows'] = $this->packages_model->fetchRecord(array('package_category_id = '=> $id),array('id','desc'));

        $data['packages'] = $this->packages_model->fetchRecord(array('package_category_id = '=> $id));

        $data['package_category'] = $this->packages_model->fetchPackageCategory('package_category',array('status <> '=> 0));

        $this->load->view('admin/packages/ajax/index',$data);

    }
}
?>