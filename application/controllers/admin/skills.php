<?php
require('AppController.php');

/*
skills Class Extended from AppController

The skills class is responsible for managing all the skills of a freelancer 

@author: Sandipan Biswas 
*/

class Skills extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('skills_model');

    }
    
    public function index()
    {
        $data = $this->header_footer('Skill Management',array(
                'sub_heading'=>'Skill Management'
        ));

        $data['rows'] = $this->skills_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/skills/index',$data);
    }

    /*
    This is a method for Adding skills. 

    @author: Sandipan Biswas

    @access: public

    @return: void    
    */
    
    public function add()
    {
            $data = $this->header_footer('Add New Skills');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }
            
            $this->load->view('admin/skills/edit',$data);
    }

    /*
    This is a method for Editing skills. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $id

    @return: void    
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Skill Selected','ERROR');
                redirect(base_url().'admin/skill');
            }
            
            $isExist = $this->skills_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Skill not found','ERROR');
                            redirect(base_url().'admin/skill');
            }
            
            $data = $this->header_footer('Editing Skills');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $this->load->view('admin/skills/edit',$data);
    }


    /*
    This is a method for Changing the status of skills from active to inactive and vice versa. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $id,$frmSecurity

    @return: void    
    */
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Skill Selected','ERROR');
                redirect(base_url().'admin/skills');
            }
            $isExist = $this->skills_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Skills Selected','ERROR');
                            redirect(base_url().'admin/skills');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/skills');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;




            $this->skills_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Skill status change successfully','SUCCESS');
            redirect(base_url().'admin/skills');
    }
    

    /*
    This is a method for Deleting the skills of the freelancers. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $id,$frmSecurity

    @return: void    
    */

    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Skill Selected','ERROR');
                redirect(base_url().'admin/skills');
            }
            
            $isExist = $this->skills_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Skill Selected','ERROR');
                            redirect(base_url().'admin/skills');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/skills');
            }
            $this->skills_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Skill is Deleted','SUCCESS');
            redirect(base_url().'admin/skills');
    }
    

    /*
    This is a method for add/edit the skills of a freelancer. 

    @author: Sandipan Biswas

    @access: private

    @parameters: $id

    @return: void    
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('skill','Skill','required');

    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');

                return false;
        }

        $data = array(
                    'skill'=>$this->utility->info_cleanQuery($this->input->post('skill')),
                    'status'=>$this->utility->info_cleanQuery($this->input->post('status'))
                    );
    


        if($id)
        {
            $this->skills_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->skills_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/skills/edit/'.base64_encode($adsID));
    }
}
?>