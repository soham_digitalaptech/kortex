<?php
require('AppController.php');

/*
This is Slider_master Class for CodeIgniter

Slider_master Class Extended from AppController

The Slider_master class is responsible for managing all the sliders 

@author: Sandipan Biswas 
*/

class Slider_master extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('users_model');
        $this->load->model('slider_master_model');
        $this->load->model('sliders_model');
        $this->checkLogin();
    }
    
    public function index()
    {
        $data = $this->header_footer('Slider Master',array(
                'sub_heading'=>'Slider Master Management'
        ));

        $data['rows'] = $this->slider_master_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/slider_master/index',$data);
    }

    /*
    This is a method for adding the Slider master. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */

    public function add()
    {
            $data = $this->header_footer('Add New slider');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }
            
            $this->load->view('admin/slider_master/edit',$data);
    }



    /*
    This is a method for editing the Slider master. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id

    @return: void
    */

    public function edit($id="")
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Slider Selected','ERROR');
                redirect(base_url().'admin/slider_master');
            }
            
            $isExist = $this->slider_master_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Slider not found','ERROR');
                            redirect(base_url().'admin/slider_master');
            }
            
            $data = $this->header_footer('Editing Slider');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $this->load->view('admin/slider_master/edit',$data);
    }
    
    /*
    This is a method for deleting the slider. 

    @author(s): Sandipan Biswas 

    @parameter: $id,$frmSecurity

    @access: public

    @return: void
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Slider Selected','ERROR');
                redirect(base_url().'admin/slider_master');
            }
            
            $isExist = $this->slider_master_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Slider Selected','ERROR');
                            redirect(base_url().'admin/slider_master');
            }

            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/slider_master');
            }
          
            $this->slider_master_model->addEdit(array('status'=>3),array('id'=>$id));

            $this->utility->setMsg($isExist['ads_size'].'Slider is Deleted','SUCCESS');
            redirect(base_url().'admin/slider_master');
    }
    
    /*
    This is a method for deleting the slider images from a slider. 

    @author(s): Digitalaptech

    @parameter: $slider_master_id, $id,$frmSecurity

    @access: public

    @return: void
    */
    public function delete_image($slider_master_id="",$id="",$frmSecurity="")
    {
        $source_path="uploads/slider/";
        $data['id']=$id;
        $id=base64_decode($id);
        $slider_master_id=  base64_decode($slider_master_id);
        
        if(empty($slider_master_id) || !is_numeric($slider_master_id))
        {
            $this->utility->setMsg('Invalid Image Selected','ERROR');
            redirect(base_url().'admin/slider_master');
        }
        
        if(empty($id) || !is_numeric($id))
        {
            $this->utility->setMsg('Invalid Image Selected','ERROR');
            redirect(base_url().'admin/slider_master/view_slider/'.  base64_encode($slider_master_id));
        }
        
        if($this->utility->getSecurity()!=$frmSecurity)
        {
            $this->utility->setMsg('Session doesn\'t matched','ERROR');
            redirect(base_url().'admin/slider_master/view_slider/'.  base64_encode($slider_master_id));
        }
        $isExist=$this->sliders_model->fetchRow(array('id'=>$id));
        if($isExist)
        {
            @unlink($source_path.$isExist['image']);
            @unlink($source_path.'thumb/'.$isExist['image']);
            $this->sliders_model->delete(array('id'=>$id));
            $this->utility->setMsg($isExist['image'].' slider image successfully deleted','SUCCESS');
        }
        else
        {
            $this->utility->setMsg('Image not found','ERROR');
        }
        redirect(base_url().'admin/slider_master/view_slider/'.  base64_encode($slider_master_id));
    }
    
    
    /*
    This is a method for uploading the multiple slider images of individual master slider. 

    @author: Sandipan Biswas 

    @return: void    
    */

    public function upload()
    {
        $this->load->library('imagetransform');
        $pictures = array();
        $source_path = "uploads/slider/";

        if($this->input->post())
        {
            if($this->utility->getSecurity() != $this->input->post('frmSecurity'))
            {
                $this->utility->setMsg('Invalid Security Token','ERROR');
                redirect(base_url().'admin/slider_master');
            }
            
            foreach($_FILES['upload']['name'] as $key => $value)
            { 
                if($_FILES['upload']['name'][$key]!='')
                {
                    $file = '';
                    $up = $this->imagetransform->upload("upload",$source_path,time().rand(0,100),$key,"array");
                    
                    if($up)
                    {
                        $this->imagetransform->setQuality(100);

                        $this->imagetransform->crop($this->imagetransform->main_src,75,75, $source_path.'thumb/'.$this->imagetransform->main_img);

                        $file = $this->imagetransform->main_img;
                    }

                    if($file)
                    {
                        $data=array(
                            'image'=>$file
                        );
                        array_push($pictures,$data);
                    }
                }
            }


            if($pictures)
            {
                $id=  base64_decode($this->input->post('id'));
                if(!$id || !is_numeric($id))
                {
                    $this->utility->setMsg('Invalid slider master id','ERROR');
                    redirect(base_url().'admin/slider_master');
                }
                $this->slider_master_model->insert_images($pictures,$id);

                $this->utility->setMsg('Successfully Uploaded','SUCCESS');
                
                redirect(base_url().'admin/slider_master/view_slider/'.$this->input->post('id'));
            }
            else
            {
                $this->utility->setMsg('Sorry!! Some Internal problem has been occured','ERROR');
                
                redirect(base_url().'admin/slider_master/view_slider/'.$this->input->post('id'));
            }
        }
        else 
        {
            redirect(base_url().'admin/slider_master');
        }
    }


    /*
    This is a method for view and manage(add/edit/delete) the slider images of the master slider. 

    @author(s): Sandipan Biswas 

    @parameter: $id

    @access: public

    @return: void
    */

    public function view_slider($id="")
    {
        $id = base64_decode($id);

        
        if(!$id || !is_numeric($id))
        {
            $this->utility->setMsg('Invalid Slider Selected','ERROR');
            redirect(base_url().'admin/slider_master');
        }
            
        $data = $this->header_footer('Slider Master',array(
                'sub_heading'=>'Slider Master Management'
        ));

        
        $data['images'] = $this->slider_master_model->getImages($id);

        $data['id'] = $id;
        $data['slider_master_id']=  base64_encode($id);

        $this->load->view('admin/slider_master/view_slider',$data);
    }


    


    /*
    This is a method for changing the status of the Slider Master from active to inactive and vice versa. 

    @author: Sandipan Biswas 

    @scope: public

    @parameter: $id,$frmSecurity

    @return: void
    */

    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Slider Selected','ERROR');
                redirect(base_url().'admin/slider_master');
            }
            
            $isExist = $this->slider_master_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Slider Selected','ERROR');
                            redirect(base_url().'admin/slider_master');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/slider_master');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->slider_master_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Slider Master status change successfully','SUCCESS');
            redirect(base_url().'admin/slider_master');
    }




    /*
    This is a method for adding/editing the Slider Master. 

    @author: Sandipan Biswas 

    @scope: private

    @parameter: $id

    @return: void
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('slider_name','Slider Name','required');

    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');

                return false;
        }

        $data = array(
                    'slider_name'=>$this->utility->info_cleanQuery($this->input->post('slider_name')),
                    'status'=>$this->utility->info_cleanQuery($this->input->post('status'))
                    );

        if($id)
        {
            $this->slider_master_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->slider_master_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/slider_master/edit/'.base64_encode($adsID));
    }
}
?>