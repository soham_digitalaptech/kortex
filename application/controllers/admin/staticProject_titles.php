<?php
require('AppController.php');
class Static_project_titles extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('static_project_titles_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Project Title Management',array(
                'sub_heading'=>'Project Title Management'
        ));

        $data['rows'] = $this->static_project_titles_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/static_project_titles/index',$data);
    }
    
    public function add()
    {
            $data = $this->header_footer('Add New Sub Category');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }

            $data['categoryList'] = $this->project_category_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
            
            $this->load->view('admin/sub_category/edit',$data);
    }
    
    public function edit($id="")
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Sub Category Selected','ERROR');
                redirect(base_url().'admin/project_subcategory');
            }
            
            $isExist = $this->project_subcategory_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            $isExist = $this->project_subcategory_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Project Sub Category not found','ERROR');
                            redirect(base_url().'admin/project_subcategory');
            }
            
            $data = $this->header_footer('Editing Project Sub Category');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $data['categoryList'] = $this->project_category_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

            $this->load->view('admin/sub_category/edit',$data);
    }
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Project Sub Category Selected','ERROR');
                redirect(base_url().'admin/project_subcategory');
            }
            $isExist = $this->project_subcategory_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Sub Category Selected','ERROR');
                            redirect(base_url().'admin/project_subcategory');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/project_subcategory');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;




            $this->project_subcategory_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Project Sub Category change successfully','SUCCESS');
            redirect(base_url().'admin/project_subcategory');
    }
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Sub Category Selected','ERROR');
                redirect(base_url().'admin/project_subcategory');
            }
            
            $isExist = $this->project_subcategory_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Sub Category Selected','ERROR');
                            redirect(base_url().'admin/project_subcategory');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/project_subcategory');
            }
            $this->project_subcategory_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Sub Category is Deleted','SUCCESS');
            redirect(base_url().'admin/project_subcategory');
    }
    


    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('subcategory_title','Subcategory Title','required');
        $this->form_validation->set_rules('category_id','Category Id','required');
        $this->form_validation->set_rules('subcategory_description','Project Description','required');

    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');

                return false;
        }

        $data = array(
                    'subcategory_title'=>$this->utility->info_cleanQuery($this->input->post('subcategory_title')),
                    'category_id'=>$this->utility->info_cleanQuery($this->input->post('category_id')),
                    'subcategory_description'=>$this->utility->info_cleanQuery($this->input->post('subcategory_description')),
                    'status'=>$this->utility->info_cleanQuery($this->input->post('status'))
                    );
    


        if($id)
        {
            $this->project_subcategory_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->project_subcategory_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/project_subcategory/edit/'.base64_encode($adsID));
    }
}
?>