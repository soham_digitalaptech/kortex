<?php
require('AppController.php');

/*
This is Ads Class v2.2 for CodeIgniter

Ads Class Extended from AppController

The Ads class is responsible for managing all the ads  

Author(s): Sandipan Biswas 
*/

class Ads extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('system_models/ads_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Ads Management',array(
                'sub_heading'=>'site ads'));

        $data['rows'] = $this->ads_model->fetchRecord(array(),array('id','desc'));
        $this->load->view('admin/ads/index',$data);
    }
    
    /*
    This is a method for adding advertisement. 

    Author(s): Sandipan Biswas 

    scope: public

    Return: void    
    */

    public function add()
    {
            $data = $this->header_footer('Add New Ads');
            $data['rows'] = array();
            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }
            
            $this->load->view('admin/ads/edit',$data);
    }

    /*
    This is a method for editing advertisement.(Ads company,Ads Company Name,Ads Size,Ads Banner,Ads URL,Ads Status) 

    Author(s): Sandipan Biswas

    scope: public 

    Return: void    
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Ads Selected','ERROR');
                redirect(base_url().'admin/ads');
            }
            $isExist = $this->ads_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            if(!$isExist)
            {
                            $this->utility->setMsg('Ads not found','ERROR');
                            redirect(base_url().'admin/ads');
            }
            $data = $this->header_footer('Editing Ads');
            $data['rows'] = $isExist;
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }
            $this->load->view('admin/ads/edit',$data);
    }

    /*
    This is a method for changing the status of the Advertisement from active to inactive and vice versa. 

    Author(s): Sandipan Biswas 

    scope: public

    Parameter(s): $id,$frmSecurity

    Return: void
    */
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Ads Selected','ERROR');
                redirect(base_url().'admin/ads');
            }
            $isExist=$this->ads_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Ads Selected','ERROR');
                            redirect(base_url().'admin/ads');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/ads');
            }
            $data=array(
                    'status'=>1
            );
            if($isExist['status'])
                    $data['status'] = 0;
            $this->ads_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].' Ads is Now '.$status[$data['status']],'SUCCESS');
            redirect(base_url().'admin/ads');
    }
    
    /*
    This is a method for deleting advertisements. 

    Author(s): Sandipan Biswas

    scope: public

    Parameters: $id,$frmSecurity 

    Return: void    
    */

    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Ads Selected','ERROR');
                redirect(base_url().'admin/ads');
            }
            $isExist = $this->ads_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Ads Selected','ERROR');
                            redirect(base_url().'admin/ads');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/ads');
            }
            $this->ads_model->delete(array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].' Ads is Deleted','SUCCESS');
            redirect(base_url().'admin/ads');
    }

    /*
    This is a method for Add/Edit the advertisement. 

    Author(s): Sandipan Biswas 

    scope: private

    Parameter(s): $id

    Return: void
    */
    
    private function update($id="")
    {
        $currentTime = (int)time();
        
        if($this->utility->getSecurity() != $this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                return false;
        }

        if($this->input->post('ads_company')=='google')
        {
        
            $this->form_validation->set_rules('ads_company','Ads Company','required');
            $this->form_validation->set_rules('ads_size','Ads Size','required');
            $this->form_validation->set_rules('ads_code','Ads Code','required');
            $this->form_validation->set_rules('status','Ads Status','required');

        }
        else
        {
            $this->form_validation->set_rules('ads_company','Ads Company','required');
            $this->form_validation->set_rules('ads_size','Ads Size','required');
            $this->form_validation->set_rules('ads_url','Ads Url','required');
            $this->form_validation->set_rules('status','Ads Status','required');


            $dir = "uploads/affiliate/";
                             
            if($_FILES['ads_image']['name']!='')
            {
                $this->load->library('imagetransform');
                $file = '';
                                
                $up = $this->imagetransform->upload("ads_image",$dir,time().rand(0,100));

                if($up)
                {
                    $this->imagetransform->setQuality(100);
                    $file = $this->imagetransform->main_img;
                }
            }
        
        }

        if(!$this->form_validation->run())
        {
            $this->utility->setMsg(validation_errors(),'ERROR');
            return false;
        }

        if($this->input->post('ads_company')=='google')
        {

            $data=array(
                    'ads_company'=>$this->utility->info_cleanQuery($this->input->post('ads_company')),
                    'ads_size'=>$this->utility->info_cleanQuery($this->input->post('ads_size')),
                    'ads_code'=>$this->utility->info_cleanQuery($this->input->post('ads_code')),
                    'date_of_creation'=>$currentTime,
                    'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                    );
        }
        else
        {
            $data = array(
                    'ads_company'=>$this->utility->info_cleanQuery($this->input->post('ads_company')),
                    'ads_size'=>$this->utility->info_cleanQuery($this->input->post('ads_size')),
                    'ads_url'=>$this->utility->info_cleanQuery($this->input->post('ads_url')),
                    'ads_image'=>$file,
                    'date_of_creation'=>$currentTime,
                    'status'=>$this->utility->info_cleanQuery($this->input->post('status'))
                    );
        }

    
        if($this->input->post('ads_size') == "others")
        {
            if(!$this->input->post('custom_size'))
            {
                $this->utility->setMsg('Custom Size Required','ERROR');
                return false;
            }
            
            $data['ads_size'] = $this->utility->info_cleanQuery($this->input->post('custom_size'));
        }
    
        if($this->input->post('ads_company')=="others")
        {
            if(!$this->input->post('others_name'))
            {
                $this->utility->setMsg('Company Name is Required','ERROR');
                return false;
            }
            
            $data['ads_company'] = $this->utility->info_cleanQuery($this->input->post('others_name'));
        }
    
        if($id)
        {
            $this->ads_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->ads_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/ads/edit/'.base64_encode($adsID));
    }
}
?>