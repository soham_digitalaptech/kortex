<?php require('AppController.php');
class Walletmanager extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->checkSuperAdmin();

        $this->load->model('escrow_account_model');

        $this->load->model('system_models/user_types_model');
        $this->load->model('system_models/modules_model');
        $this->load->model('system_models/user_permissions_model');
        $this->load->helper('common_helper');
        
    }
    
    public function index() 
    {
        $data = $this->header_footer('Wallet Management',array('sub_heading'=>'all system users'));
        
        $data['rows'] = $this->escrow_account_model->fetchRecord(array('escrow_account.status'=>'1'),array('id','desc'));

        $this->load->view('admin/wallet/list',$data);
    }

    public function withdrawAmount($id="")
    {
        $id = base64_decode($id);

        if(!$id || !is_numeric($id))
        {

            $this->utility->setMsg("Invalid user id",'ERROR');

            redirect(base_url()."admin/walletmanager");
        }

        $users = $this->escrow_account_model->find(array('where'=>"user_account_details.user_id = ".$id));


        //*****************  ****************//


        $cardtype       = $users['user_account_details'][0]['cardtype'].",";

        $card_number    = $users['user_account_details'][0]['card_number'].",";

        $cvv_no         = $users['user_account_details'][0]['cvv_no'].",";

        $expire_month   = $users['user_account_details'][0]['expire_month'].",";

        $expire_year    = $users['user_account_details'][0]['expire_year'].",";

        $amount         = $users['escrow_account'][0]['amount'];

        
        //*****************  ***************//

        $sandbox = TRUE;

   
    $api_version    = '85.0';
    $api_endpoint   = $sandbox ? 'https://api-3t.sandbox.paypal.com/nvp' : 'https://api-3t.paypal.com/nvp';
    $api_username   = $sandbox ? 'support.fdx-facilitator-1_api1.gmail.com' : 'LIVE_USERNAME_GOES_HERE';
    $api_password   = $sandbox ? 'ZKPVNY73JHD8W4C5' : 'LIVE_PASSWORD_GOES_HERE';
    $api_signature  = $sandbox ? 'AFcWxV21C7fd0v3bYYYRCpSSRl31AL.BNW4Nl33nAICTQQkGsMnp-uNo' : 'LIVE_SIGNATURE_GOES_HERE';



                    $request_params = array
                    (
                    'METHOD'    =>  'DoDirectPayment', 
                    'USER'      =>  $api_username, 
                    'PWD'       =>  $api_password, 
                    'SIGNATURE' =>  $api_signature, 
                    'VERSION'   =>  $api_version, 
                    'PAYMENTACTION' => 'Sale',                  
                    'IPADDRESS' =>  $_SERVER['REMOTE_ADDR'],
                    'CREDITCARDTYPE' => $cardtype, 
                    'ACCT'      =>  $card_number,                         
                    'EXPDATE'   =>  $expire_month.$expire_year,            
                    'CVV2'      =>  $cvv_no, 
                    'AMT'       =>  $amount, 
                    'CURRENCYCODE' => 'USD', 
                    'DESC'      =>  'Testing Payments Pro' 
                    );
                    
                    
                    $nvp_string = '';
                    foreach($request_params as $var=>$val)
                    {
                        $nvp_string .= '&'.$var.'='.urlencode($val);    
                    }


              
                    $curl = curl_init();
                            curl_setopt($curl, CURLOPT_VERBOSE, 1);
                            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                            curl_setopt($curl, CURLOPT_URL, $api_endpoint);
                            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);

                    $result = curl_exec($curl);
                    $result.'<br /><br />';
                    curl_close($curl);

                  
                    $result_array = NVPToArray($result);


                if($result_array['ACK'] == 'Success')
                {

                    $insert = array(
                    'wallet_balance' => ('wallet_balance'+$amount),
                    'others' => serialize($this->packages)
                    );

                    $id = $this->escrow_account_model->addEdit($insert,array('id'=>$id));
                }

        
    }
}

?>