<?php
require('AppController.php');

/*
This is Wallets Class for CodeIgniter

Wallets Class Extended from AppController

The Wallets class is responsible for managing all the Notifications 

@author: Sandipan Biswas 
*/

class Wallets extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('wallets_model');
       
    }

    /*
    This is a method for the wallet details. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $user_id

    @return: void   
    */
    
    public function wallet_details($user_id="")
    {       
        
        $user_id=base64_decode($user_id);
        
        if(!$user_id || !is_numeric($user_id))
        {
            $this->utility->setMsg('Invalid user id','ERROR');
            redirect(base_url().'admin/user');
        }        

        $userDetails = $this->wallets_model->find(array('where' => 'user_id ='.$user_id),array('id','desc'));
        
        if(!$userDetails)
        {
            $this->utility->setMsg('User not found','ERROR');
            redirect(base_url().'admin/user');
        }
        

        $data = $this->header_footer('Wallet Details',array('sub_heading'=>'Wallet Details'));

        $data['transactions'] = $this->wallets_model->fetchTransactionDetails('transaction',$user_id);

        $data['users'] = $userDetails;
        
        $data['user_id'] = base64_encode($user_id);
 
        $this->load->view('admin/wallets/wallet_details',$data);
        
    }


    /*
    This is a method for Adding the cms pages. 

    @author: Sandipan Biswas

    @access: public

    @return: void   
    */
    
    // public function add()
    // {
    //         $data = $this->header_footer('Add New Package');
    //         $data['rows'] = array();


    //     if($this->input->post())
    //     {
    //         foreach($this->packages as $fieldName=>$value)
    //         {
    //             if(in_array('required',$value['rules']))
    //             {

    //                 if(!$this->input->post($fieldName))
    //                 {
    //                     $this->utility->setMsg($fieldName.' is required field');


    //                     redirect(base_url().'admin/packages/add');
    //                 }

                   
    //             }

    //                 if($_FILES[$fieldName]['name']!='')
    //                 {

    //                     $dir="uploads/package/";
               
                
    //                     if($_FILES[$fieldName]['name']!='')
    //                     {
    //                         $this->load->library('imagetransform');
    //                         $file = '';
                            
    //                         $up = $this->imagetransform->upload($fieldName,$dir,time().rand(0,100));

    //                         if($up)
    //                         {
    //                             $this->imagetransform->setQuality(100);
    //                             $file = $this->imagetransform->main_img;
    //                         }
                           
    //                     }


    //                     $this->fillPackageArray($fieldName,$file);
    //                 }
    //                 else
    //                 {
    //                     $this->fillPackageArray($fieldName,$this->input->post($fieldName));
    //                 }
                 
    //         }


                
    //         $insert=array(
    //             'status'=>1,
    //             'others'=>serialize($this->packages)
    //         );
    //         $id = $this->packages_model->addEdit($insert);

    //         redirect(base_url().'admin/packages');
         
    //     }
        
    //         $data['package_instruction'] = $this->packages;


    //         $this->load->view('admin/packages/edit',$data);
    // }
    

    /*
    This is a method for Editing the Wallets. 

    @author: Sandipan Biswas

    @access: public

    @parameters: $id

    @return: void   
    */

    // public function edit($id="")
    // {
    //         $id = base64_decode($id);
    //         if(!$id || !is_numeric($id))
    //         {
    //             $this->utility->setMsg('Invalid Packages Selected','ERROR');
    //             redirect(base_url().'admin/packages');
    //         }
    //         $isExist = $this->packages_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
    //         if(!$isExist)
    //         {
    //                         $this->utility->setMsg('Packages not found','ERROR');
    //                         redirect(base_url().'admin/packages');
    //         }
    //         $data = $this->header_footer('Editing Packages');

    //         $data['package_instruction'] = unserialize($isExist['others']);
            
            


    //         ////////////////////////////////////
    //         if($this->input->post())
    //         {

               
                
    //             foreach($this->packages as $fieldName=>$value)
    //             {
    //                 if(in_array('required',$value['rules']))
    //                 {

    //                     if(!$this->input->post($fieldName))
    //                     {
    //                         $this->utility->setMsg($fieldName.' is required field');
    //                         redirect(base_url().'admin/packages/add');
    //                     }
                       
    //                 }

                    

    //                 if($_FILES[$fieldName]['name']!='')
    //                 {

    //                     $dir="uploads/package/";
               
                
    //                     if($_FILES[$fieldName]['name']!='')
    //                     {
    //                         $this->load->library('imagetransform');
    //                         $file = '';
                            
    //                         $up = $this->imagetransform->upload($fieldName,$dir,time().rand(0,100));

    //                         if($up)
    //                         {
    //                             $this->imagetransform->setQuality(100);
    //                             $file = $this->imagetransform->main_img;
    //                         }
                           
    //                     }


    //                     $this->fillPackageArray($fieldName,$file);
    //                 }
    //                 else
    //                 {
    //                     $this->fillPackageArray($fieldName,$this->input->post($fieldName));
    //                 }
    //             }

                

    //             $insert=array(
    //                 'status'=>1,

    //                 'others'=>serialize($this->packages)
    //             );

    //             $id = $this->packages_model->addEdit($insert,array('id'=>$id));
                
    //             redirect(base_url().'admin/packages');
    //         }
    //         //////////////////////////////////////


    //         $data['rows']=array('id'=>$id);
    //         $this->load->view('admin/packages/edit',$data);
    // }
    
    // public function change_status($id,$frmSecurity)
    // {
    //         $id = base64_decode($id);
    //         if(!$id || !is_numeric($id))
    //         {
    //             $this->utility->setMsg('Invalid Package Selected','ERROR');
    //             redirect(base_url().'admin/packages');
    //         }
    //         $isExist = $this->packages_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
    //         if(!$isExist)
    //         {
    //                         $this->utility->setMsg('Invalid Package Selected','ERROR');
    //                         redirect(base_url().'admin/packages');
    //         }
            
    //         if($this->utility->getSecurity()!=$frmSecurity)
    //         {
    //                 $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
    //                 redirect(base_url().'admin/packages');
    //         }

    //         $data=array(
    //                 'status'=>1
    //         );

    //         if($isExist['status'])
    //                 $data['status'] = 0;

    //         $this->packages_model->addEdit($data,array('id'=>$id));
    //         $this->utility->setMsg('Package status change successfully','SUCCESS');
    //         redirect(base_url().'admin/packages');
    // }
    
    // public function delete($id,$frmSecurity)
    // {
    //         $id = base64_decode($id);
           
    //         if(!$id || !is_numeric($id))
    //         {
    //             $this->utility->setMsg('Invalid Package Selected','ERROR');
    //             redirect(base_url().'admin/packages');
    //         }
            
    //         $isExist = $this->packages_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
    //         if(!$isExist)
    //         {
    //                         $this->utility->setMsg('Invalid Package Selected','ERROR');
    //                         redirect(base_url().'admin/packages');
    //         }
    //         if($this->utility->getSecurity()!=$frmSecurity)
    //         {
    //                 $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
    //                 redirect(base_url().'admin/packages');
    //         }
    //         $this->packages_model->addEdit(array('status'=>3),array('id'=>$id));
    //         $this->utility->setMsg($isExist['ads_size'].'Package is Deleted','SUCCESS');
    //         redirect(base_url().'admin/packages');
    // }
    


    // private function fillPackageArray($field_name,$value)
    // {
    //     $this->packages[$field_name]['value']=$value;
    // }
}
?>