<?php
require('AppController.php');

/*
This is Listing_types Class for CodeIgniter

Listing_types Class Extended from AppController

The Listing_types class is responsible for managing all the Notifications 

@author: Sandipan Biswas 
*/

class Listing_types extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('listing_types_model');
    }
    
    public function index()
    {
        $data = $this->header_footer('Listing Type Management',array(
                'sub_heading'=>'Listing Type Management'
        ));

        $data['rows'] = $this->listing_types_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

        $this->load->view('admin/listing_types/index',$data);
    }


    /*
    This is a method for adding the project listing types. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */
    
    public function add()
    {
            $data = $this->header_footer('Add New Listing Type');
            
            $data['rows'] = array();

            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->listing_types_model->fetchRecord(array('status <> '=> 3),array('id','desc'));
            
            $this->load->view('admin/listing_types/edit',$data);
    }

    /*
    This is a method for editing the project listing types. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);

            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Listing Type Selected','ERROR');
                redirect(base_url().'admin/listing_types');
            }
            
            $isExist = $this->listing_types_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            $isExist = $this->listing_types_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Listing Types not found','ERROR');
                            redirect(base_url().'admin/listing_types');
            }
            
            $data = $this->header_footer('Editing Listing Types');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $data['projectList'] = $this->listing_types_model->fetchRecord(array('status <> '=> 3),array('id','desc'));

            $this->load->view('admin/listing_types/edit',$data);
    }

    /*
    This is a method for changing the status of the the project listing types from active to inactive and vice versa. 

    @author(s): Sandipan Biswas 

    @access: public

    @return: void
    */
    
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Listing Type Selected','ERROR');
                redirect(base_url().'admin/listing_types');
            }
            $isExist = $this->listing_types_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Listing Type Selected','ERROR');
                            redirect(base_url().'admin/listing_types');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/listing_types');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->listing_types_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Listing Type status changed successfully','SUCCESS');
            redirect(base_url().'admin/listing_types');
    }


    /*
    This is a method for deleting the project listing types from database. 

    @author(s): Sandipan Biswas 

    @access: public

    @parameters: $id,$frmSecurity

    @return: void
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Listing Type Selected','ERROR');
                redirect(base_url().'admin/listing_types');
            }
            
            $isExist = $this->listing_types_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Listing Type Selected','ERROR');
                            redirect(base_url().'admin/listing_types');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                    redirect(base_url().'admin/listing_types');
            }
            $this->listing_types_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Listing Type is Deleted','SUCCESS');
            redirect(base_url().'admin/listing_types');
    }
    

    /*
    This is a method for updating the project listing types of database. 

    @author(s): Sandipan Biswas 

    @parameters: $id

    @access: private

    @return: void
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!= $this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired.please try again','ERROR');
                return false;
        }


        $this->form_validation->set_rules('title','Listing Type','required');
    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
        }
    
        if($id)
        {
            $data = array(
                'title' => $this->utility->info_cleanQuery($this->input->post('title')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_creation' => (int)time()
                );

            $this->listing_types_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
        {
            $data = array(
                'title' => $this->utility->info_cleanQuery($this->input->post('title')),
                'status'=>$this->utility->info_cleanQuery($this->input->post('status')),
                'date_of_modification' => (int)time()
                );

            $adsID = $this->listing_types_model->addEdit($data);
        }
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/listing_types/edit/'.base64_encode($adsID));

    }
}
?>