<?php
require('AppController.php');
class Menu extends AppController
{
        private $order;
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
                $this->load->model('menu_model');
		$this->checkLogin();
                $this->order=1;
	}
	public function index()
	{
		$data=$this->header_footer('Menu Manger');
                $data['menu']=$this->menu_model->getMenu();
		$this->load->view('admin/menu/index',$data);
	}
        
        public function update()
        {
            if($this->input->post('frmSecurity')!=$this->utility->getSecurity())
            {
                $this->output('Invalid security token','ERROR');
            }
            $this->form_validation->set_rules('id','ID','required|numeric');
            $this->form_validation->set_rules('location','Main Menu or Footer Menu','required|numeric');
            $this->form_validation->set_rules('caption','Caption','required');
            $this->form_validation->set_rules('url','URL','required|prep_url|valid_url');
            if(!$this->form_validation->run())
            {
                $this->output(validation_errors(),'ERROR');
            }
            $id=$this->utility->info_cleanQuery($this->input->post('id'));
            $data=array(
                'caption'=>$this->utility->info_cleanQuery($this->input->post('caption')),
                'url'=>$this->utility->info_cleanQuery($this->input->post('url')),
                'location'=>$this->utility->info_cleanQuery($this->input->post('location')),
            );
            if($id==-99){
                $data['orderx']=$this->menu_model->next_order();
                $id=$this->menu_model->addEdit($data);
            }
            else
                $this->menu_model->addEdit($data,array('id'=>$id));
            $this->output('Saved','SUCCESS',array('id'=>$id));
        }
        public function delete()
        {
            if($this->input->post('frmSecurity')!=$this->utility->getSecurity())
            {
                $this->output('Invalid security token','ERROR');
            }
            $this->form_validation->set_rules('id','ID','required|numeric');
            if(!$this->form_validation->run())
            {
                $this->output(validation_errors(),'ERROR');
            }
            $id=$this->utility->info_cleanQuery($this->input->post('id'));
            $isExist=$this->menu_model->fetchRow(array('id'=>$id));
            $this->menu_model->delete(array('id'=>$id));
            $this->menu_model->delete(array('parent_id'=>$id));
            $this->output($isExist['caption'].' and sub menus have been deleted','SUCCESS');
        }
        public function update_order()
        {
            if($this->input->post('frmSecurity')!=$this->utility->getSecurity())
            {
                $this->utility->setMsg('Invalid security token','ERROR');
                redirect(base_url().'admin/menu');
            }
            $this->form_validation->set_rules('location','Main Menu or Footer Menu','required|numeric');
            if(!$this->form_validation->run())
            {
                $this->utility->setMsg(validation_errors(),'ERROR');
                redirect(base_url().'admin/menu');
            }
            $orderSet=json_decode($this->input->post('order_set'),true);
            if($this->register_order($orderSet,$this->utility->info_cleanQuery($this->input->post('location'))))
                $this->utility->setMsg('Saved','SUCCESS');
            else
                $this->utility->setMsg('Error to set order','Error');
            redirect(base_url().'admin/menu');
        }
        private function register_order($order_set=array(),$location,$parent=0)
        {
            foreach($order_set as $value)
            {
                if($value['id']!=-99)
                {
                    $data=array(
                        'parent_id'=>$parent,
                        'orderx'=>$this->order++
                    );
                    $this->menu_model->addEdit($data,array('id'=>$value['id']));
                }
                else
                {
                    $data=array(
                        'parent_id'=>$parent,
                        'caption'=>'New Menu',
                        'location'=>$location,
                        'orderx'=>$this->order++
                    );
                    $this->menu_model->addEdit($data);
                }
                if(!empty($value['children']))
                    $this->register_order($value['children'], $location,$value['id']);
            }
            return true;
        }
}
?>