<?php
require('AppController.php');
class Login extends AppController
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
	}
	public function index()
	{
		if($this->checkLogin(false))
			redirect(base_url().'admin/home');
		$data=$this->header_footer('Admin Login');
		$this->load->view('admin/login',$data);
	}
	public function doLogin()
	{
		if($this->input->post())
		{
			if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
			{
				$this->utility->setMsg('Session expired.. Please try again..','ERROR');
				redirect(base_url().'admin/login');
			}
			$this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('password','Password','required');
			if($this->form_validation->run())
			{
				$isExist=$this->users_model->fetchRow(array(
													'username'=>$this->utility->info_cleanQuery($this->input->post('username')),
													'password'=>sha1($this->utility->info_cleanQuery($this->input->post('password')))
												));
				if($isExist)
				{
					if(!$isExist['last_login'])
						$isExist['last_login']=$this->currentTime;
					$this->register_veriables($isExist);
					$this->register_current_login($isExist['username']);
					$this->utility->setMsg('Successfully logged in.','SUCCESS');
						redirect(base_url().'admin/home');
				}
				else
				{
					$isExist=$this->users_model->fetchRow(array(
													'username'=>$this->utility->info_cleanQuery($this->input->post('username')),
													'new_password'=>sha1($this->utility->info_cleanQuery($this->input->post('password')))
												));
					if($isExist)
					{
						if(!$isExist['last_login'])
							$isExist['last_login']=$this->currentTime;
						$this->register_veriables($isExist);
						$this->register_current_login($isExist['username']);
						
						$data=array(
							'new_password'=>'',
							'password'=>sha1($this->utility->info_cleanQuery($this->input->post('password')))
						);
						$this->users_model->addEdit($data,array('username'=>$isExist['username']));
						$this->utility->setMsg('Successfully logged in.','SUCCESS');
						redirect(base_url().'admin/home');
					}
					else{
						$this->utility->setMsg('Invalid username and password','ERROR');
						redirect(base_url().'admin/login');
					}
				}
			}
			else{
				$this->utility->setMsg(validation_errors(),'ERROR');
				redirect(base_url().'admin/login');
			}
		}
		else
		{
			redirect(base_url().'admin/login');
		}
	}
	public function logout()
	{
		$data=array(
			'admin_id'=>'',
			'username'=>'',
			'last_login'=>''
		);
		$this->unregister_veriables($data);
                $this->utility->setMsg('Successfully Logged out','SUCCESS');
		redirect(base_url().'admin/login');
	}
	private function unregister_veriables($data)
	{
		foreach($data as $key=>$value)
		{
			$this->session->unset_userdata($key);
		}
	}
	private function register_veriables($data)
	{
		foreach($data as $key=>$value)
		{
			$session_key=$key;
			if($key=='id')
				$session_key="admin_id";
			if($key=="password")
				continue;
			$this->session->set_userdata($session_key,$value);
		}
	}
	private function register_current_login($username)
	{
		$data=array(
			'last_login'=>$this->currentTime
		);
		$this->users_model->addEdit($data,array('username'=>$username));
	}
}
?>