<?php
require('AppController.php');
/*
widget_manager Class Extended from AppController

The widget_manager class is responsible for managing all the sliders 

@author: Sandipan Biswas 
*/

class Widget_manager extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('widget_manager_model');

    }
    
    public function index()
    {
        $data = $this->header_footer('Widget Management',array(
                'sub_heading'=>'Widget Management'
        ));

        $data['rows'] = $this->widget_manager_model->fetchRecord(array('status <> '=> 3),array('id','desc'));


        $this->load->view('admin/widget_manager/index',$data);
    }
    
    /*
    This is a method for adding widget manager. 

    @author: Sandipan Biswas

    @access: public 

    @return: void    
    */

    public function add()
    {
            $data = $this->header_footer('Add New Widget Manager');
            $data['rows'] = array();
            
            if($this->input->post())
            {
                    if(!$this->update())
                            $data['rows'] = $this->input->post();
            }

            $this->load->view('admin/widget_manager/edit',$data);
    }

    /*
    This is a method for editing the widget manager. 

    @author: Sandipan Biswas 

    @parameter(s): $id

    @return: void
    */
    
    public function edit($id="")
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Widget Selected','ERROR');
                redirect(base_url().'admin/widget_manager');
            }
            
            $isExist = $this->widget_manager_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));

            if(!$isExist)
            {
                            $this->utility->setMsg('Widget not found','ERROR');
                            redirect(base_url().'admin/widget_manager');
            }
            
            $data = $this->header_footer('Editing Widget Manager');
            $data['rows'] = $isExist;
            
            if($this->input->post())
            {
                    if(!$this->update($id))
                            $data['rows'] = $this->input->post();
            }

            $this->load->view('admin/widget_manager/edit',$data);
    }


    /*
    This is a method for changing the status of the widget manager from active to inactive and vice versa. 

    @author: Sandipan Biswas 

    @parameter(s): $id,$frmSecurity

    @return: void
    */
    
    public function change_status($id,$frmSecurity)
    {
            $id = base64_decode($id);
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid widget Selected','ERROR');
                redirect(base_url().'admin/widget_manager');
            }
            $isExist = $this->widget_manager_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Widget Selected','ERROR');
                            redirect(base_url().'admin/widget_manager');
            }
            
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/widget_manager');
            }

            $data=array(
                    'status'=>1
            );

            if($isExist['status'])
                    $data['status'] = 0;

            $this->widget_manager_model->addEdit($data,array('id'=>$id));
            $this->utility->setMsg('Widget status change successfully','SUCCESS');
            redirect(base_url().'admin/widget_manager');
    }

    /*
    This is a method for deleting the widget manager. 

    @author: Sandipan Biswas 

    @parameter(s): $id,$frmSecurity

    @return: void
    */
    
    public function delete($id,$frmSecurity)
    {
            $id = base64_decode($id);
           
            if(!$id || !is_numeric($id))
            {
                $this->utility->setMsg('Invalid Widget Selected','ERROR');
                redirect(base_url().'admin/widget_manager');
            }
            
            $isExist = $this->widget_manager_model->fetchRow(array('id'=>$this->utility->info_cleanQuery($id)));
            
            if(!$isExist)
            {
                            $this->utility->setMsg('Invalid Widget Selected','ERROR');
                            redirect(base_url().'admin/widget_manager');
            }
            if($this->utility->getSecurity()!=$frmSecurity)
            {
                    $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                    redirect(base_url().'admin/widget_manager');
            }
            $this->widget_manager_model->addEdit(array('status'=>3),array('id'=>$id));
            $this->utility->setMsg($isExist['ads_size'].'Widget is Deleted','SUCCESS');
            redirect(base_url().'admin/widget_manager');
    }
    
    /*
    This is a method for adding/editing the widget name,widget type,widget status,comment etc. 

    @author: Sandipan Biswas 

    @parameter: $id

    @access: private

    @return: void
    */

    private function update($id="")
    {
        $currentTime = (int)time();

        if($this->utility->getSecurity()!=$this->input->post('frmSecurity'))
        {
                $this->utility->setMsg('Your submission session has been expired..please try again','ERROR');
                return false;
        }

        //$this->form_validation->set_rules('slug_name','Slug Name','required');
        $this->form_validation->set_rules('widget_name','Widget Name','required');
        $this->form_validation->set_rules('widget_type','Widget Type','required');
        //$this->form_validation->set_rules('comment','Comment','required');
        $this->form_validation->set_rules('status','Widget Status','required');

        $slug_name = str_replace("'", '', $this->input->post('widget_name'));

        $slug_name = strtolower(str_replace(" ",'_',$slug_name));

        if($this->input->post('widget_type')=='T')
        {
            $this->form_validation->set_rules('widget_text','Widget Text','required');
        }
        else if($this->input->post('widget_type')=='H')
        {
            $this->form_validation->set_rules('widget_html','Widget HTML','required');
        }
        else if($this->input->post('widget_type')=='L')
        {
            $this->form_validation->set_rules('widget_link','Widget Link','required');
        }
        else if($this->input->post('widget_type')=='S')
        {
            $this->form_validation->set_rules('widget_script','Widget Script','required');
        }
    
        if(!$this->form_validation->run())
        {
                $this->utility->setMsg(validation_errors(),'ERROR');
                return false;
        }

        if($this->input->post('widget_type')=='T')
        {
            $data = array('slug_name'=>$this->utility->info_cleanQuery($slug_name),
            'widget_name'=>$this->utility->info_cleanQuery($this->input->post('widget_name')),
            'widget_type'=>$this->utility->info_cleanQuery($this->input->post('widget_type')),
            'widget_text'=>$this->utility->info_cleanQuery($this->input->post('widget_text')),
            'widget_html'=>'',
            'widget_link'=>'',
            'widget_script'=>'',
            'comment'=>$this->utility->info_cleanQuery($this->input->post('comment')),
            'status'=>$this->utility->info_cleanQuery($this->input->post('status')));
        }
        else if($this->input->post('widget_type')=='H')
        {
            $data = array('slug_name'=>$this->utility->info_cleanQuery($slug_name),
            'widget_name'=>$this->utility->info_cleanQuery($this->input->post('widget_name')),
            'widget_type'=>$this->utility->info_cleanQuery($this->input->post('widget_type')),
            'widget_html'=>$this->utility->info_cleanQuery($this->input->post('widget_html')),
            'widget_text'=>'',
            'widget_link'=>'',
            'widget_script'=>'',
            'comment'=>$this->utility->info_cleanQuery($this->input->post('comment')), 
            'status'=>$this->utility->info_cleanQuery($this->input->post('status')));
        }
        else if($this->input->post('widget_type')=='L')
        {
            $data = array('slug_name'=>$this->utility->info_cleanQuery($slug_name),
            'widget_name'=>$this->utility->info_cleanQuery($this->input->post('widget_name')),
            'widget_type'=>$this->utility->info_cleanQuery($this->input->post('widget_type')),
            'widget_link'=>$this->utility->info_cleanQuery($this->input->post('widget_link')),
            'widget_html'=>'',
            'widget_text'=>'',
            'widget_script'=>'',
            'comment'=>$this->utility->info_cleanQuery($this->input->post('comment')), 
            'status'=>$this->utility->info_cleanQuery($this->input->post('status')));
        }
        else
        {
            $data = array('slug_name'=>$this->utility->info_cleanQuery($slug_name),
            'widget_name'=>$this->utility->info_cleanQuery($this->input->post('widget_name')),
            'widget_type'=>$this->utility->info_cleanQuery($this->input->post('widget_type')),
            'widget_script'=>$this->utility->info_cleanQuery($this->input->post('widget_script')),
            'widget_html'=>'',
            'widget_link'=>'',
            'widget_text'=>'',
            'comment'=>$this->utility->info_cleanQuery($this->input->post('comment')), 
            'status'=>$this->utility->info_cleanQuery($this->input->post('status')));
        }

        
    
        if($id)
        {
            $this->widget_manager_model->addEdit($data,array('id'=>$id));
            $adsID = $id;
        }
        else
            $adsID = $this->widget_manager_model->addEdit($data);
        
        $this->utility->setMsg('Saved','SUCCESS');
        redirect(base_url().'admin/widget_manager/edit/'.base64_encode($adsID));
    }


    /*
    This is a method for checking the duplicate entry of the slug name. 

    @author: Sandipan Biswas 

    @access: public

    @return: void
    */

    public function check_slug()
    {
            $slug_name = $this->input->post('slug_name');

            $isExist = $this->widget_manager_model->fetchRow(array('slug_name'=>$this->utility->info_cleanQuery($slug_name)));
            
            if($isExist)
            {
                echo "Slug Name is not available.Please try another Name";
            }
    }
}
?>