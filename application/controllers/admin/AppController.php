<?php
class AppController extends CI_Controller
{
	protected $currentTime;
	public function __construct()
	{
            parent:: __construct();
            $this->load->model('site_settings_model');
            $this->load->model('system_models/user_permissions_model');
            $this->load->model('system_models/modules_model');
            $this->initiate();
	}
	
	protected function header_footer($title="",$opta=array())
	{
		if(empty($title))
			$title=SITE_NAME;
		
		$include=array();
		$include['sub_heading']="";
	
		if(!empty($opta['sub_heading']))
			$include['sub_heading']=$opta['sub_heading'];
    
                $include['title']=$title;
		$include['dashboard_controller']=$this->load->view('admin/includes/dashboard_controller','',true);
		$include['navbar']=$this->load->view('admin/includes/navbar','',true);
                $include['header']=$this->load->view('admin/includes/header',$include,true);
                $include['footer']=$this->load->view('admin/includes/footer','',true);
                return $include;
	}
	
	protected function checkLogin($redirect=true)
	{
		if($this->session->userdata('admin_id'))
			return true;
		else{
			if($redirect)
				redirect(base_url().'admin/login');
			return false;
		}
	}
        protected function checkSuperAdmin($redirect=true)
        {
                if($this->session->userdata('user_type_id')==1)
                    return true;
                else{
                    $this->utility->setMsg('Sorry!! You don\'t have a permission to access this section','ERROR');
                    if($redirect)
                            redirect(base_url().'admin');
                    return false;
                }
        }
        private function getPermissionList()
        {
            $modules=array();
            $temp=$this->modules_model->fetchRecord();
            foreach($temp as $value)
            {
                $modules[$value['slug']]=array(
                    'read'=>false,
                    'write'=>false
                );
            }
            if(!$this->session->userdata('admin_id'))
                return $modules;
            if($this->session->userdata('user_type_id')==1)
            {
                foreach($temp as $value)
                {
                    $modules[$value['slug']]=array(
                        'read'=>true,
                        'write'=>true
                    );
                }
            }
            else
            {
                $temp=$this->user_permissions_model->find(array('where'=>'user_permissions.user_id = '.$this->session->userdata('admin_id')));
                if(!empty($temp['user_permissions']))
                {
                    foreach($temp['user_permissions'] as $value)
                    {
                        if($value['read_access'])
                           $modules[$value['modules'][0]['slug']]['read']=true; 
                        if($value['write_access'])
                           $modules[$value['modules'][0]['slug']]['write']=true;
                    }
                }
            }
            return $modules;
        }

	private function initiate()
	{
		$this->currentTime=(int)time();
		$siteSettings=$this->site_settings_model->fetchRecord(array(),array('setting_name','desc'));
		foreach($siteSettings as $key=>$value)
		{
			if(!empty($value['setting_value']))
				define(strtoupper(str_replace(' ','_',$value['setting_name'])),$value['setting_value']);
			else
				define(strtoupper(str_replace(' ','_',$value['setting_name'])),$value['setting_name']);
		}
                define('PERMISSIONS',  json_encode($this->getPermissionList()));
                $this->logArray=array(
                    'admin_id'=>$this->session->userdata('admin_id'),
                    'slug'=>'',
                    'description'=>'',
                    'date'=>(int)time()
                );
	}
        protected function setOrder($currentProduct=array(),$order=0,$allSameTypeProducts=array())
        {
                $resultArray=$allSameTypeProducts;
                foreach($allSameTypeProducts as $key=>$value)
                {
                        $resultArray[$key]=$value;
                        /******************Order highest to lowest******************/
                        if($currentProduct['order'] > $order)
                        {
                                if($value['order'] >= $order && $currentProduct['order']>=$value['order'])
                                {
                                        $resultArray[$key]['order']=$value['order']+1;
                                }
                        }

                        /******************Order lowest to highest******************/
                        if($currentProduct['order'] < $order)
                        {
                                if($value['order'] <= $order && $currentProduct['order']<=$value['order'])
                                {
                                        $resultArray[$key]['order']=$value['order']-1;
                                }
                        }

                        if($value['id']==$currentProduct['id'])
                        {
                                $resultArray[$key]=$value;
                                $resultArray[$key]['order']=$order;
                        }
                }
                return $resultArray;
        }
        /*
        public function initialize()
        {
            $return=array(
                           'result' =>'',
                           'status'=>'initiate',
                           'cause'=>'Nothing is happening..'
            );
            return $return;
        }
    
        protected function output($cause="",$status="error",$data=array())
        {
               $return=$this->initialize();
               $return['result']=$data;
               $return['cause']=$cause;
               $return['status']='error';
               if(strtolower($status)=="success")
                   $return['status']='success';
               if(!$this->input->is_ajax_request())
                    return json_encode($return);
               echo json_encode($return);die;
        }
         */

}
?>