<?php
require('AppController.php');
class Home extends AppController
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('sliders_model');
    }
    public function index()
    {
        $data=$this->header_footer('Home Page');
        $this->load->view('index',$data);
    }
    public function about()
    {
            $data=$this->header_footer('About Us');
            $this->load->view('about',$data);
    }

    public function products()
    {
            $data=$this->header_footer('Products');
            $data['page_data']='Under Construction';
            $this->load->view('service',$data);
    }

    public function contact()
    {
            $data=$this->header_footer('Contact Us');
            if($this->utility->getSecurity()==$this->input->post('frmSecurity'))
            {
                    if($this->input->post('captcha')==$_SESSION['security_code'])
                    {
                            $name=$this->input->post('name');
                            $to=$this->input->post('email');
                            $from="no-reply@oxitelbroadband.com";
                            $subject=$this->input->post('subject');
                            $message='
                                                    <style>
                                                            table,tbody,tfoot,thead,tr,th,td {
                                                                    background: none;
                                                                    margin: 0;
                                                                    padding: 0;
                                                                    border: 0;
                                                                    font-size: 16px;
                                                                    font: inherit;
                                                                    vertical-align: middle;
                                                            }
                                                            table {
                                                                    border-collapse: collapse;
                                                                    border-spacing: 0;
                                                                    color: #333;
                                                                    font-family: Helvetica, Arial, sans-serif;
                                                                    width: 100%;
                                                                    border-collapse: collapse;
                                                                    border-spacing: 0;
                                                            }
                                                            td,th {
                                                                    border: 1px solid transparent;
                                                                    height: 40px;
                                                                    transition: all 0.3s;
                                                            }
                                                            th {
                                                                    background: #DFDFDF;
                                                                    font-weight: bold;
                                                            }
                                                            td {
                                                                    background: #FAFAFA;
                                                                    text-align: center;
                                                            }
                                                            tr: nth-child(even) td {
                                                                    background: #F1F1F1;
                                                            }
                                                            tr: nth-child(odd) td {
                                                                    background: #FEFEFE;
                                                            }
                                                            tr td: hover {
                                                                    background: #666;
                                                                    color: #FFF;
                                                            }
                                                            tr: nth-child(even) td {
                                                                                    background: #F1F1F1;
                                                                            }
                                                            tr: nth-child(odd) td {
                                                                                    background: #FEFEFE;
                                                                            }
                                                    </style>

                                                    <table>
                                                            <tbody>
                                                                    <tr>
                                                                            <td>Name: </td>
                                                                            <td>'.$name.'</td>
                                                                    </tr>
                                                                    <tr>
                                                                            <td>Email: </td>
                                                                            <td>'.$to.'</td>
                                                                    </tr>
                                                                    <tr>
                                                                            <td>Subject: </td>
                                                                            <td>'.$subject.'</td>
                                                                    </tr>
                                                                    <tr>
                                                                            <td>Address: </td>
                                                                            <td>'.$this->input->post('address').'</td>
                                                                    </tr>
                                                                    <tr>
                                                                            <td>Mobile: </td>
                                                                            <td>+91 '.$this->input->post('mobile').'</td>
                                                                    </tr>
                                                                    <tr>
                                                                            <td>Purpose: </td>
                                                                            <td>+91 '.$this->input->post('purpose').'</td>
                                                                    </tr>
                                                                    <tr>
                                                                            <td>Message: </td>
                                                                            <td>'.$this->input->post('message').'</td>
                                                                    </tr>
                                                            </tbody>
                                                    </table>';
                                            $this->utility->sendMail(COMPANY_EMAIL,'OXITEL: '.$subject,$message,$from);

                                            $this->utility->sendMail($to,"Thank You for Contact to OXITEL",'<strong>We have got your enquiry. We will get back soon.</strong><br/><br/>'.$message,$from);			

                                            $this->utility->setMsg('Your message has been sent to us.','SUCCESS');
                    }
                    else
                    {
                            $this->utility->setMsg('Invalid Captcha Code entered','ERROR');
                            $data['posted_data']=$this->input->post();
                    }
            }

            $this->load->view('contact',$data);
    }

    public function packages()
    {
            $this->load->model('cities_model');
            $this->load->model('packages_model');
            $data=$this->header_footer('Packages');
            $data['states']=$this->cities_model->find(array(
                                                                                                            "where"=>"city_state IN ('Assam','Bihar','Gujrat','Manipur','Orissa','Tripura','Uttar Pradesh','West Bengal') ORDER BY city_state"
                                                                                            ));
            $data['posted_data']=array();
            if($this->utility->getSecurity()==$this->input->post('frmSecurity'))
            {
                    $data['posted_data']=$this->input->post();
            }

            $stateCity=array();
            foreach($data['states']['cities'] as $value)
            {
                    if(empty($stateCity[$value['city_state']]))
                            $stateCity[$value['city_state']]=array();
                    array_push($stateCity[$value['city_state']],array('id'=>$value['id'],'city_name'=>$value['city_name']));
            }
            $data['states']=$stateCity;
            $data['plans']=$this->packages_model->fetchRecord();
            $this->load->view('package',$data);
    }
}
?>