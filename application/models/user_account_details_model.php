<?php
class User_account_details_model extends Core 
{
    public function __construct()
    {
            parent::__construct('user_account_details');
    }
    public function find($opta=array())
    {
        if(empty($opta['join'])){
                $opta['join']=array(
                        array(
                                'table'=>'users',
                                'on'=>'user_account_details.user_id=users.id'
                        )
                );
        }
        return $this->buildResult(parent::find($opta));
    }
    public function buildResult($result=array())
    {
            if(!empty($result['user_account_details']))
            {
                foreach($result['user_account_details'] as $key=>$value)
                {
                    if(!empty($result['users']))
                    {
                        foreach($result['users'] as $keyx=>$valuex)
                        {
                            if($value['user_id']==$valuex['id'])
                            {
                                    if(empty($result['user_account_details'][$key]['users']))
                                            $result['user_account_details'][$key]['users']=array();
                                    array_push($result['user_account_details'][$key]['users'],$valuex);
                            }
                        }
                    }
                }
            }
            return $result;
    }
}