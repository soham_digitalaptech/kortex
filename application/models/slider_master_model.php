<?php
class Slider_master_model extends Core {

    public function __construct()
    {
        parent::__construct('slider_master');
    }

    public function find($opta=array())
    {
        if(empty($opta['join']))
        {
                $opta['join']=array(
                        array(
                                'table'=>'sliders',
                                'on'=>'sliders.slider_master_id = slider_master.id'
                        )
                );
        }

        return $this->buildResult(parent::find($opta));
    }

    public function buildResult($result=array())
    {
        if(!empty($result['slider_master']))
        {
            foreach($result['slider_master'] as $key=>$value)
            {
                    if(!empty($result['sliders']))
                    {
                        foreach($result['sliders'] as $keyx=>$valuex)
                        {
                            if($value['id']==$valuex['slider_master_id'])
                            {
                                    if(empty($result['slider_master'][$key]['sliders']))
                                            $result['slider_master'][$key]['sliders']=array();
                                    
                                    array_push($result['slider_master'][$key]['sliders'],$valuex);
                            }
                        }
                    }
            }
        }
            
        return $result;
    }

    public function insert_images($data,$id)
	{
			if($data)
			{

				foreach ($data as $key => $value) 
				{

					//print_r($value['image']);
					$data = array('slider_master_id'=>$id,'image'=>$value['image']);
				
            		$this->db->insert('sliders', $data); 
            		//$insert_id = $this->db->insert_id();

				}

				
				
        	}	 
	}

    public function getImages($id)
    {
        $this->db->select('*');
        $this->db->from('sliders');
        $this->db->where('slider_master_id',$id);
        $query = $this->db->get();

        if($query->num_rows()>0)
        {
            $user_details = $query->result();
            return $user_details;
        }
        else
            return 0;
    }
}