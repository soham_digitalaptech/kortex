<?php
class Packages_model extends Core {
    
    public function __construct()
	{
		parent::__construct('packages');
	}

	public function fetchPackageCategory($table = '',$where = array())
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function fetchPackages($table = '',$where = array())
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();

		return $query->result();
	}
}