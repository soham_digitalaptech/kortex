<?php
class Users_model extends Core {
    public function __construct()
    {
            parent::__construct('users');
    }
    public function find($opta=array())
    {
        if(empty($opta['join'])){
                $opta['join']=array(
                        array(
                                'table'=>'user_types',
                                'on'=>'user_types.id=users.user_type_id'
                        )
                );
        }
        return $this->buildResult(parent::find($opta));
    }
    public function buildResult($result=array())
    {
            if(!empty($result['users']))
            {
                foreach($result['users'] as $key=>$value)
                {
                    if(!empty($result['user_types']))
                    {
                        foreach($result['user_types'] as $keyx=>$valuex)
                        {
                            if($value['user_type_id']==$valuex['id'])
                            {
                                    if(empty($result['users'][$key]['user_types']))
                                            $result['users'][$key]['user_types']=array();
                                    array_push($result['users'][$key]['user_types'],$valuex);
                            }
                        }
                    }
                }
            }
            return $result;
    }
}