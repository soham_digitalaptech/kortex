<?php
class Wallets_model extends Core {
    
    public function __construct()
    {
            parent::__construct('wallets');
    }
    
    public function find($opta=array())
    {
        if(empty($opta['join'])){
                $opta['join']=array(
                        array(
                                'table'=>'users',
                                'on'=>'users.id = wallets.user_id'
                        )
                );
        }
        return $this->buildResult(parent::find($opta));
    }

    public function buildResult($result=array())
    {
            if(!empty($result['wallets']))
            {
                foreach($result['wallets'] as $key=>$value)
                {
                    if(!empty($result['users']))
                    {
                        foreach($result['users'] as $keyx=>$valuex)
                        {
                            if($value['user_id']==$valuex['id'])
                            {
                                    if(empty($result['wallets'][$key]['users']))
                                            $result['wallets'][$key]['users']=array();
                                    array_push($result['wallets'][$key]['users'],$valuex);
                            }
                        }
                    }
                }
            }
            return $result;
    }

    public function fetchTransactionDetails($table,$user_id)
    {
			 	$this->db->select('*');
			    $this->db->from($table);
			    
				$this->db->where("(sender_wallet_id='".$user_id."' OR receiver_wallet_id='".$user_id."')", NULL, FALSE);
			    $query = $this->db->get();

			    if($query->num_rows()>0)
			    {
			      return $query->result();
			    }
			    else
			      return FALSE;   	
    }
}