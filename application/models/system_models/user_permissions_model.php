<?php
class User_permissions_model extends Core {
    public function __construct()
    {
        parent::__construct('user_permissions');
    }
    public function find($opta=array())
    {
            if(empty($opta['join'])){
                    $opta['join']=array(
                            array(
                                    'table'=>'modules',
                                    'on'=>'user_permissions.module_id=modules.id'
                            )
                    );
            }
            return $this->buildResult(parent::find($opta));
    }
    public function buildResult($result=array())
    {
        if(!empty($result['user_permissions']))
        {
                foreach($result['user_permissions'] as $key=>$value)
                {
                        if(!empty($result['modules']))
                        {
                                foreach($result['modules'] as $keyx=>$valuex)
                                {
                                        if($value['module_id']==$valuex['id'])
                                        {
                                                if(empty($result['user_permissions'][$key]['modules']))
                                                        $result['user_permissions'][$key]['modules']=array();
                                                array_push($result['user_permissions'][$key]['modules'],$valuex);
                                        }
                                }
                        }
                }
        }
        return $result;
    }
}