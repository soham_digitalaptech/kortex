<?php
class Menu_model extends Core {
    public function __construct()
    {
            parent::__construct('menu');
    }
    public function getMenu()
    {
        $mainMenu=$this->buildMenu($this->fetchRecord(array('parent_id'=>0),array('orderx','asc')));
        return $mainMenu;
    }
    private function buildMenu($menu)
    {
        foreach($menu as $key=>$value)
        {
            $menu[$key]['sub_menu']=array();
            $sub_menu=$this->fetchRecord(array('parent_id'=>$value['id']),array('orderx','asc'));
            if($sub_menu)
            {
                $menu[$key]['sub_menu']=$sub_menu;
                $menu[$key]['sub_menu']=$this->buildMenu($menu[$key]['sub_menu']);
            }
        }
        return $menu;
    }

    public function next_order()
    {
        $this->db->select_max('orderx');
        $this->db->from('menu');
        $query = $this->db->get();
        $count=$query->row_array();
        if(!$count['orderx'])
            return 1;
        else
            return $count['orderx']+1;
    }
}