<?php
class Affiliate_manager_model extends Core {
    public function __construct()
	{
		parent::__construct('affiliate_manager');
	}

	public function fetchColoumns($table='')
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}
}