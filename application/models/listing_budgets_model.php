<?php
class Listing_budgets_model extends Core {
    
    public function __construct()
    {
        parent::__construct('listing_budgets');
    }
	public function find($opta=array())
    {
        if(empty($opta['join'])){
                $opta['join']=array(
                        array(
                                'table'=>'listing_budget_types',
                                'on'=>'listing_budget_types.listing_budget_id=listing_budgets.id',
								'join_type'=>'LEFT'
                        ),
						array(
                                'table'=>'listing_types',
                                'on'=>'listing_budget_types.listing_type_id=listing_types.id',
								'join_type'=>'LEFT'
                        ),
                );
        }
        return $this->buildResult(parent::find($opta));
    }
	public function buildResult($result=array())
    {
		if(!empty($result['listing_budgets']))
		{
			foreach($result['listing_budgets'] as $key=>$value)
			{
				if(empty($result['listing_budgets'][$key]['listing_types']))
					$result['listing_budgets'][$key]['listing_types']=array();
				if(!empty($result['listing_budget_types']))
				{
					foreach($result['listing_budget_types'] as $keyx=>$valuex)
					{
						if($value['id']==$valuex['listing_budget_id'])
						{
							if(!empty($result['listing_types']))
							{
								foreach($result['listing_types'] as $valuexx)
								{
									if($valuexx['id']==$valuex['listing_type_id'])
									{
										array_push($result['listing_budgets'][$key]['listing_types'],$valuexx);
									}
								}
							}
						}
					}
				}
			}
		}
		return $result;
    }
    
}