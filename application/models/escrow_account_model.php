<?php
class Escrow_account_model extends Core {
    
    public function __construct()
    {
        parent::__construct('escrow_account');
    }
    
    public function find($opta=array())
    {
        if(empty($opta['join'])){
                $opta['join']=array(
                        array(
                                'table'=>'user_account_details',
                                'on'=>'user_account_details.user_id=escrow_account.receiver_id'
                        )
                );
        }
        return $this->buildResult(parent::find($opta));
    }
    
    public function buildResult($result=array())
    {
            if(!empty($result['escrow_account']))
            {
                foreach($result['escrow_account'] as $key=>$value)
                {
                    if(!empty($result['user_account_details']))
                    {
                        foreach($result['user_account_details'] as $keyx=>$valuex)
                        {
                            if($value['receiver_id']==$valuex['user_id'])
                            {
                                    if(empty($result['escrow_account'][$key]['user_account_details']))
                                        $result['escrow_account'][$key]['user_account_details'] = array();
                                    
                                    array_push($result['escrow_account'][$key]['user_account_details'],$valuex);
                            }
                        }
                    }
                }
            }
            return $result;
    }
    
}
