<?php
	session_start();
	require 'PaypalSDK/autoload.php';
	use PayPal\Api\Amount;
	use PayPal\Api\Details;
	use PayPal\Api\Item;
	use PayPal\Api\ItemList;
	use PayPal\Api\CreditCard;
	use PayPal\Api\Payer;
	use PayPal\Api\Payment;
	use PayPal\Api\FundingInstrument;
	use PayPal\Api\Transaction;
	class Payment_paypal{
		private $opta;
		private $key;
		private $id;
		private $mode;
		private $url;
		/**
		*Payment Gateway Library Constructor
		*Initiate API KEY, API USER ID AND URL as the basis of MODE (LIVE/TEST)
		*@access public
		*/
		public function __construct(){
			if(MODE_PAYPAL=="LIVE"){
				$this->mode="live";
				$this->key=LIVE_PAYPAL_SECRETE_KEY;
				$this->id=LIVE_PAYPAL_CLIENT_ID;
			}
			else
			{
				$this->mode="sandbox";
				$this->key=TEST_PAYPAL_SECRETE_KEY;
				$this->id=TEST_PAYPAL_CLIENT_ID;
			}
		}
		
		
		public function sendPaymentRequest($opta)
		{
			$validation=array(
				'card_type',
				'card_number',
				'card_month',
				'card_year',
				'cvv_code',
				'card_holder_first_name',
				'card_holder_last_name',
				'policy_no',
				'policy_desc',
				'amount',
				'txn_no',
			);
			foreach($validation as $data)
			{
				if(empty($opta[$data]))
					return false;
			}
			$apiContext = new \PayPal\Rest\ApiContext(
						new \PayPal\Auth\OAuthTokenCredential(
							$this->id, // Client ID
							$this->key //Client Secrete
							)
					);
					$apiContext->setConfig(
						array(
							'mode' => $this->mode,
						)
					);
			
			$creditCard = new CreditCard();
			$creditCard->setType($opta['card_type'])
				->setNumber($opta['card_number'])
				->setExpireMonth($opta['card_month'])
				->setExpireYear($opta['card_year'])
				->setCvv2($opta['cvv_code'])
				->setFirstName($opta['card_holder_first_name'])
				->setLastName($opta['card_holder_last_name']);
			
			$fi = new FundingInstrument();
			$fi->setCreditCard($creditCard);
			
			$payer = new Payer();
			$payer->setPaymentMethod("credit_card")->setFundingInstruments(array($fi));
			
			$item1 = new Item();
			$item1->setName($opta['policy_no'])
				->setDescription($opta['policy_desc'])
				->setCurrency('USD')
				->setQuantity(1)
				->setTax(0.0)
				->setPrice($opta['amount']);
			
			$itemList = new ItemList();
			$itemList->setItems(array($item1));
			
			$details = new Details();
			$details->setShipping(0.0)
				->setTax(0.0) ->setSubtotal($opta['amount']);
			
			
			$amount = new Amount();
			$amount->setCurrency("USD")
				->setTotal($opta['amount'])
				->setDetails($details);
			
			$transaction = new Transaction();
			$transaction->setAmount($amount)
					->setItemList($itemList)
					->setDescription($opta['policy_desc'])
					->setInvoiceNumber($opta['txn_no']);
			$payment = new Payment();
			$payment->setIntent("sale")
				->setPayer($payer)
				->setTransactions(array($transaction));
			try {
				$payment->create($apiContext);
			}catch (PayPal\Exception\PayPalConnectionException $ex) {
				//echo $ex->getData();
				return false;
			}
			return $payment;
		}

		
		/**
		*Convert to XML to ARRAY
		*@access private
		*/
		private function xmlToArray($xml, $options = array()) {
			$defaults = array(
				'namespaceSeparator' => ':',//you may want this to be something other than a colon
				'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
				'alwaysArray' => array(),   //array of xml tag names which should always become arrays
				'autoArray' => true,        //only create arrays for tags which appear more than once
				'textContent' => '$',       //key used for the text content of elements
				'autoText' => true,         //skip textContent key if node has no attributes or child nodes
				'keySearch' => false,       //optional search and replace on tag and attribute names
				'keyReplace' => false       //replace values for above search values (as passed to str_replace())
			);
			$options = array_merge($defaults, $options);
			$namespaces = $xml->getDocNamespaces();
			$namespaces[''] = null; //add base (empty) namespace
		 
			//get attributes from all namespaces
			$attributesArray = array();
			foreach ($namespaces as $prefix => $namespace) {
				foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
					//replace characters in attribute name
					if ($options['keySearch']) $attributeName =
							str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
					$attributeKey = $options['attributePrefix']
							. ($prefix ? $prefix . $options['namespaceSeparator'] : '')
							. $attributeName;
					$attributesArray[$attributeKey] = (string)$attribute;
				}
			}
		 
			//get child nodes from all namespaces
			$tagsArray = array();
			foreach ($namespaces as $prefix => $namespace) {
				foreach ($xml->children($namespace) as $childXml) {
					//recurse into child nodes
					$childArray = $this->xmlToArray($childXml, $options);
					list($childTagName, $childProperties) = each($childArray);
		 
					//replace characters in tag name
					if ($options['keySearch']) $childTagName =
							str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
					//add namespace prefix, if any
					if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;
		 
					if (!isset($tagsArray[$childTagName])) {
						//only entry with this key
						//test if tags of this type should always be arrays, no matter the element count
						$tagsArray[$childTagName] =
								in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
								? array($childProperties) : $childProperties;
					} elseif (
						is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
						=== range(0, count($tagsArray[$childTagName]) - 1)
					) {
						//key already exists and is integer indexed array
						$tagsArray[$childTagName][] = $childProperties;
					} else {
						//key exists so convert to integer indexed array with previous value in position 0
						$tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
					}
				}
			}
		 
			//get text content of node
			$textContentArray = array();
			$plainText = trim((string)$xml);
			if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;
		 
			//stick it all together
			$propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
					? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;
		 
			//return node as array
			return array(
				$xml->getName() => $propertiesArray
			);
		}
	}
?>