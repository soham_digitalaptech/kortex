<?php 
	class Payment{
		private $opta;
		private $key;
		private $id;
		private $mode;
		private $url;
		/**
		*Payment Gateway Library Constructor
		*Initiate API KEY, API USER ID AND URL as the basis of MODE (LIVE/TEST)
		*@access public
		*/
		public function __construct(){
			if(MODE=="LIVE"){
				$this->mode="LIVE";
				$this->key=LIVE_KEY;
				$this->id=LIVE_ID;
				$this->url=LIVE_URL;
			}
			else
			{
				$this->mode="TEST";
				$this->key=TEST_KEY;
				$this->id=TEST_ID;
				$this->url=TEST_URL;
			}
		}
		
		/**
		*Send Payment Request to Payment Gateway and Receive Redirection Secure URL and Redirect to that URL
		*Also generate URL Request XML
		*And call via cURL submitXml()
		*@params array(
				'amount'=>'x.xx', //(Double must be 2 place Decimal)Mandatory
				'name'=>'', //(String) Optional
				'email'=>'', //(String) Optional
				'policy_no'=>'', //(String) Optional
				'txn_id'=>'', //(String) Optional
			)
		*@access public
		*/
		public function sendPaymentRequest($opta)
		{
			$this->opta=array(
				'amount'=>'0.00',
				'name'=>'',
				'email'=>'',
				'policy_no'=>'',
				'txn_id'=>''
			);
			foreach($opta as $key=>$value)
			{
				$this->opta[$key]=htmlspecialchars($value);
			}
			$requestXML='<GenerateRequest>
					<PxPayUserId>'.$this->id.'</PxPayUserId>
					<PxPayKey>'.$this->key.'</PxPayKey>
					<MerchantReference>'.$this->opta['policy_no'].'</MerchantReference>
					<TxnType>Purchase</TxnType>
					<AmountInput>'.number_format($this->opta['amount'], 2, '.', '').'</AmountInput>
					<CurrencyInput>USD</CurrencyInput>
					<TxnData1>'.$this->opta['name'].'</TxnData1>
					<TxnData2></TxnData2>
					<TxnData3></TxnData3>
					<EmailAddress>'.$this->opta['email'].'</EmailAddress>
					<TxnId>'.$this->opta['txn_id'].'</TxnId>
					<UrlSuccess>'.base_url().'action/success/'.'</UrlSuccess>
					<UrlFail>'.base_url().'action/failed/'.'</UrlFail>
				     </GenerateRequest>';
			try{	     
				$response=$this->xmlToArray(new SimpleXMLElement($this->submitXml($this->url,$requestXML)));
				redirect($response['Request']['URI']);
			}
			catch(Exception $e)
			{
				echo 'ERROR!! Error has been occured while generating Payment Request';
				die;
			}
		}
		
		/**
		*Decrypt Response
		*Call a Requst to Payment Gateway for Dycrpt transaction information by response id sent on success or failure redirction of pages
		*@access public
		*/
		public function decryptResponse($response_id)
		{
			if(empty($response_id)){
				return false;
			}
			$requestXML='
				<ProcessResponse>
					<PxPayUserId>'.$this->id.'</PxPayUserId>
					<PxPayKey>'.$this->key.'</PxPayKey>
					<Response>'.$response_id.'</Response>
				</ProcessResponse>
			';
			//echo $this->submitXml($this->url,$requestXML);die;
			
			try{	     
				$response=$this->xmlToArray(new SimpleXMLElement($this->submitXml($this->url,$requestXML)));
				return $response;
			}
			catch(Exception $e)
			{
				echo 'ERROR!! Error has been occured while generating Payment Response';
				die;
			}
		}
		
		/**
		*Decrypt Response
		*This is cURL Function
		*@access private
		*/
		private function submitXml($url,$inputXml){
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url);
			
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$inputXml);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			
			#set up proxy, this may change depending on ISP, please contact your ISP to get the correct cURL settings
			#curl_setopt($ch,CURLOPT_PROXY , "proxy:8080");
			#curl_setopt($ch,CURLOPT_PROXYUSERPWD,"username:password");
	
			$outputXml = curl_exec ($ch); 		
				
			curl_close ($ch);
	  
			return $outputXml;
		}
		
		/**
		*Convert to XML to ARRAY
		*@access private
		*/
		private function xmlToArray($xml, $options = array()) {
			$defaults = array(
				'namespaceSeparator' => ':',//you may want this to be something other than a colon
				'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
				'alwaysArray' => array(),   //array of xml tag names which should always become arrays
				'autoArray' => true,        //only create arrays for tags which appear more than once
				'textContent' => '$',       //key used for the text content of elements
				'autoText' => true,         //skip textContent key if node has no attributes or child nodes
				'keySearch' => false,       //optional search and replace on tag and attribute names
				'keyReplace' => false       //replace values for above search values (as passed to str_replace())
			);
			$options = array_merge($defaults, $options);
			$namespaces = $xml->getDocNamespaces();
			$namespaces[''] = null; //add base (empty) namespace
		 
			//get attributes from all namespaces
			$attributesArray = array();
			foreach ($namespaces as $prefix => $namespace) {
				foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
					//replace characters in attribute name
					if ($options['keySearch']) $attributeName =
							str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
					$attributeKey = $options['attributePrefix']
							. ($prefix ? $prefix . $options['namespaceSeparator'] : '')
							. $attributeName;
					$attributesArray[$attributeKey] = (string)$attribute;
				}
			}
		 
			//get child nodes from all namespaces
			$tagsArray = array();
			foreach ($namespaces as $prefix => $namespace) {
				foreach ($xml->children($namespace) as $childXml) {
					//recurse into child nodes
					$childArray = $this->xmlToArray($childXml, $options);
					list($childTagName, $childProperties) = each($childArray);
		 
					//replace characters in tag name
					if ($options['keySearch']) $childTagName =
							str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
					//add namespace prefix, if any
					if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;
		 
					if (!isset($tagsArray[$childTagName])) {
						//only entry with this key
						//test if tags of this type should always be arrays, no matter the element count
						$tagsArray[$childTagName] =
								in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
								? array($childProperties) : $childProperties;
					} elseif (
						is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
						=== range(0, count($tagsArray[$childTagName]) - 1)
					) {
						//key already exists and is integer indexed array
						$tagsArray[$childTagName][] = $childProperties;
					} else {
						//key exists so convert to integer indexed array with previous value in position 0
						$tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
					}
				}
			}
		 
			//get text content of node
			$textContentArray = array();
			$plainText = trim((string)$xml);
			if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;
		 
			//stick it all together
			$propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
					? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;
		 
			//return node as array
			return array(
				$xml->getName() => $propertiesArray
			);
		}
	}
?>