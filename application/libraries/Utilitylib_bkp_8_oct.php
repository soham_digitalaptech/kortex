<?php
 class Utilitylib 
{
 
 	protected $CI;
	
 	public function __construct($rules = array())
	{
		$this->CI =& get_instance();
                $this->CI->load->model('generic_model');
	}
 
 
	function showMsg()
	{
	     if($this->CI->session->userdata('msg'))
	     {
		   echo '<div style="clear:both;"></div><div class="'.$this->CI->session->userdata('class').'">'.$this->CI->session->userdata('msg').'</div><div style="height:10px;">&nbsp;</div>';
		   $this->CI->session->unset_userdata('msg');
		   $this->CI->session->unset_userdata('class');
	     }
	     
	}
        function sendMail($to,$sub,$body)
	{
            $this->CI->load->library('email');
            $this->CI->email->set_mailtype("html");
            $this->CI->email->from('info@healthcrowds.com', 'Health Crowds');
            $this->CI->email->to($to); 
            $this->CI->email->subject($sub);
            $cont='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
              <title>Untitled Document</title>
            </head>

            <body>
                  <div style="width:500px; float:left; background:#fff; border:1px solid #a2d246;">
                   <div style="text-align:center;padding:10px;"><a href="'.base_url().'"><img src="'.base_url().'/images/logo.png" border="0" align="middle"/></a></div>
                   <div style="font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; color:#333; width:480px; padding:10px; margin: 0; border-top:1px solid #a2d246;">
                     <div>'.$body.'</div>
                 <p style="color:#D20D5E;">Thank you  <br>HealthCrowds Team </p>
               </div>
              </div>
            </body>
           </html>';
           $this->CI->email->message($cont);
           return $this->CI->email->send();
	}
        
        function behav_category($id='',$name='category',$caption='Choose')
        {        
            $behavior_category = $this->CI->generic_model->get_data('behavior_category');
            $opt='<select name="'.$name.'" class="form-control category"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($behavior_category as $data):
                    $sel=($id!='' && $id==$data['behav_cat_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['behav_cat_id'].'">'.$data['behav_cat_name'].'</option>';
                endforeach;
            }
            echo $opt.'</select>';
        }
        function behav_group($id='',$name='group',$caption='Choose')
        {        
            $behavior_group = $this->CI->generic_model->get_data('behavior_group');
            $opt='<select name="'.$name.'" class="form-control group"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($behavior_group as $data):
                    $sel=($id!='' && $id==$data['behav_group_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['behav_group_id'].'">'.$data['behav_group_name'].'</option>';
                endforeach;
            }
            echo $opt.'</select>';
        }
        function behavior($id='',$name='behavior',$caption='Choose')
        {        
            $behavior = $this->CI->generic_model->get_data('behaviors');
            $opt='<select name="'.$name.'" class="form-control behavior"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($behavior as $data):
                    $sel=($id!='' && $id==$data['behaviors_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['behaviors_id'].'">'.$data['behavior_name'].'</option>';
                endforeach;
            }
            echo $opt.'</select>';
        }
        
        function behavior_dosage($id='',$name='behavior_dosage',$caption='Choose')
        {        
            $behavior = $this->CI->generic_model->get_data('behaviors');
            $opt='<select name="'.$name.'" class="form-control behavior_dosage"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($behavior as $data):
                    $sel=($id!='' && $id==$data['behaviors_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['behaviors_id'].'">'.$data['behavior_dosage_type'].'</option>';
                endforeach;
            }
            echo $opt.'</select>';
        }
        
        function problem_category($id='',$name='problem_category',$caption='Choose')
        {        
            $problem_category = $this->CI->generic_model->get_data('problem_category');
            $opt='<select name="'.$name.'" class="form-control problem_category"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($problem_category as $data):
                    $sel=($id!='' && $id==$data['problem_category_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['problem_category_id'].'">'.$data['pc_name'].'</option>';
                endforeach;
            }
            echo $opt.'</select>'.$id;
        }
        function problem_group($id='',$name='problem_group',$caption='Choose')
        {        
            $problem_group = $this->CI->generic_model->get_data('problem_group');
            $opt='<select name="'.$name.'" class="form-control problem_group"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($problem_group as $data):
                    $sel=($id!='' && $id==$data['problem_group_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['problem_group_id'].'">'.$data['pg_name'].'</option>';
                endforeach;
            }
            echo $opt.'</select>';
        }
        function problem($id='',$name='problem',$caption='Choose')
        {        
            $problem = $this->CI->generic_model->get_data('problem');
            $opt='<select name="'.$name.'" class="form-control problem"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($problem as $data):
                    $sel=($id!='' && $id==$data['problem_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['problem_id'].'">'.$data['problem_name'].'</option>';
                endforeach;
            }
            echo $opt.'</select>';
        }
        
        
        
        
        function side_effect_category($id='',$name='side_effect_cat',$caption='Choose')
        {        
            $problem_category = $this->CI->generic_model->get_data('side_effect_category');
            $opt='<select name="'.$name.'" class="form-control side_effect_category"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($problem_category as $data):
                    $sel=($id!='' && $id==$data['side_effect_category_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['side_effect_category_id'].'">'.$data['side_effect_category_name'].'</option>';
                endforeach;
            }
            echo $opt.'</select>'.$id;
        }
        function side_effect_group($id='',$name='side_effect_group',$caption='Choose')
        {        
            $problem_group = $this->CI->generic_model->get_data('side_effect_group');
            $opt='<select name="'.$name.'" class="form-control side_effect_group"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($problem_group as $data):
                    $sel=($id!='' && $id==$data['side_effect_group_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['side_effect_group_id'].'">'.$data['side_effect_group_name'].'</option>';
                endforeach;
            }
            echo $opt.'</select>';
        }
        function side_effect($id='',$name='side_effect',$caption='Choose')
        {        
            $problem = $this->CI->generic_model->get_data('side_effect');
            $opt='<select name="'.$name.'" class="form-control side_effect"><option value="">'.$caption.'</option>';
            if($id!='')
            {
                foreach ($problem as $data):
                    $sel=($id!='' && $id==$data['side_effect_id'])?'selected':'';
                    $opt.='<option '.$sel.' value="'.$data['side_effect_id'].'">'.$data['side_effect_name'].'</option>';
                endforeach;
            }
            echo $opt.'</select>';
        }
        
        
        function daily_schedule($id='')
        {
            $data = $this->CI->generic_model->get_data('behavior_daily_schedule',array('behavior_ds_status '=>1));
            
            $opt='';
            foreach ($data as $dat):
               $sel=($id!='' && $id==$dat['behavior_ds_id'])?'selected':'';
               $opt.='<option '.$sel.' value="'.$dat['behavior_ds_id'].'">'.$dat['behavior_ds_name'].'</option>';
            endforeach;
            return $opt;
        }
        
        function calender_schedule($id='')
        {
            $data = $this->CI->generic_model->get_data('Behavior_calendar_schedule',array('status '=>1));
            
            $opt='';
            foreach ($data as $dat):
               $sel=($id!='' && $id==$dat['Behavior_calendar_schedule_id'])?'selected':'';
               $opt.='<option '.$sel.' value="'.$dat['Behavior_calendar_schedule_id'].'">'.$dat['Behavior_cs_name'].'</option>';
            endforeach;
            return $opt;
        }
        
        function meals($id='')
        {
            $data = $this->CI->generic_model->get_data('behavior_meals',array('status '=>1));
            
            $opt='';
            foreach ($data as $dat):
               $sel=($id!='' && $id==$dat['behavior_meals_id'])?'selected':'';
               $opt.='<option '.$sel.' value="'.$dat['behavior_meals_id'].'">'.$dat['behavior_meals_name'].'</option>';
            endforeach;
            return $opt;
        }
        
        function get_behavior($id,$field)
        {        
            if(!empty($id)){
            $behavior = $this->CI->generic_model->get_data('behaviors',array('behaviors_id ='=>$id));
            return $behavior[0][$field];
            }
        }
 }
