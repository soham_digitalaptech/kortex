<?php
	/**
	*CarjamApi Class
	*@Author: Unifiedinfotech.net
	*Date: 27-March-2015
	*Developed By: Soham Krishna Paul
	*/
	class CarjamApi{
		private $apiUrl;
		private $apiKey;
		private $plate;
		public function __construct(){
			$this->apiKey=CARJAM_API_LIVE_KEY;
			$this->apiUrl=CARJAM_API_LIVE_URL;
			if(CARJAM_API_MODE=="TEST"){
				$this->apiKey=CARJAM_API_TEST_KEY;
				$this->apiUrl=CARJAM_API_TEST_URL;
			}		
		}
		/**
		*Get Car Details Function Array getDetails(string)
		*@access Public
		*@param Car Registration/Plate No (String)
		*@return Array
		*/
		public function getDetails($plate)
		{
			$this->plate=$plate;
			$url=$this->buildUrl();
			//echo $url;die;
			return json_decode($this->sendRequest($url),true);
		}
		
		/**
		*BuildURL Function Array getDetails(string)
		*Building Api Rquest Url
		*@access Private
		*@param Null
		*@return String
		*/
		private function buildUrl()
		{
			/*
			Request parameters:

				plate — plate or VIN
				key — API key
				basic — include basic information
				owners — include ownership history information (optional and defaults to ‘false’)
				owner — include current owner details
				ppsr — include PPSR (money owing information)
				rucs — include RUC history
				motfuel — include Motfuel/FuelSaver information
				translate — provide translations for the raw encoded values in the separate tags prefixed with ‘h’ (optional and defaults to ‘false’)
				cache — enable lookup of basic information in CarJam cache before going to NZTA
				f — xml or json. default is xml.
			*/

			//$url=$this->apiUrl."?key=".$this->apiKey."&plate=".$this->plate."&f=json&basic=1&owner=1&ppsr=1&motfuel=1&cache=1";
			$url=$this->apiUrl."?key=".$this->apiKey."&plate=".$this->plate."&f=json&basic=1&cache=1";
			return $url;
		}
		
		/**
		*Execute Function
		*@access Private
		*@param String
		*@return XML
		*/
		private function sendRequest($opta)
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL,$opta);
			$result=curl_exec($ch);
			curl_close($ch);
			return $result;
		}
		
		/**
		*Convert to XML to ARRAY
		*@access private
		*/
		private function xmlToArray($xml, $options = array()) {
			$defaults = array(
				'namespaceSeparator' => ':',//you may want this to be something other than a colon
				'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
				'alwaysArray' => array(),   //array of xml tag names which should always become arrays
				'autoArray' => true,        //only create arrays for tags which appear more than once
				'textContent' => '$',       //key used for the text content of elements
				'autoText' => true,         //skip textContent key if node has no attributes or child nodes
				'keySearch' => false,       //optional search and replace on tag and attribute names
				'keyReplace' => false       //replace values for above search values (as passed to str_replace())
			);
			$options = array_merge($defaults, $options);
			$namespaces = $xml->getDocNamespaces();
			$namespaces[''] = null; //add base (empty) namespace
		 
			//get attributes from all namespaces
			$attributesArray = array();
			foreach ($namespaces as $prefix => $namespace) {
				foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
					//replace characters in attribute name
					if ($options['keySearch']) $attributeName =
							str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
					$attributeKey = $options['attributePrefix']
							. ($prefix ? $prefix . $options['namespaceSeparator'] : '')
							. $attributeName;
					$attributesArray[$attributeKey] = (string)$attribute;
				}
			}
		 
			//get child nodes from all namespaces
			$tagsArray = array();
			foreach ($namespaces as $prefix => $namespace) {
				foreach ($xml->children($namespace) as $childXml) {
					//recurse into child nodes
					$childArray = $this->xmlToArray($childXml, $options);
					list($childTagName, $childProperties) = each($childArray);
		 
					//replace characters in tag name
					if ($options['keySearch']) $childTagName =
							str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
					//add namespace prefix, if any
					if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;
		 
					if (!isset($tagsArray[$childTagName])) {
						//only entry with this key
						//test if tags of this type should always be arrays, no matter the element count
						$tagsArray[$childTagName] =
								in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
								? array($childProperties) : $childProperties;
					} elseif (
						is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
						=== range(0, count($tagsArray[$childTagName]) - 1)
					) {
						//key already exists and is integer indexed array
						$tagsArray[$childTagName][] = $childProperties;
					} else {
						//key exists so convert to integer indexed array with previous value in position 0
						$tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
					}
				}
			}
		 
			//get text content of node
			$textContentArray = array();
			$plainText = trim((string)$xml);
			if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;
		 
			//stick it all together
			$propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
					? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;
		 
			//return node as array
			return array(
				$xml->getName() => $propertiesArray
			);
		}
	}
?>