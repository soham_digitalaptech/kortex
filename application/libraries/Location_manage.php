<?php
 class Location_manage
{
    public $rules=array();
    protected $CI;
    
    public function __construct($rules = array())
    {
         $this->CI =& get_instance();
         $this->rules=$rules;
         $this->CI->load->model('generic_model');
    }

    function country($coutry_id='',$name='',$class='')
    {   
        $country_list = $this->CI->generic_model->get_data('country',array('country_status '=>'1'));
        
        $opt='<select name="'.$name.'" class="form-control '.$class.'"  style="width:73%;"><option value="">Choose</option>';
        foreach ($country_list as $count_data):
            $sel=($coutry_id!='' && $coutry_id=$count_data['country_id'])?'selected':'';
            $opt.='<option '.$sel.' value="'.$count_data['country_id'].'">'.$count_data['country_name'].'</option>';
        endforeach;
        
        return $opt.'</select>';
    }
    
    function state($country_id='',$name='',$class='state',$state_id='')
    {
        $state_list = $this->CI->generic_model->get_data('state',array('country_id '=>$country_id));
        
        $opt='<select name="'.$name.'" class="form-control '.$class.'"  style="width:73%;"><option value="">Choose</option>';
        if($country_id!=''){
        foreach ($state_list as $state_data):
            $sel=($state_id!='' && $state_id=$state_data['state_id'])?'selected':'';
            $opt.='<option '.$sel.' value="'.$state_data['state_id'].'">'.$state_data['state_name'].'</option>';
        endforeach;
        }
        echo $opt.'</select>';
    }
    
    function city($state_id='',$name='',$class='city',$city_id='')
    {
        $city_list = $this->CI->generic_model->get_data('city',array('state_id '=>$state_id));
        
        $opt='<select name="'.$name.'" class="form-control '.$class.'"  style="width:73%;"><option value="">Choose</option>';
        if($state_id!=''){
        foreach ($city_list as $city_data):
            $sel=($city_id!='' && $city_id=$city_data['city_id'])?'selected':'';
            $opt.='<option '.$sel.' value="'.$city_data['city_id'].'">'.$city_data['city_name'].'</option>';
        endforeach;
        }
        echo $opt.'</select>';
    }
    
    
        
 }
