-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2016 at 09:59 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `freelancedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `new_password` varchar(255) NOT NULL,
  `last_login` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `admin_username`, `admin_password`, `new_password`, `last_login`) VALUES
(1, 'admin', '7b1d6a0497d7216a55272f992600771ac8ea2384', '', 1460275333);

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL,
  `ads_code` longtext NOT NULL,
  `ads_size` varchar(255) NOT NULL,
  `ads_company` varchar(255) NOT NULL,
  `status` int(1) NOT NULL COMMENT '0->Inactive,1->Active,2->Partial Delete',
  `date_of_creation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `date_of_creation` int(11) NOT NULL,
  `date_of_modification` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1=Active/2=Inactive/3=Partially Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE IF NOT EXISTS `cms` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `short_description` longtext NOT NULL,
  `description` longtext NOT NULL,
  `meta_title` mediumtext,
  `meta_keyword` mediumtext,
  `meta_description` mediumtext,
  `date_of_created` int(11) NOT NULL,
  `date_of_modified` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=Blocked/1=Active',
  `is_in_menu` tinyint(1) NOT NULL COMMENT '0=Not Place in Menu/1=Place in Menu'
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`id`, `title`, `slug`, `short_description`, `description`, `meta_title`, `meta_keyword`, `meta_description`, `date_of_created`, `date_of_modified`, `order`, `status`, `is_in_menu`) VALUES
(1, 'Home', 'home', 'Home Page', '&lt;h1&gt;Welcome to Indoasian Elevators&lt;/h1&gt;\r\n\r\n&lt;p&gt;Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula.&lt;/p&gt;\r\n', 'Home Page Meta', 'Home Page Keyword', 'Home Page Description', 1449064473, 1451130173, 1, 1, 1),
(2, 'About Us', 'about', 'About Us', '&lt;p&gt;Coruscate comes with business and creative designed layout to provide you the best way to feel how your page would look like. Both layouts come with extensive color options palette.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;&quot; class=&quot;cmi_responsive img-responsive&quot; src=&quot;img/content/pic_content.jpg&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus.&lt;/p&gt;\r\n\r\n&lt;p&gt;Coruscate comes with business and creative designed layout to provide you the best way to feel how your page would look like. Both layouts come with extensive color options palette. Color styles are placed in separate css file so you can easily.&lt;/p&gt;\r\n\r\n&lt;p&gt;Coruscate comes with business and creative designed layout to provide you the best way to feel how your page would look like. Both layouts come with extensive color options palette. Color styles are placed in separate css file so you can easily.&lt;/p&gt;\r\n\r\n&lt;p&gt;Coruscate comes with business and creative designed layout to provide you the best way to feel how your page would look like. Both layouts come with extensive color options palette. Color styles are placed in separate css file so you can easily.&lt;/p&gt;\r\n', '', '', '', 1451129309, 1451131747, 2, 1, 1),
(3, 'Services', 'services', '', '', NULL, NULL, NULL, 0, 0, 5, 1, 1),
(6, 'Modernization', 'modernization', '', '', NULL, NULL, NULL, 0, 0, 6, 1, 1),
(5, 'Careers', 'careers', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. ', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.&lt;/p&gt;\r\n', '', '', '', 1451709936, 1451709958, 4, 1, 1),
(7, 'Contact Us', 'contact', 'Contact Us', '&lt;p&gt;Contact Us&lt;/p&gt;\r\n', '', '', '', 1451129013, 1451129013, 7, 1, 1),
(9, 'Jobs at Indoasian', 'jobs-at-indoasian', 'Jobs at Indoasian', '&lt;p&gt;Jobs at Gibson&lt;/p&gt;\r\n', 'Jobs at Indoasian', 'Jobs at Indoasian', 'Jobs at Indoasian', 1449145943, 1449145943, 8, 1, 0),
(4, 'Product', 'products', 'Product', '&lt;div class=&quot;our_history&quot;&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.&lt;/p&gt;\r\n\r\n&lt;p&gt;Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur.&lt;/p&gt;\r\n\r\n&lt;p&gt;Coruscate comes with business and creative designed layout to provide you the best way to feel how your page would look like. Both layouts come with extensive color options palette. Color styles are placed in separate css file, so you can easily choose any of them or even make your own color that suits your brand identity.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', '', '', 1451129502, 1451134289, 3, 1, 1),
(12, 'What we Do', 'what-we-do', 'What we Do', '&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-12 col-md-12 col-sm-12 effect-slide-top in&quot; style=&quot;transition: all 0.7s ease-in-out 0s;&quot;&gt;\r\n&lt;div class=&quot;title&quot;&gt;\r\n&lt;h2&gt;What we Do&lt;/h2&gt;\r\n\r\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis nostrud exercitation ullamco laboris nisi ut ex ea commodo consequat.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-6 col-md-6 col-sm-6 effect-slide-left in&quot; style=&quot;transition: all 0.7s ease-in-out 0s;&quot;&gt;\r\n&lt;div class=&quot;video_content&quot;&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-6 col-md-6 col-sm-6 effect-slide-right in&quot; style=&quot;transition: all 0.7s ease-in-out 0s;&quot;&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Donec convallis, metus nec tempus aliquet&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Aenean commodo ligula eget dolor&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Lorem ipsum dolor sit amet adipiscing&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Nunc aliquet tincidunt metus sit amet&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Lorem ipsum dolor sit amet adipiscing&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Nunc aliquet tincidunt metus sit amet&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n', 'What we Do', '', '', 1451129753, 1451130356, 9, 1, 0),
(13, 'Our Latest Product', 'our-latest-product', 'Our Latest Product', '&lt;div class=&quot;title&quot;&gt;\r\n							&lt;h2&gt;Our Latest Product&lt;/h2&gt;\r\n							&lt;p&gt;Nullam ut consectetur dolor. Sed sit amet iaculis nisi. Mauris ridiculus elementum non felis etewe blandit. Vestibulum iaculis dolor porttitors erte.&lt;/p&gt;\r\n						&lt;/div&gt;', '', '', '', 1451130434, 1451130434, 10, 1, 0),
(14, 'Our Mission', 'our-mission', 'Our Mission', '&lt;h4&gt;Our Mission&lt;/h4&gt;\r\n									&lt;p&gt;Going above and beyond to help our customers succeed&lt;/p&gt;\r\n', '', '', '', 1451131814, 1451132057, 11, 1, 0),
(15, 'Our Solutions', 'our-solutions', 'Our Solutions', '&lt;p&gt;Our Solutions&lt;/p&gt;\r\n', '', '', '', 1451132335, 1451132335, 12, 1, 0),
(16, 'Our Visions', 'our-visions', 'Our Visions', '&lt;p&gt;Our Visions&lt;/p&gt;\r\n', '', '', '', 1451132372, 1451133091, 13, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(255) NOT NULL,
  `url` longtext NOT NULL,
  `orderx` int(11) NOT NULL,
  `location` int(1) NOT NULL DEFAULT '1' COMMENT '1=Main Menu/2=Footer Menu',
  `date_of_modification` int(11) NOT NULL,
  `date_of_creation` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `caption`, `url`, `orderx`, `location`, `date_of_modification`, `date_of_creation`) VALUES
(1, 0, 'New Menu', 'http://www.digitalaptech.com', 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL,
  `msg` mediumtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1=Active/2=Outdated',
  `date_of_post` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE IF NOT EXISTS `site_settings` (
  `id` int(11) NOT NULL,
  `setting_name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `setting_value` longtext NOT NULL,
  `instruction` text,
  `is_text` tinyint(1) NOT NULL DEFAULT '1',
  `is_textarea` tinyint(1) NOT NULL DEFAULT '0',
  `is_image` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `setting_name`, `slug`, `setting_value`, `instruction`, `is_text`, `is_textarea`, `is_image`) VALUES
(1, 'Site Name', 'site_name', 'Freelance', '', 1, 0, 0),
(2, 'Company Name', 'company_name', 'Freelance', '', 1, 0, 0),
(3, 'Company Description', 'company_description', '', '', 0, 1, 0),
(4, 'Company Email', 'company_email', 'support.fdx@gmail.icom', '', 1, 0, 0),
(5, 'Admin Email', 'admin_email', 'support.fdx@gmail.com', '', 1, 0, 0),
(6, 'Site Logo', 'site_logo', '146182812725.jpg', 'Maintain Image Size 300x90', 0, 0, 1),
(7, 'Email Caption', 'email_caption', 'Digitalaptech.com', '', 1, 0, 0),
(8, 'Site Meta', 'site_meta', '', '', 0, 1, 0),
(9, 'Facebook Link', 'facebook_link', '', '', 1, 0, 0),
(10, 'Twitter Link', 'twitter_link', '', '', 1, 0, 0),
(11, 'Linkdin Link', 'linkdin_link', '', '', 1, 0, 0),
(12, 'Instragram Link', 'instragram_link', '', '', 1, 0, 0),
(13, 'Youtube Link', 'youtube_link', '', '', 1, 0, 0),
(14, 'Vimeo Link', 'vimeo_link', '', '', 1, 0, 0),
(15, 'Site Contact No', 'site_contact_no', '(+91)-8013066558', '', 1, 0, 0),
(16, 'Site Email', 'site_email', 'support.fdx@gmail.com', '', 1, 0, 0),
(17, 'Admin Image', 'admin_image', '144879833865.jpg', 'Maintain Image Size 29x29', 0, 0, 1),
(18, 'Admin Name', 'admin_name', 'Soham Krishna Paul', NULL, 1, 0, 0),
(19, 'Company Address', 'company_address', 'Kolkata', NULL, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `order` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `snapshot` varchar(255) NOT NULL,
  `url` mediumtext NOT NULL,
  `date_of_creation` int(11) NOT NULL,
  `date_of_modification` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1=Active/2=Inactive/3=Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms`
--
ALTER TABLE `cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms`
--
ALTER TABLE `cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
