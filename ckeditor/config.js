/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
        config.allowedContent = true;
};

CKEDITOR.on('instanceReady', function( ev )
{        
    // Ends self closing tags the HTML4 way, like <br>.
    // See: http://stackoverflow.com/questions/4466185/ckeditor-align-images-instead-of-float
    // Mod added for CKE 4
    // See: http://vibhajadwani.wordpress.com/2011/07/18/how-to-remove-image-style-property-from-ckeditor/
    ev.editor.dataProcessor.htmlFilter.addRules(
    {
        elements:
        {
            $: function( element )
            {
                // Output dimensions of images as width and height
                if( element.name == 'img' )
                {
                    var style = element.attributes.style;

                    if( style )
                    {
                        // Get the width from the style.
                        var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec( style ),
                        width = match && match[ 1 ];

                        // Get the height from the style.
                        match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec( style );
                        var height = match && match[ 1 ];

                        // Get the float from the style.
                        match = /(?:^|\s)float\s*:\s*(\w+)/i.exec( style );

                        var align = match && match[ 1 ];

                        if( align )
                        {
                            element.attributes.align = align;
                        }
                    }

                    element.forEach             = function(){};
                    element.writeChildrenHtml   = function(){};
                }

                return element;
            }
        }
    });
});
